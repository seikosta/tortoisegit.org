<?php
require(__DIR__.'/../../inc/head.php');
printHead('Frequently asked questions');
?>
	<body>
	<div id=wrapper>
		<div id="container_headline">
			<?php printHeadLine(); ?>
		</div>

		<div class="container_seperator">
			<div class="wrap_content breadcrumbs">
				<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">TortoiseGit.org</span></a></span> &raquo; <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/support/" itemprop="url"><span itemprop="title">Support</span></a></span> &raquo; FAQ
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h1>Frequently asked questions (FAQ)</h1>

				<h2>Table of Contents</h2>
				<ol class="ol menubox">
					<li>Installation & Upgrade</li>
						<ol class="ol">
							<li><a href="#prerequisites" class="link_menubox">What are the system prerequisites of TortoiseGit?</a></li>
							<li><a href="#install" class="link_menubox">How to install TortoiseGit?</a></li>
							<li><a href="#upgrade" class="link_menubox">How to update/upgrade TortoiseGit?</a></li>
							<li><a href="#installcommonproblems" class="link_menubox">The installer fails with an error message</a></li>
							<li><a href="#explorernotrestarting" class="link_menubox">The installer closes the explorer leaving me with a empty screen</a></li>
							<li><a href="#restartlotsofprograms" class="link_menubox">Installer requires to close/restart at lot of programs on update</a></li>
						</ol>
					<li>Overlay icons
						<ol class="ol">
							<li><a href="#ovlnotshowing" class="link_menubox">Why don't the icon overlays appear?</a></li>
							<li><a href="#ovlnotall" class="link_menubox">The overlay icons appear, but not all of them!</a></li>
							<li><a href="#ovlonlylocal" class="link_menubox">Why are the icons only visible on local and not on network drives?</a></li>
						</ol>
					</li>
					<li>How can I&hellip;
						<ol class="ol">
							<li><a href="#howremembercredentials" class="link_menubox">&hellip; make TortoiseGit remember HTTP(s) crecentials?</a></li>
							<li><a href="#howputtygitcli" class="link_menubox">&hellip; use PuTTY for git cli and git bash?</a></li>
							<li><a href="#howselfsignedcert" class="link_menubox">&hellip; close/push/pull from a server with a self signed or &quot;invalid&quot; certificate?</a></li>
						</ol>
					</li>
					<li>General questions
						<ol class="ol">
							<li><a href="#sshconfigignored" class="link_menubox">~/.ssh/config seems to be ignored by TortoiseGit</a></li>
							<li><a href="#tortoisegitblamewronglanguage" class="link_menubox">TortoiseGitBlame shows menu in wrong language</a></li>
						</ol>
					</li>
				</ol>
			</div>
		</div>

		<div class="container_white">
			<div class="wrap_content contentpage">
				<h2>Installation & Upgrade</h2>
				<h3 id="prerequisites">What are the system prerequisites of TortoiseGit?</h3>
				<ul class="ul">
					<li>Windows XP SP3 or newer is required</li>
					<li>For Windows 7, SP1 is required; For Windows Vista, SP2 is required</li>
					<li>Admin privileges for the installation</li>
					<li><a href="http://www.microsoft.com/en-us/download/details.aspx?id=8483" target="_blank" rel="nofollow">Windows Installer 4.5</a> (required since TortoiseGit 1.7.15.2)</li>
					<li><a href="https://msysgit.github.io/" target="_blank" rel="nofollow">Git for Windows</a> is required by TortoiseGit (sometimes also mentioned as msysGit)
						<ul class="ul">
							<li>Installation of Git for Windows can be done with preselected options, however, no need to install the &quot;Windows explorer integration&quot;. If you know about CRLF and LF line endings and you have editors coping with that, you should select &quot;Checkout as-is, commit as-is&quot; in order to prevent automatic translations</li>
							<li><a href="https://msysgit.github.io/" target="_blank" rel="nofollow">Git for Windows/msysGit 1.9.5+</a> is recommended for TortoiseGit 1.7.9+ (Git for Windows/msysGit 1.7.10 adds utf-8 support and is compatible to *nix git)</li>
							<li><a href="https://git-for-windows.github.io/" target="_blank" rel="nofollow">Git for Windows 2.0+</a> is also supported since TortoiseGit 1.8.15</li>
							<li>minimum compatible version is 1.6.1 (for TortoiseGit &lt; 1.7.9 you should use Git for Windows/msysGit 1.7.6)</li>
							<li>(Cygwin and Msys2 Git also work, see <a href="/docs/tortoisegit/tgit-dug-settings.html#tgit-dug-settings-main" target="_blank">manual</a> for configuration. Please note that Cygwin and Msys2 Git are not officially supported by TortoiseGit as the developers only use Git for Windows. Bug reports, however, are welcome. </li>
						</ul>
					</li>
				</ul>
				<p><a href="#top">&uarr; Top</a></p>

				<h3 id="install">How to install TortoiseGit?</h3>
				<p>Check the <a href="#prerequisites">system prerequisites</a> which are listed above.</p>
				<p>As a command line git client is required for using TortoiseGit you have to install both. The recommended order is to install TortoiseGit first.</p>
				<p>Just <a href="/download/">download</a> the setup package for your system and install it. If you want to localized interface of TortoiseGit also download a language pack and install it. If you are running a 64 bit system, you do not need to download and install the 32 bit version: The 32 bit shell extension is included in the 64 bit installer since TortoiseGit 1.7.3.0. </p>
				<p><a href="#top">&uarr; Top</a></p>

				<h3 id="upgrade">How to update/upgrade TortoiseGit?</h3>
				<p>Before upgrading you should read the <a href="/docs/releasenotes/">release notes</a> so that you know what changed.</p>
				<p>Just <a href="/download/">download</a> the setup package for your system and install it. The old version will be replaced automatically. If possible please use the automatic updater of TortoiseGit (in this case the binaries are cryptographically verified). You can trigger an update check by going to the about dialog of TortoiseGit and clicking on "Check for update".</p>
				<p>If you are upgrading from 1.7.3.0 or older and you have installed the 32-bit version on a 64-bit system you have to uninstall the 32-bit version first. </p>
				<p><a href="#top">&uarr; Top</a></p>

				<h3 id="installcommonproblems">The installer fails with an error message</h3>
				<p>There are some error messages which are more common:
					<dl>
						<dt>"This installation package is not supported by this processor type. Contact your product vendor."</dt>
						<dd>This means you are trying to install the 64-bit version of TortoiseGit on a normal 32-bit operating system. You need to download and use the correct msi file for your OS. For normal 32-bit OS, make sure the msi filename does not have "64-bit" in it. </dd>

						<dt>"Please wait while the installer finishes determining your disk space requirements."</dt>
						<dd>Cleanup/empty the temp-directory (e.g. C:\Users\<your user>\AppData\Local\Temp, C:\User and Settings\<your user>\Local Settings\Temp, c:\Windows\Temp\).</dd>
					</dl>
				</p>
				<p><a href="#top">&uarr; Top</a></p>

				<h3 id="explorernotrestarting">The installer closes the explorer leaving me with a empty screen</h3>
				<p>This is a known issue (issues <a href="/issue/1161" target="_blank">#1161</a>, <a href="/issue/2103" target="_blank">#2103</a>, <a href="/issue/2367" target="_blank">#2367</a>, and <a href="/issue/2557" target="_blank">#2557</a>) in the Windows Installer and we're sorry, becasue there is nothing we can do against this.</p>
				<p><a href="#top">&uarr; Top</a></p>

				<h3 id="restartlotsofprograms">Installer requires to close/restart at lot of programs on update</h3>
				<p>TortoiseGit is a shell extension. So, if any program dispalys an explorer like dialog (such as a file open or save dialog) TortoiseGit.dll is loaded by the process. Therefore, many processes might require to be restarted.</p>
				<p><a href="#top">&uarr; Top</a></p>
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h2>Overlay icons</h2>
				<h3 id="ovlnotshowing">Why don't the icon overlays appear?</h3>
				<ul class="ul">
					<li>You rebooted your PC of course after the installation? If you haven't please do so now. TortoiseGit is a Windows Explorer Shell extension and will be loaded together with Explorer.</li>
					<li>Go to the settings of TSVN and activate the icon overlays for at least the fixed drives. The installer does this automatically for the <em>current</em> user (can't do it for other users&hellip;) but since you are using TortoiseGit as a different user than you installed it you need to set this manually.</li>
				</ul>
				<p>Based on a similar <a href="http://tortoisesvn.net/faq.html#ovlnotshowing" target="_blank" rel="nofollow">TortoiseSVN FAQ entry</a></p>
				<p><a href="#top">&uarr; Top</a></p>

				<h3 id="ovlnotall">The overlay icons appear, but not all of them!</h3>
				<p>You may find that not all of these icons are used on your system. This is because the number of overlays allowed by Windows is limited to 15. Windows uses 4 of those, and the remaining 11 can be used by other applications. And if you have OneDrive installed, that uses another 5 slots. If you then have another cloud drive tool installed, those slots can be used up. TortoiseGit and TortoiseSVN try to be a "Good Citizen (TM)" and limit its use of overlays to give other apps a chance.</p>
				<ul class="ul">
					<li>Normal, Modified and Conflicted are always loaded and visible (if possible!).</li>
					<li>Deleted is loaded if possible, but falls back to Modified if there are not enough slots.</li>
					<li>ReadOnly is loaded if possible, but falls back to Normal if there are not enough slots.</li>
					<li>Locked is only loaded if there are less than 13 overlays already loaded. It falls back to Normal if there are not enough slots.</li>
					<li>Added is only loaded if there are less than 14 overlays already loaded. It falls back to Modified if there are not enough slots.</li>
				</ul>
				<p>You can check which other apps are using overlays by using regedit to look at <code>HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\ShellIconOverlayIdentifiers</code></p>
				<p>Other apps which are known to use overlays:
					<ul class=ul>
						<li>Windows itself. Vista and Win7 use more than XP.</li>
						<li>SkyDrive</li>
						<li>OneDrive</li>
						<li>GDrive</li>
						<li>Mega</li>
						<li>Dropbox</li>
						<li>OwnCloud</li>
						<li>many others&hellip;</li>
					</ul>
				</p>
				<p>If there are too many overlay handlers installed and TortoiseGit does not show any overlays, you can try to delete some of the installed handlers from the registry. But be careful when editing the registry!</p>
				<p>Deletion is sometimes a bit tricky. You can also try to prefix the Tortoise* entries with spaces and/or double quotes (&quot;).</p>
				<p>Please also see <a href="/issue/692">issue #692</a> and <a href="/issue/2548">issue #2548</a> for more user reports.</p>
				<p>Based on a similar <a href="http://tortoisesvn.net/faq.html#ovlnotall" target="_blank" rel="nofollow">TortoiseSVN FAQ entry</a></p>
				<p><a href="#top">&uarr; Top</a></p>

				<h3 id="ovlonlylocal">Why are the icons only visible on local and not on network drives?</h3>
				<p>Go to the Settings -> Look and Feel -> Icon Overlays and check the drive types for which you want to see overlay icons. Be aware that enabling overlays for network drives will slow down not only TortoiseGit but the whole system.</p>
				<p>Based on a similar <a href="http://tortoisesvn.net/faq.html#ovlonlylocal" target="_blank" rel="nofollow">TortoiseSVN FAQ entry</a></p>
				<p><a href="#top">&uarr; Top</a></p>
			</div>
		</div>

		<div class="container_white">
			<div class="wrap_content contentpage">
				<h2>How can I&hellip;</h2>
				<h3 id="howremembercredentials">&hellip; make TortoiseGit remember HTTP(s) crecentials?</h3>
				<p>There is a very good answer on <a href="http://stackoverflow.com/q/14000173/3906760" target="_blank">StackOverflow</a>.</p>
				<p><a href="#top">&uarr; Top</a></p>

				<h3 id="howputtygitcli">&hellip; use PuTTY for git cli and git bash?</h3>
				<p>Create an environment variable called "GIT_SSH" with the path to the PuTTY plink.exe or preferably to TortoiseGitPLink.exe. This can be done on the command line by executing "set GIT_SSH=PATH_TO_PLINK.EXE" ("C:\Program Files\TortoiseGit\bin\TortoiseGitPLink.exe" on default installations) or <a href="http://www.computerhope.com/issues/ch000549.htm" target="_blank" rel="nofollow">permanently</a>.</p>
				<p><a href="#top">&uarr; Top</a></p>

				<h3 id="howselfsignedcert">&hellip; close/push/pull from a server with a self signed or &quot;invalid&quot; certificate?</h3>
				<p>There is a very good answer on <a href="http://stackoverflow.com/a/26128676/3906760" target="_blank">StackOverflow</a>.</p>
				<p><a href="#top">&uarr; Top</a></p>
			</div>
		</div>
		

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h2>General questions</h2>
				<h3 id="sshconfigignored">~/.ssh/config seems to be ignored by TortoiseGit</h3>
				<p>TortoiseGit uses TortoiseGitPlink by default as SSH client. PuTTY and TortoiseGitPlink do not respect ~/.ssh/config (which is the config file for OpenSSH). Both tools store, like Windows tools, their configuration in registry.</p>
				<p>Please see the <a href="/docs/tortoisegit/tgit-ssh-howto.html">TortoiseGit Manual - Tips and tricks for SSH/PuTTY</a> for configuration hints.</p>
				<p>If you want to use OpenSSH, set the ssh client to ssh.exe in TortoiseGit network settings (there is no need to use the full path to "ssh.exe" as it is on the %PATH%).</p>
				<p>Based on an answer on <a href="http://stackoverflow.com/a/32115724/3906760" target="_blank">StackOverflow</a>.</p>
				<p><a href="#top">&uarr; Top</a></p>

				<h3 id="tortoisegitblamewronglanguage">TortoiseGitBlame shows menu in wrong language</h3>
				<p>You have to reset the menu bar (even if you did not make any changes to it). Left click -&gt; Customize -&gt; Menu tab and hit Reset/Restore.</p>
				<p><a href="#top">&uarr; Top</a></p>
			</div>
		</div>

		<div id="space"></div>
		<div id="container_footer">
			<div class="wrap_content">
		<?php printFooter(); ?>
			</div>
		</div>
	</div>
	</body>
</html>