<?php
require(__DIR__.'/../inc/head.php');
printHead('Support');
?>
	<body>
	<div id=wrapper>
		<div id="container_headline">
			<?php printHeadLine(3); ?>
		</div>

		<div class="container_seperator">
			<div class="wrap_content breadcrumbs">
				<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">TortoiseGit.org</span></a></span> &raquo; Support
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h1>Support</h1>

				<p>There are several places where you can get support for TortoiseGit.</p>

				<ul class="ul">
					<li><a href="/docs/">Manuals</a></li>
					<li><a href="faq/">FAQ</a></li>
					<li><a href="/issues" target="_blank" class="gitlab">Issue tracker on GitLab</a>, but before reporting an issue please see <a href="#howtoreport">here ("What to do if things go wrong&hellip;")</a></li>
					<li>Mailing lists:
						<ul class="ul">
							<li><a href="https://groups.google.com/group/tortoisegit-announce" target="_blank">tortoisegit-announce</a> (very low traffic, we announce new releases here),</li>
							<li><a href="https://groups.google.com/group/tortoisegit-users" target="_blank">tortoisegit-users</a> and</li>
							<li><a href="https://groups.google.com/group/tortoisegit-dev" target="_blank">tortoisegit-dev</a></li>
						</ul>
					</li>
					<li>StackOverflow tag: <a href="http://stackoverflow.com/questions/tagged/tortoisegit" target="_blank">tortoisegit</a></li>
				</ul>

				<p>Please do not mail the TortoiseGit developers directly for support requests.</p>

				<h2 id="howtoreport">What to do if things go wrong or a crash happened?</h2>
				<p>Before reporting an issue, please <a href="/issues" class="gitlab" target="_blank">search</a> whether a similar issue or <a href="faq/">FAQ entry</a> already exists and check that your problem isn't fixed in our latest <a href="//download.tortoisegit.org/tgit/previews/">preview release</a>.</p>

				<p>An important aspect of reporting issues it to have a reproducible way for the issue and also to mention your exact version of your operating system, the version of Git and the version of TortoiseGit (this information can be found on the TortoiseGit about dialog).</p>

				<p>TortoiseGit includes a crash reporter (if not disabled on installation), which automatically uploads crash dumps to drdump.com, where the TortoiseGit team can review them. If you have a reproducible way, please also file an issue and link the crash report.</p>

				<p>We have a special <a href="https://gitlab.com/tortoisegit/tortoisegit/blob/master/src/Debug-Hints.txt" target="_blank" class="gitlab">page describing steps for debugging</a>, where the majority of ways does not require to build TortoiseGit on your own.</p>
			</div>
		</div>
		<div id="space"></div>
		<div id="container_footer">
			<div class="wrap_content">
<?php printFooter(); ?>
			</div>
		</div>
	</div>
	</body>
</html>