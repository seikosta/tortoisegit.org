<?php
require(__DIR__.'/../../../inc/head.php');
printHead('Crash caused by a third party module', true);
?>
	<body>
	<div id=wrapper>
		<div id="container_headline">
			<?php printHeadLine(); ?>
		</div>

		<div class="container_seperator">
			<div class="wrap_content breadcrumbs">
				<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">TortoiseGit.org</span></a></span> &raquo; <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/support/" itemprop="url"><span itemprop="title">Support</span></a></span> &raquo; Crash caused by a third party module
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h1>CrashReport: Crash caused by a third party module</h1>

				<p>The crash you experienced wasn't caused by TortoiseGit, but by some issue in some third party module.</p>
				<p>You might find an indication which tool caused this when you inspect the stack trace on the page which redirected you here.</p>
				<p>We're sorry about this, however, there is nothing we can do about this.</p>
			</div>
		</div>
		<div id="space"></div>
		<div id="container_footer">
			<div class="wrap_content">
<?php printFooter(); ?>
			</div>
		</div>
	</div>
	</body>
</html>