<?php
require(__DIR__.'/../../../inc/head.php');
printHead('In-Page-Error', true);
?>
	<body>
	<div id=wrapper>
		<div id="container_headline">
			<?php printHeadLine(); ?>
		</div>

		<div class="container_seperator">
			<div class="wrap_content breadcrumbs">
				<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">TortoiseGit.org</span></a></span> &raquo; <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/support/" itemprop="url"><span itemprop="title">Support</span></a></span> &raquo; In-Page-Error
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h1>CrashReport: In-Page-Error</h1>

				<p>The crash was caused by an In-Page-Error. This can have several causes:
				<ul class="ul">
					<li>Hard drive corruption: Check your hard-drive.</li>
					<li>Memory corruption: Check your memory for errors.</li>
					<li>If your repository is on a network share: don't do that or fix the network.</li>
				</ul>
				</p>
				<p>We're sorry about this, however, there is nothing we can do about this.</p>
			</div>
		</div>
		<div id="space"></div>
		<div id="container_footer">
			<div class="wrap_content">
<?php printFooter(); ?>
			</div>
		</div>
	</div>
	</body>
</html>