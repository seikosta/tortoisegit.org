<?php
require(__DIR__.'/../../../inc/head.php');
printHead('Crash report without usable data', true);
?>
	<body>
	<div id=wrapper>
		<div id="container_headline">
			<?php printHeadLine(); ?>
		</div>

		<div class="container_seperator">
			<div class="wrap_content breadcrumbs">
				<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">TortoiseGit.org</span></a></span> &raquo; <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/support/" itemprop="url"><span itemprop="title">Support</span></a></span> &raquo; Crash report without usable data
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h1>CrashReport: Crash report without usable data</h1>

				<p>Thank you for reporting this crash. We analyzed your report, however, the crash report did not contain enough data to spot the cause of this issue.</p>
				<p>Please make sure that this issue is not caused by a broken repository. You might want to execute <i>git <a href="https://www.kernel.org/pub/software/scm/git/docs/git-fsck.html">fsck</a></i> to check it.</p>
				<p>If you can reproduce the issue, please keep a copy of the current state of your repository and open an <a href="/support/#howtoreport">issue</a> mentioning the problem ID of your report (previous page) if the issue is not already reported or known in our <a href="/support/faq/">FAQ</a>.</p>
				<p>We're sorry about this, however, without more data there is nothing we can do about this. </p>
			</div>
		</div>
		<div id="space"></div>
		<div id="container_footer">
			<div class="wrap_content">
<?php printFooter(); ?>
			</div>
		</div>
	</div>
	</body>
</html>