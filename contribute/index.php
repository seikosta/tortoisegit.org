<?php
require(__DIR__.'/../inc/head.php');
printHead('Contribute');
?>
	<body>
	<div id=wrapper>
		<div id="container_headline">
			<?php printHeadLine(4); ?>
		</div>

		<div class="container_seperator">
			<div class="wrap_content breadcrumbs">
				<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">TortoiseGit.org</span></a></span> &raquo; Contribute
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h1>How Can I Contribute?</h1>

				<p>You can consider a <a href="/donate/">donation</a>. But it's not all about money. We really appreciate if you donate money, however, manpower is also needed!</p>

				<p>You're welcome to contribute to this project! There are several aspects you can help on:</p>
				<ul class="ul">
					<li>improving our <a href="https://tortoisegit.org/docs/">documentation</a> (see <a href="https://gitlab.com/tortoisegit/tortoisegit/blob/master/doc/readme.txt" target="_blank" class="gitlab">doc/readme.txt</a> file and <a href="https://gitlab.com/tortoisegit/tortoisegit/tree/master/doc" target="_blank" class="gitlab">doc</a> folder),</li>
					<li><a href="https://gitlab.com/tortoisegit/tortoisegit/blob/master/Languages/README.txt" target="_blank" class="gitlab">translations</a>,</li>
					<li>testing <a href="https://download.tortoisegit.org/tgit/previews/">preview releases</a>,</li>
					<li>helping other users on the <a href="/support/">mailing lists</a>,</li>
					<li>improving our UIs, or also</li>
					<li>coding (e.g., fix open issues or implement new features; see <a href="#how-to-build">below</a> for more information).
						<ul class="ul">
							<li>Our main source code repository is hosted on GitLab: <a href="https://gitlab.com/tortoisegit/tortoisegit/" target="_blank">https://gitlab.com/tortoisegit/tortoisegit/</a>; forking, creating issues and merge requests possible</li>
							<li>We also have a mirror on GitHub: <a href="https://github.com/tortoisegit/tortoisegit/" target="_blank">https://github.com/tortoisegit/tortoisegit/</a>, forking and creating pull requests possible</li>
						</ul>
					</li>
				</ul>

				<p>Any help is appreciated!</p>

				<p>Feel free to report issues and open merge requests.</p>

				<p>Please also check the <a href="https://gitlab.com/tortoisegit/tortoisegit/blob/master/doc/HowToContribute.txt" target="_blank" class="gitlab">contribution guidelines</a> to understand our workflow.</p>

				<h2 id="how-to-build">How to build</h2>
				<p>Building TortoiseGit normally is not necessary, however, it is easy. All necessary requirements and steps are described in the <a href="https://gitlab.com/tortoisegit/tortoisegit/blob/master/build.txt" target="_blank" class="gitlab">build.txt</a> file. Helpful might also be our short description in the <a href="https://gitlab.com/tortoisegit/tortoisegit/blob/master/architecture.txt" target="_blank" class="gitlab">architecture.txt</a> file.</p>
			</div>
		</div>
		<div id="space"></div>
		<div id="container_footer">
			<div class="wrap_content">
<?php printFooter(); ?>
			</div>
		</div>
	</div>
	</body>
</html>