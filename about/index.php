<?php
require(__DIR__.'/../inc/head.php');
printHead('About');
?>
	<body>
	<div id=wrapper>
		<div id="container_headline">
			<?php printHeadLine(1); ?>
		</div>

		<div class="container_seperator">
			<div class="wrap_content breadcrumbs">
				<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">TortoiseGit.org</span></a></span> &raquo; About
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h1>About TortoiseGit</h1>
				<p>TortoiseGit is a Windows Shell Interface to Git and based on TortoiseSVN. It's open source and can fully be build with freely available software.</p>
				<p>Since it's not an integration for a specific IDE like Visual Studio, Eclipse or others, you can use it with whatever development tools you like, and with any type of file. Main interaction with TortoiseGit will be using the context menu of the Windows explorer.</p>
				<p>TortoiseGit supports you by regular tasks, such as committing, showing logs, diffing two versions, creating branches and tags, creating patches and so on (see our <a href="/about/screenshots/">Screenshots</a> or <a href="/docs/">documentation</a>).</p>
				<p>It is developed under the <a href="https://www.gnu.org/licenses/gpl-2.0" target="_blank">GPL</a>. Which means it is completely free for anyone to use, including in a commercial environment, without any restriction. The source code is also freely available, so you can even develop your own version if you wish to.</p>
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h2 class=title>Features of TortoiseGit</h2>
				<ul class="ul">
					<li>Easy to use
						<ul class="ul">
							<li>all commands are available directly from the Windows Explorer (<a href="screenshots/#Explorer_integration">see screenshots</a>).</li>
							<li>only commands that make sense for the selected file/folder are shown. You won't see any commands that you can't use in your situation.</li>
							<li>See the status of your files directly in the Windows explorer  (<a href="screenshots/#Overlay_icons_in_explorer">see screenshots</a>)</li>
							<li>descriptive dialogs, constantly improved due to user feedback</li>
							<li>allows moving files by right-dragging them in the Windows explorer</li>
						</ul>
					</li>
					<li>Powerful commit dialog (<a href="screenshots/#Commit_Dialog">see screenshots</a>)
						<ul class="ul">
							<li>integrated spell checker for log messages</li>
							<li>auto completion of paths and keywords of the modified files</li>
							<li>text formatting with special chars</li>
						</ul>
					</li>
					<li>Per project settings
						<ul class="ul">
							<li>minimum log message length to avoid accidentally committing with an empty log message</li>
							<li>language to use for the spell checker</li>
						</ul>
					</li>
					<li>Integration with issue tracking systems<br>
						TortoiseGit provides a flexible mechanism to integrate any web based bug tracking system.
						<ul class="ul">
							<li>A separate input box to enter the issue number assigned to the commit, or coloring of the issue number directly in the log message itself</li>
							<li>When showing all log messages, an extra column is added with the issue number. You can immediately see to which issue the commit belongs to.</li>
							<li>Issue numbers are converted into links which open the webbrowser directly on the corresponding issue</li>
							<li>Optional warning if a commit isn't assigned to an issue number</li>
						</ul>
					</li>
					<li>Helpful Tools
						<ul class="ul">
							<li>TortoiseGitMerge (<a href="screenshots/#TortoiseGitMerge">see screenshot</a> and the <a href="/docs/tortoisegitmerge/">TortoiseGitMerge manual</a>)
								<ul class="ul">
									<li>Shows changes you made to your files</li>
									<li>Helps resolving conflicts</li>
									<li>Can apply patchfiles you got from users without commit access to your repository</li>
								</ul>
							</li>
							<li>TortoiseGitBlame: to show blames of files. Shows also log messages for each line in a file. (<a href="screenshots/#TortoiseGitBlame">see screenshot</a>)</li>
							<li>TortoiseGitIDiff: to see the changes you made to your image files (<a href="screenshots/#TortoiseGitMerge">see screenshot</a>)</li>
						</ul>
					</li>
					<li>Available in <a href="/download/#Language_Packs">many languages</a></li>
					<li>TortoiseGit is stable
						<ul class="ul">
							<li>Before every release, we create one or more <a href="//download.tortoisegit.org/tgit/previews/">preview releases</a> for "adventurous" people to test first.  This helps finding bugs very early so they won't even get into an official release.</li>
							<li>A custom crash report tool is included in every TortoiseGit release which helps us fix the bugs much faster, even if you can't remember exactly what you did to trigger it.</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>

		<div id="space"></div>
		<div id="container_footer">
			<div class="wrap_content">
<?php printFooter(); ?>
			</div>
		</div>
	</div>
	</body>
</html>