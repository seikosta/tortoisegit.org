<?php
require(__DIR__.'/../../inc/head.php');
printHead('Screenshots');
?>
	<body>
	<div id=wrapper>
		<div id="container_headline">
			<?php printHeadLine(); ?>
		</div>

		<div class="container_seperator">
			<div class="wrap_content breadcrumbs">
				<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">TortoiseGit.org</span></a></span> &raquo; <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/about/" itemprop="url"><span itemprop="title">About</span></a></span> &raquo; Screenshots
			</div>
		</div>

		<div class="container_white">
			<div class="wrap_content contentpage">
				<h1>Screenshots</h1>
				<p>Find a list of screenshots of common dialogs here.</p>
				<ul class="ul menubox">
					<li><a href="#Explorer_integration">Explorer integration</a>
						<ul class="ul">
							<li><a href="#(Configurable)_Context_menu">(Configurable) Context menu</a></li>
							<li><a href="#Overlay_icons_in_explorer">Overlay icons in explorer</a></li>
							<li><a href="#Integration_in_properties">Integration in properties</a></li>
						</ul>
					</li>
					<li><a href="#Common_tasks">Common tasks</a>
						<ul class="ul">
							<li><a href="#Cloning">Cloning</a></li>
							<li><a href="#Commit_Dialog">Commit Dialog</a></li>
							<li><a href="#Push_Dialog">Push Dialog</a></li>
							<li><a href="#Sync_Dialog">Sync Dialog</a></li>
							<li><a href="#Log_Dialog">Log Dialog</a></li>
							<li><a href="#Compare_(Revisions)_Dialog">Compare (Revisions) Dialog</a></li>
							<li><a href="#Repository_Browser">Repository Browser</a></li>
							<li><a href="#Revision_Graphic">Revision Graphic</a></li>
							<li><a href="#Merge_Dialog">Merge Dialog</a></li>
							<li><a href="#Conflict_handling">Conflict handling</a></li>
							<li><a href="#Rebase_Dialog">Rebase Dialog</a></li>
							<li><a href="#TortoiseGitBlame">VS Style Blame</a></li>
							<li><a href="#Handling_patches">Handling patches</a></li>
							<li><a href="#Handling_Submodules">Handling Submodules</a></li>
							<li><a href="#TortoiseGitMerge">TortoiseGitMerge: Diffing/Merging files/patches</a></li>
							<li><a href="#Browse_references">Browse references</a></li>
							<li><a href="#Supports_bisecting">Supports bisecting</a></li>
						</ul>
					</li>
				</ul>

				<h2 id="Explorer_integration">Explorer integration</h2>

				<h3 id="(Configurable)_Context_menu">(Configurable) Context menu</h3>
				<p>The context menu is the main way of interacting with TortoiseGit. In the following image you see several possible options. The options shown are tailored to the current context (e.g., "Add" is only visible for unversioned files and "Diff" is not visible here is only shown for modified files) - you won't see any commands that you can't use in your situation. There are also enhanced drag'n'drop context menus.</p>
				<p><img src="/docs/tortoisegit/images/ContextMenuDirControl.png" alt="Context menu example"><br><a href="#top">&uarr; Top</a></p>
				<p>Tailored context menu for (ignored) .patch files:<br><img src="sendmail.png" alt="Context menu example for patch files"><br><a href="#top">&uarr; Top</a></p>
				<p>Drag'n'drop context menu:<br><img src="drag-contextmenu.png" alt="Drag'n'drop context menu"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Overlay_icons_in_explorer">Overlay icons in explorer indicating the status of files and folders</h3>
				<p><img src="/docs/tortoisegit/images/Overlays.png" alt="Overlay icons"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Integration_in_properties">Integration in properties</h3>
				<p><img src="/docs/tortoisegit/images/Properties.png" alt="Explorer properties dialog"><br><a href="#top">&uarr; Top</a></p>

				<h2 id="Common_tasks">Common tasks</h2>
				<h3 id="Cloning">Cloning</h3>
				<p><img src="/docs/tortoisegit/images/GitClone.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Commit_Dialog">Commit Dialog</h3>
				<p>Support spell checking (English + different languages based on installed language packs), basic styling and, autocompletion of filenames as well as method and variable names for various programming languages</p>
				<p><img src="/docs/tortoisegit/images/Commit.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Push_Dialog">Push Dialog</h3>
				<p><img src="/docs/tortoisegit/images/GitPush.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Sync_Dialog">Sync Dialog</h3>
				<p><img src="/docs/tortoisegit/images/GitSync.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Log_Dialog">Log Dialog</h3>
				<p><img src="/docs/tortoisegit/images/LogMessages.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Compare_(Revisions)_Dialog">Compare (Revisions) Dialog</h3>
				<p><img src="/docs/tortoisegit/images/CompareRevisions.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Repository_Browser">Repository Browser</h3>
				<p><img src="/docs/tortoisegit/images/RepoBrowser.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Revision_Graphic">Revision Graphic</h3>
				<p><img src="/docs/tortoisegit/images/RevisionGraph.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Merge_Dialog">Merge Dialog</h3>
				<p><img src="/docs/tortoisegit/images/Merge.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Conflict_handling">Conflict handling</h3>
				<p><img src="/docs/tortoisegit/images/ResolveConflict.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Rebase_Dialog">Rebase Dialog</h3>
				<p><img src="/docs/tortoisegit/images/GitRebase.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="TortoiseGitBlame">VS Style Blame</h3>
				<p><img src="/docs/tortoisegit/images/TortoiseBlame.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Handling_patches">Handling patches</h3>
				<p><img src="/docs/tortoisegit/images/CreatePatch.png"><br><a href="#top">&uarr; Top</a></p>
				<p><img src="sendmail.png"><br><a href="#top">&uarr; Top</a></p>
				<p><img src="/docs/tortoisegit/images/SendPatch.png"><br><a href="#top">&uarr; Top</a></p>
				<p><img src="/docs/tortoisegit/images/ApplyPatch.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Handling_Submodules">Handling Submodules</h3>
				<p><img src="/docs/tortoisegit/images/SubmoduleDiff.png"><br><a href="#top">&uarr; Top</a></p>
				<p><img src="/docs/tortoisegit/images/SubmoduleUpdate.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="TortoiseGitMerge">TortoiseGitMerge: Diffing/Merging files/patches</h3>
				<p>TortoiseGitMerge can open Git patch file directly, you review it and patch to working copy.</p>
				<p><img src="/docs/tortoisegit/images/TMerge_TwoPane.png"><br><a href="#top">&uarr; Top</a></p>
				<p><img src="/docs/tortoisegit/images/TortoiseIDiff.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Browse_references">Browse references</h3>
				<p><img src="/docs/tortoisegit/images/BrowseRef.png"><br><a href="#top">&uarr; Top</a></p>

				<h3 id="Supports_bisecting">Supports bisecting</h3>
				<p><img src="/docs/tortoisegit/images/bisect.png"> <img src="/docs/tortoisegit/images/ContextMenuBisect.png"><br><a href="#top">&uarr; Top</a></p>
			</div>
		</div>
		<div id="space"></div>
		<div id="container_footer">
			<div class="wrap_content">
<?php printFooter(); ?>
			</div>
		</div>
	</div>
	</body>
</html>