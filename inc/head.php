<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

header("Content-Type: text/html; charset=utf-8");
function get_root() { return ""; }

function printHead($pageTitle='', $robotsNoIndex = false) {
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" href="<? echo get_root(); ?>/css/combined-201509040.min.css" type="text/css">
		<link rel="shortcut icon" href="<? echo get_root(); ?>/favicon.ico">
		<title><?php if ($pageTitle != '') { echo $pageTitle.' – '; } ?>TortoiseGit – Windows Shell Interface to Git</title>
		<meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html;charset=utf-8">
		<?php
		if ($pageTitle=='' && !$robotsNoIndex) {
			//<meta name="description" content="" >
			echo '<meta name="keywords" content="TortoiseGit,Download,Git,Windows,ShellExtension">'."\n";
		} ?>
		<meta name="MSSmartTagsPreventParsing" content="TRUE">
		<meta name="copyright" content="TortoiseGit team">
<?php if ($robotsNoIndex) { ?>
		<meta name="robots" content="noindex, follow">
<?php } else { ?>
		<meta name="robots" content="index, follow">
<?php } ?>
	</head>
<?php
}

function printFooter() {
	echo '<p class="center copyright">&copy; 2015 TortoiseGit and contributors; Patches, suggestions and comments for this web site are <a href="https://gitlab.com/tortoisegit/tortoisegit.org" target="_blank" class="gitlab">welcome on GitLab</a>.</p>';
}

function printHeadLine($active=0) {
?>
			<div id="wrap_headline">
				<div id="spacehack"></div>
				<a href="/" id="link_logo"></a>
				<div id="menutoggle">
					<label for="nav-trigger"><svg xmlns="http://www.w3.org/2000/svg" height="2.5em" width="2.5em" viewBox="0 0 440 365"><rect height="75" width="440" y="0" x="0"/><rect height="75" width="440" y="145" x="0"/><rect height="75" width="440" y="290" x="0"/></svg></label>
				</div>
				<input type="checkbox" id="nav-trigger" autocomplete="off">
				<ul id="menu">
					<li class="menu_item"><a href="/about/" class="menu_<?php if($active==1) { echo 'active'; } else { echo 'link'; } ?>">About</a></li>
					<li class="menu_item"><a href="/download/" class="menu_<?php if($active==2) { echo 'active'; } else { echo 'link'; } ?>">Download</a></li>
					<li class="menu_item"><a href="/support/" class="menu_<?php if($active==3) { echo 'active'; } else { echo 'link'; } ?>">Support</a> <a class="sub <?php if($active==3) { echo 'menu_active'; } ?>" href="#"><svg width="1em" height="1em" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path fill="currentColor" d="M1683 808l-742 741q-19 19-45 19t-45-19l-742-741q-19-19-19-45.5t19-45.5l166-165q19-19 45-19t45 19l531 531 531-531q19-19 45-19t45 19l166 165q19 19 19 45.5t-19 45.5z"/></svg></a>
						<ul>
							<li><a href="/support/" class="menu_<?php if($active==3) { echo 'active'; } else { echo 'link'; } ?>">Support</a></li>
							<li><a href="/support/faq/" class="menu_link">FAQ</a></li>
							<li><a href="/docs/" class="menu_link">Manuals</a></li>
						</ul>
					</li>
					<li class="menu_item"><a href="/contribute/" class="menu_<?php if($active==4) { echo 'active'; } else { echo 'link'; } ?>">Contribute</a></li>
				</ul>
			</div>
<?php
}
?>