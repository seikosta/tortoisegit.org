<?php
require(__DIR__.'/../inc/head.php');
printHead('Documentation');
?>
	<body>
	<div id=wrapper>
		<div id="container_headline">
			<?php printHeadLine(); ?>
		</div>

		<div class="container_seperator">
			<div class="wrap_content breadcrumbs">
				<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">TortoiseGit.org</span></a></span> &raquo; Documentation
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h1>Documentation</h1>

				<h2>Manuals</h2>
				<p>The are two manuals available:</p>

				<ul class="ul">
					<li><a href="/docs/tortoisegit/">TortoiseGit (in general, includes a daily usage guide)</a></li>
					<li><a href="/docs/tortoisegitmerge/">TortoiseGitMerge</a></li>
				</ul>

				<p>If you need help or more specific support please see our <a href="/support/">support page</a> and our <a href="/support/faq/">FAQ</a>.</p>

				<h2>Developer documentation</h2>
				<p>Information for developers can be found on the <a href="/contribute/">contribute page</a>.</p>
			</div>
		</div>
		<div id="space"></div>
		<div id="container_footer">
			<div class="wrap_content">
<?php printFooter(); ?>
			</div>
		</div>
	</div>
	</body>
</html>