<?php
require(__DIR__.'/../../inc/head.php');
printHead('Release notes');
?>
	<body>
	<div id=wrapper>
		<div id="container_headline">
			<?php printHeadLine(); ?>
		</div>

		<div class="container_seperator">
			<div class="wrap_content breadcrumbs">
				<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">TortoiseGit.org</span></a></span> &raquo; <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/docs/" itemprop="url"><span itemprop="title">Documentation</span></a></span> &raquo; Release notes
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h1>Release notes</h1>
<pre style="font-family: monospace;white-space: pre-wrap;">
<a name="Release_1.8.15.0">Release 1.8.15.0</a>
Released: 2015-08-02

== Features ==
 * Fixed <a href="/issue/2245">issue #2245</a>: Make the meaning of "Squash" clearer
 * Fixed <a href="/issue/2488">issue #2488</a>: BrowseRefsDlg: Option to hide nested branches
 * Fixed <a href="/issue/2490">issue #2490</a>: BrowseRefsDlg: Allow to configure tracked branches
 * Fixed <a href="/issue/2469">issue #2469</a>: Support reviewing merge commits (git show merge-commit)
 * Add full support for Git for Windows 2.x and windows-wide system config
   (<a href="https://git-for-windows.github.io" target="_blank">https://git-for-windows.github.io</a>)
 * Fixed <a href="/issue/2232">issue #2232</a>: Change Show Log to display all lines of multiline commit comments in the message column
 * Fixed <a href="/issue/1930">issue #1930</a>: Navigate directly to a commit in "Show log" by pasting its hash
 * Fixed <a href="/issue/2280">issue #2280</a>: Ability to use Msys2 git instead of Git for Windows
   Check out the manual (keyword "Msys2 git") regarding Msys2 Git support and configuration hints.
   Please note that Msys2 Git is not officially supported by TortoiseGit as the developers only use Git for Windows. Bug reports, however, are welcome.
 * Fixed <a href="/issue/1003">issue #1003</a>: Show Log does not remember the from-date (configurable now)
 * Fixed <a href="/issue/1793">issue #1793</a>: Fetch&Rebase: Launch rebase only if the branch isn't up to date
 * Fixed <a href="/issue/1886">issue #1886</a>: Fetch&Rebase should allow automatic branch update if FF
 * Fixed <a href="/issue/836">issue #836</a>: CommitDlg: Do not auto-select files which are deleted in file system, but are not deleted in the index on commit dialog (optional)
 * Fixed <a href="/issue/2513">issue #2513</a>: TortoiseGitUDiff: Add keyboard shortcuts

== Bug Fixes ==
 * Fixed <a href="/issue/2495">issue #2495</a>: "Show Reflog" dialog shows empty action for "push" entries
 * Fixed <a href="/issue/2500">issue #2500</a>: Amend with date reset causes an error with git 2.x
 * Fixed <a href="/issue/2507">issue #2507</a>: Support keyboard shortcuts in yes/no prompts
 * Fixed <a href="/issue/2496">issue #2496</a>: LogDlg: Controls may overlap on screen resolution change
 * Fixed <a href="/issue/2153">issue #2153</a>: Log window resize should expand commit list
 * Fixed <a href="/issue/2509">issue #2509</a>: Prompt for stash before rebase is not matched with pop stash  after rebase
 * Fixed <a href="/issue/2510">issue #2510</a>: Non-correctly visualized settings dialog on modern high-DPI displays
 * Fixed <a href="/issue/2514">issue #2514</a>: diff uses wrong tool in case the filename matches an extension for which a special diff tool is registerd
 * Fixed <a href="/issue/2487">issue #2487</a>: Show log dialog lose focus of "working dir changes" after content refresh
 * Fixed <a href="/issue/2517">issue #2517</a>: Submodule Update dialog cannot be re-sized
 * Fixed <a href="/issue/2504">issue #2504</a>: Problems with MS Outlook and generated emails
 * Optimized TortoiseGitBlame to be used with high contrast themes and black background colors
 * Fixed <a href="/issue/2529">issue #2529</a>: The status of files which are not checked should be the same in the index after commit
   Unchecked deleted/renamed files were not put into previous state in index after commit
 * Fixed <a href="/issue/2537">issue #2537</a>: TortoiseGitPlink will not connect to OpenSSH 6.9
   Upgraded TortoiseGitPlink to version 0.65
 * Shell(Extended): Fix mismatch of directory and file status; Only staged changes Were considered for the calculation of the overlay icons of directories.
 * Fixed <a href="/issue/2546">issue #2546</a>: Location bar in TortoiseGitMerge missing
 * Fixed <a href="/issue/2545">issue #2545</a>: Incorrect TortiseGitMerge behavior when ignore comments is on
 * Fixed <a href="/issue/2544">issue #2544</a>: LogDlg giving inconsistent results with rename and merge files with follow renames
 * Fixed <a href="/issue/2479">issue #2479</a>: TortoiseGit does not work in hard link folder

<a name="Release_1.8.14.0">Release 1.8.14.0</a>
Released: 2015-04-08

== Features ==
 * Fixed <a href="/issue/2441">issue #2441</a>: A way to disable the sound that is played on errors
 * Updated shipped TortoiseGitPlink and putty binaries to version 0.64
 * Fixed <a href="/issue/2466">issue #2466</a>: TortoiseGitUDiff tool cannot be localized
 * TGitCache: Improve reaction time for propagating changes to explorer
 * Fixed <a href="/issue/2031">issue #2031</a>: Missing option to disable displaying "FETCH_HEAD" in "Switch/Checkout" dialog box
 * Log, RepoBrowser, and Rebase Dialogs: Allow to compare arbitrary files across commits
 * Fixed <a href="/issue/2534">issue #2534</a>: Use the current time for amending commit (also sets commit time)
 * TortoiseGitBlame: Implemented search forward (F3) and search backward (Shift+F3)
 * Allow to compare file with deleted file in CommitDlg (e.g., useful for renames)
 * Allow to use Explorer context menu options from TortoiseGit context menu
 * Sort remotes logically/case-insensitively

== Bug Fixes ==
 * Fixed <a href="/issue/2427">issue #2427</a>: "Submodule of Project: " cannot be localized
 * Fixed <a href="/issue/2432">issue #2432</a>: MessageBox buttons cannot be localized
 * Fixed <a href="/issue/2433">issue #2433</a>: Cannot see the log in Diff with previous version
 * Fixed <a href="/issue/2453">issue #2453</a>: The overlay icon of committed files in a submodule named with Chinese characters is unversioned icon
 * Fixed <a href="/issue/2455">issue #2455</a>: "Diff with ..." menu cannot be localized
 * Fixed <a href="/issue/2431">issue #2431</a>: Add fails in submodule after upgrade from 1.8.12 to 1.8.13
 * Fixed <a href="/issue/2449">issue #2449</a>: wrong column info in tag reference dialog
 * Fixed <a href="/issue/2436">issue #2436</a>: Overlay icons gradually disappear or show files as unversioned after a short time
 * Fixed <a href="/issue/2374">issue #2374</a>: Rebase option in config not working anymore for pull
 * Fixed <a href="/issue/1440">issue #1440</a>: Different status of modified renamed or moved files in Show log dialog, Check for Modifications dialog and Commit dialog
 * Fixed <a href="/issue/1501">issue #1501</a>: Unchanged files appears modified in Show Log dialog
 * Fixed <a href="/issue/2477">issue #2477</a>: Change "Split commit" label to "Edit/Split commit" in Rebase dlg
 * Fixed <a href="/issue/2481">issue #2481</a>: "Start registry editor" does not work properly in non-English locales
 * Fixed <a href="/issue/2480">issue #2480</a>: CommitDlg: auto-complete doesn't include removed items
 * Fixed <a href="/issue/2451">issue #2451</a>: Cleanup fails quietly if file is in use
 * Various bug fixes based on our newly added unit tests
 * Not all remotes showing in Push/Pull/Fetch/Sync dialogs (was limited to 25)

<a name="Release_1.8.13.0">Release 1.8.13.0</a>
Released: 2015-02-15

== Features ==
 * Improved auto update
   Version checks will use SSL (on Vista or newer systems)
   New version.txt file format with more options
 * Fixed <a href="/issue/2243">issue #2243</a>: Submodule red-icon doesn't propagate to higher folders (configurable now)
 * Improved overlay icon accuracy
   You might need to kill TGitCache.exe once using the TaskManager in order to reset its cache
 * Fixed <a href="/issue/2388">issue #2388</a>: See the exact time when the Relative time in log enabled
 * Fixed <a href="/issue/2346">issue #2346</a>: Add drag'n'drop to "Apply patches" window
 * Fixed <a href="/issue/2357">issue #2357</a>: Save the state of Search criteria checkboxes in Log dialog
 * Fixed <a href="/issue/2420">issue #2420</a>: Make push available in log for arbitrary commits (when pressing the shift key)
 * Fixed <a href="/issue/2351">issue #2351</a>: Allow upstream to be separate from onto in rebase
 * Fixed <a href="/issue/1400">issue #1400</a>: Allow the use of tags within the Git Rebase dialog
 * Fixes <a href="/issue/2417">issue #2417</a>: Resolving a conflict forces refresh of Commit Dialog
   A new advanced option (RefreshFileListAfterResolvingConflict) was added which controls
   whether file lists are refreshed after resolving conflicts. By default this is set to
   true, but can be disabled in cases where refreshing the list takes lots of time or
   people don't want the list to automatically scroll to the top (this also mitigates
   <a href="/issue/1949">issue #1949</a> until we have a cleaner fix). F5 for manual refresh is needed if enabled.
 * Fixed <a href="/issue/56">issue #56</a>: Ability to use Cygwin's git instead of msysGit
   Check out the manual (keyword "Cygwin git") regarding Cygwin Git support and configuration hints.
   Please note that Cygwin Git is not officially supported by TortoiseGit as the developers only
   use Git for Windows. Bug reports, however, are welcome.

== Bug Fixes ==
 * Fixed <a href="/issue/2359">issue #2359</a>: Selected files counter not updated after revert
 * Fixed <a href="/issue/2362">issue #2362</a>: git.exe get stuck on Win XP
 * Fixed <a href="/issue/2372">issue #2372</a>: Remote Config Tags Dropdown doesn't enable Apply-Button when switching to Reachable
 * Updated libgit2 in order to fix CVE-2014-9390.
   TortoiseGit is not affected in its default configuration (libgit2 has to be manually enabled for cloning).
   Git for Windows and mSysGit < 1.9.5 are affected, so an update highly recommended.
 * Fixed <a href="/issue/2385">issue #2385</a>: 'Mail' - 'Use configured server' & 'none'... seems cannot be localized
 * Fixed <a href="/issue/2356">issue #2356</a>: Rebase dialog should also auto-link to issue tracker
 * Fixed <a href="/issue/2390">issue #2390</a>: /command:log /outfile not written
 * Fixed <a href="/issue/2391">issue #2391</a>: Cannot clear "Load Putty Key" in clone dialog persistently
 * Fixed <a href="/issue/1789">issue #1789</a>: Tooltips not properly displayed in Log List if that commit has refs
 * Fixed <a href="/issue/2353">issue #2353</a>: Link to bug tracker not available in Show Log dialog on Win8+
 * Fixed <a href="/issue/2368">issue #2368</a>: CTRL+ENTER locks up GUI on Updating Index...
 * Fixed <a href="/issue/2414">issue #2414</a>: SendMail doesn't correctly place the mail in to list
 * Fixed several issues in libgit2 regarding SafeCrLf and AutoCrlf

<a name="Release_1.8.12.0">Release 1.8.12.0</a>
Released: 2014-11-25

== Features ==
 * Fixed <a href="/issue/2272">issue #2272</a>: Export Dialog auto select tag if the revision has tag
 * We now use a 4096-bit RSA key with SHA2-512 for signing and verifying our releases instead of a 1024-bit DSA key with SHA1
   The new key is signed by the old key. You can get the fingerprint from a trusted TortoiseGit installation by issuing
   "TortoiseGitProc.exe /command:pgpfp"
 * Fixed <a href="/issue/1928">issue #1928</a>: Hotkeys in "Show log" dialog for navigating back / forward
 * Fixed <a href="/issue/1316">issue #1316</a>: Add git describe
 * Fixed <a href="/issue/2294">issue #2294</a>: Submodule Update dialog doesn't save its state
 * Fixed <a href="/issue/2176">issue #2176</a>: Color config for "View Patch" Window & Tortoise(Git)UDiff
 * Fixed <a href="/issue/2331">issue #2331</a>: Show prefix log asterisk in Log Dialog
 * Fixed <a href="/issue/2157">issue #2157</a>: TortoiseGitMerge does not delete temporary files on close (marking as "resolved" in TGit was necessary)
 * Fixed <a href="/issue/1950">issue #1950</a>: Log Dialog: File list filter
 * Fixed <a href="/issue/2330">issue #2330</a>: Optimize submodule conflict resolution
 * Fixed <a href="/issue/2332">issue #2332</a>: TortoiseGitMerge: Move "Create patch file" to the File menu
 * TortoiseGitMerge: Updated EditorConfig version to 0.12
 * Fixed <a href="/issue/2341">issue #2341</a>: Jump to search box when pressing CTRL+E
 * Custom dictionaries for the spell checker can now be put into the %APPDATA%\TortoiseGit\dic folder (w/o administrator privileges).
   All other locations within the TortoiseGit installation folder (e.g. bin, dic, ...) except Languages are now deprecated.
   See the manual for more information.
 * TGitCache: Overlay update improvements and speedups

== Bug Fixes ==
 * Fixed <a href="/issue/2260">issue #2260</a>: Clicking in blank space in Commit dialog causes all unversioned files to become selected
 * Fixed <a href="/issue/2290">issue #2290</a>: "Pulled Log" broken
 * Fixed <a href="/issue/2281">issue #2281</a>: TGit 1.8.11 is not using %HOME% correctly
 * Fixed <a href="/issue/2306">issue #2306</a>: TortoiseGitMerge ToolTip messages for Previous/next inline difference mixed up
 * Fixed <a href="/issue/2308">issue #2308</a>: Pressing ESC in the skip-worktree confirmation dialog box selects 'yes'
 * Fixed <a href="/issue/2309">issue #2309</a>: [Show log of aaaa..bbbb] on sub directory shows logs on root directory
 * Fixed <a href="/issue/2126">issue #2126</a>: Rebase: Squash after conflict causes additional commit
 * Fixed <a href="/issue/2313">issue #2313</a>: [Clean to recycle bin] after Clean-up dry run removes unexpected files
 * Fixed <a href="/issue/874">issue #874</a>: Add --rebase option to pull dialog
 * Fixed <a href="/issue/2316">issue #2316</a>: Error on comparing files when git config core.autocrlf ends with 2+ spaces and then a comment
 * Fixed <a href="/issue/2323">issue #2323</a>: TortoiseGit does not pull --tags despite checking Tags
 * Fixed <a href="/issue/2327">issue #2327</a>: Help button is broken in "SVN Commit Type" dialog
 * TGitCache now checks file sizes before checking file contents. This should mitigate possible "file is locked" problems.
 * Fixed <a href="/issue/2329">issue #2329</a>: Changes track remote branch when fast forward is used
 * Fixed <a href="/issue/1872">issue #1872</a>: Rebase edit doesn't amend files but only the commit message
 * Fixed <a href="/issue/2342">issue #2342</a>: Ignore whitespace blame option not work
 * Fixed <a href="/issue/2311">issue #2311</a>: Empty commits cannot be rebased
 * Fixed <a href="/issue/2312">issue #2312</a>: Fix UTF-8 AutoCRLF issues with libgit2
 * Fixed <a href="/issue/2345">issue #2345</a>: The Show Log dialog's View Patch side window is empty when the Working dir changes line is selected and no file is selected
 * Fixed <a href="/issue/2348">issue #2348</a>: Conflicts during rebase appends "conflicts" to commit message

<a name="Release_1.8.11.0">Release 1.8.11.0</a>
Released: 2014-08-22

== Bug Fixes ==
 * Fixed <a href="/issue/2257">issue #2257</a>: Fetching & pushing using ssh.exe not working with TortoiseGit 1.8.10
 * Fixed crashes while opening the revision graph in the x64 version on some AMD CPUs
 * Fixed some other possible crashes when working with broken repositories or writing files failed
 * TortoiseGitMerge did not show ribbon bar with Portuguese (Brazil) language pack loaded

<a name="Release_1.8.10.0">Release 1.8.10.0</a>
Released: 2014-08-12

== Features ==
 * Updated shipped libgit2 to version 0.21
 * Fixed <a href="/issue/1605">issue #1605</a>: Add TortoiseGit start and pre commit hooks
 * Fixed <a href="/issue/2170">issue #2170</a>: Please add interface to "git svn log -v"
 * Fixed <a href="/issue/1787">issue #1787</a>: auto-complete label when writing commit message
 * Fixed <a href="/issue/2241">issue #2241</a>: Cleanup: result window of dry run should offer to "really remove"
 * Fixed <a href="/issue/1596">issue #1596</a>: Added basic support for clean/smudge filters
 * Fixed <a href="/issue/2167">issue #2167</a>: Support git push --force-with-lease option
 * TortoiseGitMerge: Support EditorConfig indent_style(space/tab) and indent_size
 * TortoiseGitMerge: Can specify number of context lines or follow diff.context in a patch file
 * Can sort tags in reversed order
 * Can shallow pull/fetch a shallow clone repository
 * Revision Graph: Add filter to walk from Only Local Branches

== Bug Fixes ==
 * Fixed <a href="/issue/2216">issue #2216</a>: Add fails with message: libgit2: "failed to parse 'warn' as boolean"
 * Fixed <a href="/issue/2218">issue #2218</a>: Tortoise Git Extra Backslash with SUBST Drive Paths
 * Fixed <a href="/issue/1951">issue #1951</a>: Slow switch/checkout dialog
 * Fixed <a href="/issue/2064">issue #2064</a>: Broken characters when dragging the horizontal bar in TortoiseGitMerge
 * Fixed <a href="/issue/2143">issue #2143</a>: PowerShell not working in git hooks
 * Fixed <a href="/issue/2222">issue #2222</a>: 'Prune' in the pull dialog does not work
 * Fixed <a href="/issue/2223">issue #2223</a>: Move & revert operations not as expected if working tree root is directly on a drive
 * Fixed <a href="/issue/2225">issue #2225</a>: include.path does not understand tilde (~)
 * Fixed <a href="/issue/2228">issue #2228</a>: TortoiseGitUDiff incorrect font by non-ascii char name
 * Fixed <a href="/issue/2230">issue #2230</a>: First click on order dialog is ignored
 * Fixed <a href="/issue/2003">issue #2003</a>: TortoiseMerge uses wrong EOL with patches
 * Fixed <a href="/issue/2234">issue #2234</a>: Config file mess up if not end with LF
 * Fixed <a href="/issue/2238">issue #2238</a>: TortoiseGitMerge Default button in "Find" dialog does replace all
 * Fixed <a href="/issue/2250">issue #2250</a>: bugtraq.logregex Test incorrectly saves the order of the fields
 * TGitCache now does not check the contents of files with filesize > 10 MiB any more
   and falls back to checking the timestamp of the files (as if TGitCacheCheckContent
   is set to "false") according the the git index. This limit can be changed by adjusting
   TGitCacheCheckContentMaxSize (measured in KiB) in TortoiseGit advanced settings.
   The reason for this change is that libgit2 reads a file to memory for hashing and,
   thus, locking the file and the repository for this time span.
   Fixed <a href="/issue/2240">issue #2240</a>: TGitCache keeps crashing when I look at my folders or commit
 * Fixed <a href="/issue/2219">issue #2219</a>: "add cherry picked from" is not applied when squashing/editing the commits
 * TortoiseGitMerge: Fix some crashes when Wrap Long Lines is enabled
 * TortoiseGitMerge: Use the screen chars of the current view for the scrollbar page value
 * TortoiseGitMerge: Do not draw EOL mark in line number region
 * TortoiseGitMerge: No need to expand tab to spaces when finding inline diff positions
 * Repository Browser said commit not found in submodule

== Known Issues ==
 * There seems to be a bug in the MFC library which occurs when opening the revision graph in the x64
   version on AMD CPUs, we cannot fix this. Please vote for this on Microsoft Connect:
   <a href="https://connect.microsoft.com/VisualStudio/feedback/details/893171/c000001d-illegal-instruction-on-msvcr120-log-0x2d3-on-x64" target="_blank">https://connect.microsoft.com/VisualStudio/feedback/details/893171/c000001d-illegal-instruction-on-msvcr120-log-0x2d3-on-x64</a>.

<a name="Release_1.8.9.0">Release 1.8.9.0</a>
Released: 2014-06-09

== Features ==
 * Fixed <a href="/issue/2152">issue #2152</a>: Log window show whole commit in "View patch"
 * Fixed <a href="/issue/2149">issue #2149</a>: Can't search message in annotated tag or text in notes
 * Fixed <a href="/issue/2172">issue #2172</a>: Support num parameters of git blame -M and git blame -C
 * Fixed <a href="/issue/2179">issue #2179</a>: Revision comparisons against HEAD should not generate temporary files (allow to compare to working tree)
 * Fixed <a href="/issue/2168">issue #2168</a>: Add to the "Clean up" dialog "Recursive" option for submodules
 * Fixed <a href="/issue/2196">issue #2196</a>: Log dialog file list group headers show parent SHA-1
 * Fixed <a href="/issue/2144">issue #2144</a>: TortoiseGit should support configuring pushurl for remotes
 * Fixed <a href="/issue/2155">issue #2155</a>: Make "Delete Remote Tags..." more discoverable
 * Fixed <a href="/issue/2164">issue #2164</a>: allow Commit from Log > Working dir changes of selected files
 * Fixed <a href="/issue/2096">issue #2096</a>: Allow to disable blue text-label about git.exe execution timings on progress dialog
 * Fixed <a href="/issue/968">issue #968</a>: Rebase / Cherry Pick dialogs should update "conflict" status when conflicts are resolved
 * Fixed <a href="/issue/1706">issue #1706</a>: Show log in different branch reverts to active branch
 * Fixed <a href="/issue/1973">issue #1973</a>: Support INS/OVR mode in TortoiseMerge
 * Fixed <a href="/issue/1970">issue #1970</a>: Provide replacement function in TortoiseGitMerge
 * Use libgit2 for more operations instead of calling git.exe

== Bug Fixes ==
 * Fixed <a href="/issue/2158">issue #2158</a>: TortoiseGit sometimes crashes when doing Sync > Pull
 * Fixed <a href="/issue/2159">issue #2159</a>: URL combo box in Submodule Add is not case sensitive
 * Fixed <a href="/issue/2171">issue #2171</a>: blame setting 'detect moved or copied line' does not work for the case 'from modified files'
 * Fixed <a href="/issue/2174">issue #2174</a>: Submodule update shows wrong submodule list
 * Fixed <a href="/issue/2175">issue #2175</a>: TortoiseGitBlame fails to search if line has non-ascii chars and system locale is not US
 * Fixed <a href="/issue/2141">issue #2141</a>: libgit2 still added the file with mixing EOL, while autocrlf and safecrlf are true
 * Fixed <a href="/issue/2183">issue #2183</a>: Explorer Property Page "Last modified" stays empty
 * Fixed <a href="/issue/2180">issue #2180</a>: When opening revision graph, scroll to current branch
 * Fixed <a href="/issue/2104">issue #2104</a>: Opening log for non-git folder results in multiple error messages
 * Fixed <a href="/issue/2192">issue #2192</a>: Shell context menu not detecting submodule if submodule name differs from submodule path
 * Fixed <a href="/issue/2187">issue #2187</a>: Misshowed branch on many dialogs in the new version
 * Fixed <a href="/issue/2191">issue #2191</a>: Skip-worktree or assume-valid flagged files are not always listed if subfolder is selected
 * Fixed <a href="/issue/2199">issue #2199</a>: Fetch dialog does not delete remote in remote list after managing remotes
 * Fixed <a href="/issue/2200">issue #2200</a>: Add remote name starts with # should not fetch all
 * Fixed <a href="/issue/2190">issue #2190</a>: "Force Rebase" after "Squash ALL" does not mark commits for squashing
 * Fixed <a href="/issue/2073">issue #2073</a>: Remove Entry of remote in "Remote Url" box in sync window
 * Fixed <a href="/issue/2207">issue #2207</a>: "Do you want to stash pop now?" dialog shows "Stash" labeled button instead of "Pop"
 * Fixed <a href="/issue/2161">issue #2161</a>: Windows explorer slow/very long delays with shell extension enabled and accessing network folders
 * Fixed <a href="/issue/2198">issue #2198</a>: MSVCR110.dll error with TortoisePlink. However, using TortoiseGitPlink is recommended

<a name="Release_1.8.8.0">Release 1.8.8.0</a>
Released: 2014-04-01

== Features ==
 * Fixed <a href="/issue/2065">issue #2065</a>: Add link to fetch to screen when push failed
 * Fixed <a href="/issue/1855">issue #1855</a>: Support negative log filter syntax
 * Fixed <a href="/issue/1029">issue #1029</a>: Add function to control the TGitCache module
 * Fixed <a href="/issue/2086">issue #2086</a>: Rebase autoskip patches that are already in upstream
 * Fixed <a href="/issue/2112">issue #2112</a>: Disable option for Drag & Drop Handler
 * Fixed <a href="/issue/2114">issue #2114</a>: Allow to swap rebase refs
 * Updated libgit to version 1.9.0
 * Fixed <a href="/issue/1920">issue #1920</a>: Diff in log dialog like in commit dialog
 * Fixed <a href="/issue/2134">issue #2134</a>: Easier to select lines to partial commit in TortoiseGitMerge
 * Fixed <a href="/issue/2135">issue #2135</a>: Add menu button to open Explorer when clone finishes

== Bug Fixes ==
 * Fixed <a href="/issue/2067">issue #2067</a>: Normalize email with capital letters when calculating gravatar hash
 * Fixed <a href="/issue/2071">issue #2071</a>: TGitCache blocks Windows to eject USB drive
 * Fixed <a href="/issue/2074">issue #2074</a>: Time field not large enough
 * Fixed <a href="/issue/1648">issue #1648</a>: Issue with handles being kept when there are uncommitted changes
 * Fixed <a href="/issue/2083">issue #2083</a>: TortoiseGitBlame 'show unified diff' actually performs compare
 * Fixed <a href="/issue/2087">issue #2087</a>: Rebase ComboBox DropDown List too short
 * Fixed <a href="/issue/2088">issue #2088</a>: log entries have space in front of message without labels if right side drawing is enabled
 * Fixed <a href="/issue/2075">issue #2075</a>: Commit do not show merged files after merge
 * Fixed <a href="/issue/2097">issue #2097</a>: Wrong context menu when selecting ignored file in Status dialog
 * Fixed <a href="/issue/2099">issue #2099</a>: Cannot cancel "Delete and add to ignore list"
 * Fixed <a href="/issue/2109">issue #2109</a>: Checkout Tag without creating Branch - Remove Pull Option
 * Fixed <a href="/issue/2106">issue #2106</a>: "All local branches" option is not remembered
 * Fixed <a href="/issue/2076">issue #2076</a>: Issue Tracker Integration is broken in 1.8.7.0
 * Fixed <a href="/issue/2124">issue #2124</a>: Rebase icon not properly displayed
 * Fixed <a href="/issue/2113">issue #2113</a>: JiraSvn bugtraq provider's commit behavior is broken
 * Fixed <a href="/issue/2128">issue #2128</a>: tagopt not saved when asked to disable tag fetching
 * Fixed <a href="/issue/2127">issue #2127</a>: After delete last tag in Browse references, tree of refs disappeared
 * Fixed <a href="/issue/2131">issue #2131</a>: Commit message not used when merging with "squash" option
 * Fixed <a href="/issue/2147">issue #2147</a>: Resolve not dealing correctly with (modify/delete) conflicts
 * Fixed <a href="/issue/2148">issue #2148</a>: Cherry pick with conflicts does not add "Cherry picked from..."
 * Fixed <a href="/issue/2150">issue #2150</a>: Integrate with issue tracker - bug Id cannot be larger than 6 digit number

<a name="Release_1.8.7.0">Release 1.8.7.0</a>
Released: 2014-01-12

== Features ==
 * Fixed <a href="/issue/1943">issue #1943</a>: Add "Ignore N items by extension" in Commit dialog
 * Fixed <a href="/issue/1952">issue #1952</a>: Export specific folder
 * Fixed <a href="/issue/1002">issue #1002</a>: No right-drag context menu (git copy & add, git move)
 * Fixed <a href="/issue/1956">issue #1956</a>: Ship more overlay icon sets as TortoiseSVN does
 * Add Ref Compare List in Sync Dialog
 * Fixed <a href="/issue/1975">issue #1975</a>: Pull Dialog should remember fast forward only
 * Fixed <a href="/issue/1981">issue #1981</a>: Hotkey to focus on file list in commit dialog
 * Fixed <a href="/issue/1979">issue #1979</a>: Comments are not stripped from commit message
 * Fixed <a href="/issue/1991">issue #1991</a>: Disable (by default) certain operations with stash commits
 * Updated shipped libgit2 to version 0.20
 * Fixed <a href="/issue/2010">issue #2010</a>: Conflict dialog show local and remote commits
 * Autocompletion for language-dependent keywords (usually class/method/variable names) in the commit dialog
 * Fixed <a href="/issue/2034">issue #2034</a>: Show gravatar in TortoiseGitBlame
 * Fixed <a href="/issue/871">issue #871</a>: Blame copy/move detection and other options
 * Fixed <a href="/issue/1990">issue #1990</a>: TortoiseGitBlame always takes whitespace into account
 * Fixed <a href="/issue/1988">issue #1988</a>: Allow tag selection in From/To fields instead of dates only
 * Fixed <a href="/issue/2015">issue #2015</a>: Save and load ref lists (selections) in "Show log" -> "Browse references"
 * Fixed <a href="/issue/1847">issue #1847</a>: Option to show all local branches in log view
 * Fixed <a href="/issue/1823">issue #1823</a>: Pressing tab key can insert spaces instead of tab in TortoiseGitMerge
 * Fixed <a href="/issue/2047">issue #2047</a>: Create a log file with git commands and output
 * Fixed <a href="/issue/1129">issue #1129</a>: Add Fetch/Pull operations in Show Log window
 * Fixed <a href="/issue/2048">issue #2048</a>: Add date to stash list / reflog
 * Fixed <a href="/issue/2049">issue #2049</a>: Add pull to after commit actions
 * Fixed <a href="/issue/1980">issue #1980</a>: Add Show Unified Diff entry to context menu in TortoiseGitBlame in Revision List
 * Fixed <a href="/issue/1820">issue #1820</a>: Show Assume valid/unchanged files in repo status dialog on a separate list
 * Fixed <a href="/issue/1603">issue #1603</a>: Make "Show Whole Project" in log and commit dialog autosaved
 * Fixed <a href="/issue/827">issue #827</a>: add the option to autoclose the "Git command progress" window
 * Fixed <a href="/issue/1913">issue #1913</a>: SVN DCommit Button in successful merge Dialog
 * Fixed <a href="/issue/1978">issue #1978</a>: Use file directory on Save revision to path in Repository Browser and log dialog
 * Updated libgit to version 1.8.5.2
 * Fixed <a href="/issue/2059">issue #2059</a>: "Go to line" CLI argument support for diff command

== Bug Fixes ==
 * Fixed <a href="/issue/1947">issue #1947</a>: When editing git notes, spurious reformatting is done
 * Fixed <a href="/issue/1958">issue #1958</a>: Revision graph crashes when trying to switch/checkout via context menu
 * Fixed <a href="/issue/1963">issue #1963</a>: TortoiseGitMerge: Missing color config when an added line is changed
 * Fixed <a href="/issue/1976">issue #1976</a>: Commit log crashes if it receives a WM_PRINTCLIENT while opening
 * Fixed <a href="/issue/1974">issue #1974</a>: bugtraq logregex does not save correctly
 * Fixed <a href="/issue/1977">issue #1977</a>: Commit log: cannot select multiple branches if a branch name matches a filename
 * Fixed <a href="/issue/1992">issue #1992</a>: Compare selected refs fails if tag object is selected
 * Fixed <a href="/issue/1985">issue #1985</a>: TortoiseGitBlame does not follow renames when comparing line with previous version
 * Fixed <a href="/issue/2008">issue #2008</a>: Wrong reflog if hash repeated
 * Fixed <a href="/issue/2004">issue #2004</a>: In create patch serial dialog, number of commits up down buttons reversed
 * Fixed <a href="/issue/2013">issue #2013</a>: Double-clicking on ref log list doesn't work until after right-clicking it
 * Fixed <a href="/issue/2016">issue #2016</a>: Changed files list forgets sort order after switching versions in compare revisions dialog
 * Fixed <a href="/issue/2014">issue #2014</a>: "Show log" doesn't update properly when a window is dragged over it if aero is not enabled
 * Fixed <a href="/issue/2021">issue #2021</a>: Disable commit template when combine commits
 * Fixed <a href="/issue/2024">issue #2024</a>: Unnamed "remote" should not be created
 * Fixed <a href="/issue/2027">issue #2027</a>: Failed to preserve selected commit when filter is removed
 * Fixed <a href="/issue/2030">issue #2030</a>: TortoisePlink crash on x64
 * Fixed <a href="/issue/2037">issue #2037</a>: TortoiseGit.dll sometimes does not close it's handles in explorer.exe
 * Fixed <a href="/issue/2040">issue #2040</a>: "fatal: pathspec did not match any files" error when resolving deleted/modified conflict
 * Fixed <a href="/issue/1866">issue #1866</a>: Check for newer version of tgit behind proxy
 * Fixed <a href="/issue/1927">issue #1927</a>: Clicking on a hyperlinked commit hash leaves the target's log message un-hyperlinked
 * Fixed <a href="/issue/1835">issue #1835</a>: Question mark icons on committed unchanged files
 * Fixed <a href="/issue/2053">issue #2053</a>: Can't do "Combine to one commit"; changed file list wrongly empty
 * Fixed <a href="/issue/2051">issue #2051</a>: Submodule update error when fetchRecurseSubmodules has value on-demand
 * Fixed <a href="/issue/1860">issue #1860</a>: Race condition in git.exe Progress Dialog
 * Fixed <a href="/issue/2020">issue #2020</a>: Passworddialog in background after requesting username
 * Fixed <a href="/issue/1802">issue #1802</a>: submodule update module selection not always accurate
 * Fixed <a href="/issue/1579">issue #1579</a>: Log Dialog sometimes forgets the dialog height

== Known Issues ==
 * If you upgraded Windows 8 to 8.1 and you experience problems with the installer, please see <a href="/issue/2061">issue #2061</a>.

<a name="Release_1.8.6.0">Release 1.8.6.0</a>
Released: 2013-10-20

== Features ==
 * Revision Graph: Can generate output graph file through command line
 * Fixed <a href="/issue/1858">issue #1858</a>: Switch-and-merge as a single command
 * Log Dialog: Support Gravatar
 * Implement git checkout --merge
 * TortoiseGitIDiff: Conflict resolving for images
 * Fixed <a href="/issue/1795">issue #1795</a>: Allow to put branch labels on right side of message column in log view
 * Fixed <a href="/issue/1870">issue #1870</a>: Commit subset of changes selected from diff dialog
 * Fixed <a href="/issue/1785">issue #1785</a>: If PuTTY defaults to anything other than type SSH port 22, TortoiseGit acts up on SSH cloning (fixed in TortoiseGitPlink.exe)
 * Add setting to confirm to kill running git process in progress dialog
 * Added support for tgit://-links to start cloning from browser (e.g. tgit://clone/https://code.google.com/p/tortoisegit/)
 * Added optional support for git://-links to start cloning from browser
 * Added optional support for smartgit:// and github-windows://-links to start cloning from browser
   In order to be able to use github-windows://-links on github.com web page, you have to enable them for your GitHub profile once.
   Right now the only way is to install the official GitHub client and sign in with your GitHub profile (on any Windows machine).
 * Allow to edit hierarchical git settings
   If you have set "i18n.commitencoding" to something different than UTF-8 check your "user.name" setting
 * Fixed <a href="/issue/1645">issue #1645</a>: enable/disable spell checking in commit dialog
 * Fixed <a href="/issue/1806">issue #1806</a>: Compressed graph in the log view
 * Fixed <a href="/issue/1926">issue #1926</a>: Add "No Merges" checkbox in Log Messages window
 * Fixed <a href="/issue/1779">issue #1779</a>: tortoisegit does not understand include.path in global config
   If you have relevant settings for TGitCache in an included file, you have to restard TGitCache or modify the global, system, or local config file in order to reload any included file.
 * Support project icon as task button overlay
 * Fixed <a href="/issue/1946">issue #1946</a>: In "Compare revisions", please show something to confirm when there are no differences

== Bug Fixes ==
 * Fixed <a href="/issue/1859">issue #1859</a>: Log too long when using Switch to New Branch option in Create Branch dialog
 * Fixed <a href="/issue/1864">issue #1864</a>: Commit button did not work after revert
 * Fixed <a href="/issue/1862">issue #1862</a>: Some labels not wide enough for translations to fit in
 * Fixed <a href="/issue/1790">issue #1790</a>: "Clean up" window position is not preserved on multi-monitor setup
 * Fixed <a href="/issue/1876">issue #1876</a>: Up/down buttons in Rebase dialog desynchronise the bottom view
 * Fixed <a href="/issue/1875">issue #1875</a>: Commit selection is lost with focus in Rebase dialog
 * Fixed <a href="/issue/1873">issue #1873</a>: Squash doesn't preserve original author date
 * Fixed <a href="/issue/1863">issue #1863</a>: never-ending wait due to lost messages
 * Fixed <a href="/issue/1883">issue #1883</a>: Option Buttons in Hook script not wide enough
 * Fixed <a href="/issue/1893">issue #1893</a>: Unified diff for merge commit always shows inverted changes
 * Fixed <a href="/issue/1896">issue #1896</a>: Switch/Checkout Branch name resets itself after browsing for a branch
 * Fixed <a href="/issue/1895">issue #1895</a>: Context menu incomplete if working tree is located on network share
 * Fixed <a href="/issue/1618">issue #1618</a>: In "Show log", mouseover triggers improper redraw of ClearType font log descriptions in "Message" column
 * Fixed <a href="/issue/1891">issue #1891</a>: Cleanup dialog unexpectedly sizable
 * Bring back templates when creating repository with libgit2, it was "cancelled" in since TortoiseGit 1.8.0
 * Fixed <a href="/issue/1908">issue #1908</a>: "Export selection to.." fails to checkout files which names (or paths) renamed in working dir
 * Fixed <a href="/issue/1914">issue #1914</a>: Repository Browser give an error (libgit2) after change branch
 * Fixed <a href="/issue/1915">issue #1915</a>: Submodule Update Dialog behaves curious
 * Fixed <a href="/issue/1919">issue #1919</a>: Submodule visibility with square brackets
 * Fixed <a href="/issue/1918">issue #1918</a>: Settings dialog forces to use remotes
 * Fixed <a href="/issue/1923">issue #1923</a>: Once Credential node is activated, pressing apply button in Settings dialog will close the dialog
 * Fixed <a href="/issue/1769">issue #1769</a>: "Show Log" missing from context menu if "Shell Extended" and a file was Added but not committed
 * Fixed <a href="/issue/1917">issue #1917</a>: Avoid request to stash if there is nothing to rebase or fast forward is possible
 * Fixed <a href="/issue/1929">issue #1929</a>: Using "Find" in "Show log" doesn't move to commits it finds, it only selects them
 * Fixed <a href="/issue/1948">issue #1948</a>: Case sensitive authors switch throw an error

== Known Issues ==
 * If you experience problems with PuTTY ssh, please see <a href="/issue/1880">issue #1880</a>.

<a name="Release_1.8.5.0">Release 1.8.5.0</a>
Released: 2013-08-18

== Features ==
 * Fixed <a href="/issue/1831">issue #1831</a>: Allow Origin Remote Renaming on Clone
 * Fixed <a href="/issue/1839">issue #1839</a>: Conflict dialog does not allow resolving multiple or all conflicts with the same option
 * Allow to send (patch)mails over relay servers (e.g. the one from your ISP)
 * When adding second and more remotes, prompt user to disable tag fetching to avoid fetching wrong tags
 * Fixed <a href="/issue/1845">issue #1845</a>: Add "pull" button on "git switch" window
 * Fixed <a href="/issue/1848">issue #1848</a>: TortoiseGitBlame doesn't scroll to the previously selected line on "Blame previous revision"
 * Underline git hash in log message view and jump to that commit when clicking on the hash
 * Fixed <a href="/issue/1829">issue #1829</a>: You have not concluded your merge (MERGE_HEAD exists) - allow to abort merge more easily.
 * Commit Dialog: Add "Pick commit hash" in message text box context menu
 * Can set log graph line width and node size
 * Updated shipped PuTTY Plink to version 0.63 (which contains some security fixes)
 * Fixed <a href="/issue/1746">issue #1746</a>: Pressing "Delete" on keyboard should delete selected non versioned file in commit dialog
 * Sync Dialog: Add stash menu button
 * Fixed <a href="/issue/1852">issue #1852</a>: Add command-line argument to tortoisegitproc that specifies value for Directory field

== Bug Fixes ==
 * Fixed <a href="/issue/1833">issue #1833</a>: Fast-Forward rebase does not preserve SHA-1 of commits
 * Fixed <a href="/issue/1832">issue #1832</a>: TortoiseGitMerge has an empty menue when German language pack is loaded
 * Fixed <a href="/issue/497">issue #497</a>: TGitCache.exe won't let user delete folder
 * Mitigated <a href="/issue/1011">issue #1011</a>: "git svn" commands create "sys$command" file

<a name="Release_1.8.4.0">Release 1.8.4.0</a>
Released: 2013-07-09

== Features ==
 * Fixed <a href="/issue/1765">issue #1765</a>: Pull button when push (after commit) fails
 * Updated shipped libgit2 to version 0.19
 * Updated libgit to version 1.8.3
 * Support diff.context config key
 * Fixed <a href="/issue/1778">issue #1778</a>: Add Submodule with Putty key
 * Fixed <a href="/issue/1800">issue #1800</a>: Automatically remove "git clone " from URL in Clone dialog
 * Fixed <a href="/issue/1807">issue #1807</a>: Handle new descriptions of detached head introduced in git 1.8.3
 * TortoiseGitMerge: Encoding and EOL conversion
 * TortoiseGitMerge: Tab / spaces conversion and trim trailing space
 * TortoiseUDiff: Add Printing function
 * Fixed <a href="/issue/553">issue #553</a>: Pull the latest version of all submodules
 * TortoiseGitMerge: Improved applying a patch with conflicts
 * Rename touch.exe as tgittouch.exe to avoid filename collision
 * Support SOCKS proxy
 * Include submodules in RepositoryBrowser

== Bug Fixes ==
 * Fixed <a href="/issue/1763">issue #1763</a>: Unable to re-download TortoiseGit packages due to caching
 * Fixed <a href="/issue/795">issue #795</a>: Switch/Checkout Dialog, "Switch To Version" is confusing
 * Fixed <a href="/issue/722">issue #722</a>: Pull/Push sometimes does not gets the right remote branch
 * Fixed <a href="/issue/1781">issue #1781</a>: Sync dialog fails with "Encountered an improper argument" when closing log tab with escape
 * Fixed <a href="/issue/1773">issue #1773</a>: "Show changes as unified diff" on "Working dir changes" show inverted diff operation
 * Fixed <a href="/issue/1753">issue #1753</a>: On dialog box for SSH certificate's passphrase, some certificate filenames aren't fully displayed
 * Fixed <a href="/issue/1726">issue #1726</a>: Rebasing issues with empty commits
 * Fixed <a href="/issue/1782">issue #1782</a>: Do not reset changed lines/files statistics after toggling "Authors case sensitive" checkbox
 * Fixed <a href="/issue/1783">issue #1783</a>: TortoiseGit does not work on WinXP/2003 x64
 * Fixed <a href="/issue/1798">issue #1798</a>: "Paste Recent commits" during Please wait in Commit dialog does not work
 * Fixed <a href="/issue/1106">issue #1106</a>: Stairs in log view
 * Fixed <a href="/issue/1304">issue #1304</a>: TortoiseGit doesn't see a bare repo within a non-bare one
 * "Delete (keep local)" did not work on folders
 * Fixed <a href="/issue/1756">issue #1756</a>: Rebase trips over merge commits
 * Fixed possible crashes while applying a patch in TortoiseGitMerge
 * Fixed <a href="/issue/1809">issue #1809</a>: Overlay shows changes in parent folder if a file with a second extension exists
 * TortoiseGitMerge: Fixed bugs regarding multi-view edit
 * Fixed <a href="/issue/1814">issue #1814</a>: Squash rebase fills message too much
 * Reverting newly added file in Commit-dialog misbehaving
 * Fixed <a href="/issue/1589">issue #1589</a>: Clone Dialog's URL case sensitivity causes issues with folder creation
 * Fixed <a href="/issue/1786">issue #1786</a>: Committing 25 or more files causes hidden progress dialog
 * Fixed <a href="/issue/1732">issue #1732</a>: Tortoisegit always crashes when viewing the history log of blink
 * Fixed <a href="/issue/1824">issue #1824</a>: Rebase Dialog sometimes not showing after running Fetch and Rebase in Sync Dialog
 * Fixed <a href="/issue/1725">issue #1725</a>: 'Git Show Log' doesn't work on folders created by git-new-workdir script

<a name="Release_1.8.3.0">Release 1.8.3.0</a>
Released: 2013-05-05

== Features ==
 * Press F5 can refresh Submodule Diff Dialog
 * Fixed <a href="/issue/1359">issue #1359</a>: Drag support for files listed as being changed in the commit/log/modifications window
 * log list: indicate annotated tag in tag shape
 * TortoiseGitBlame: Allow to highlight lines by age of last modification
 * Fixed <a href="/issue/1376">issue #1376</a>: Daemon starting from Tortoise UI
 * Fixed <a href="/issue/1737">issue #1737</a>: Make a button to rename remote
 * Repository Browser: Allow to copy tree / blob hashes to clipboard
 * Added sound support for indicating warnings and errors
 * Fixed <a href="/issue/1755">issue #1755</a>: Add "Remove All" and "Ignore All" options in Remove from Index Prompt
 * Fixed <a href="/issue/1747">issue #1747</a>: Add interface to log window to view range log

== Bug Fixes ==
 * Fixed <a href="/issue/1729">issue #1729</a>: crash when commit
 * Fixed <a href="/issue/1728">issue #1728</a>: TortoiseGitMerge show fake difference in the same lines if line end falls to 0x40000 byte
 * Fixed <a href="/issue/1741">issue #1741</a>: Push / Pull Dialog URL combo box should not be filled unless enabled
 * Fixed <a href="/issue/1735">issue #1735</a>: diff-xls.vbs not found
 * Fixed <a href="/issue/1750">issue #1750</a>: Repository Browser fails for empty repository
 * Fixed <a href="/issue/1743">issue #1743</a>: Remove "The target folder is not empty!" warning
 * Fixed <a href="/issue/1757">issue #1757</a>: Compare single new file against working tree
 * Fixed <a href="/issue/1760">issue #1760</a>: Unified diff of two revisions is inversed
 * Fixed various possible crashes

<a name="Release_1.8.2.0">Release 1.8.2.0</a>
Released: 2013-03-26

== Features ==
 * Fixed <a href="/issue/1270">issue #1270</a>: Log dialog can revert selected files to parent revision
 * Fixed <a href="/issue/1500">issue #1500</a>: Support git merge --log option
 * Can disable log cache (tortoisegit.data, tortoisegit.index)
 * Revision Graph can save as graphviz dot language (*.gv)
 * Support merge strategies except octopus
 * Fixed <a href="/issue/1597">issue #1597</a>: Reduce chance of committing old submodule revision on cherry-pick / rebase conflicts
 * Can specify specific paths of submodules to update
 * Fixed <a href="/issue/1661">issue #1661</a>: TortoiseGitBlame: show line date time
 * Fixed <a href="/issue/1662">issue #1662</a>: skip-worktree should not be exposed via context menus or anything without confirmation
 * Fixed <a href="/issue/330">issue #330</a>: Implement --author option gui interface in the commit dialog
 * Fixed <a href="/issue/1147">issue #1147</a>: Ability to filter out branches in Log dialog
 * Fixed <a href="/issue/1628">issue #1628</a>: Support commit ranges in log list (from Reference Browser)
 * Fixed <a href="/issue/832">issue #832</a>: Compare commits between branches (branch1...branch2, from Reference Browser)
 * Fixed <a href="/issue/515">issue #515</a>: Sort the log window by date
 * Fixed <a href="/issue/1676">issue #1676</a>: Automatically check "Make it Bare" when repository path ends in ".git"
 * Fixed <a href="/issue/1677">issue #1677</a>: Clicking "revert" on a file should automatically check the file in the revert dialog
 * Fixed <a href="/issue/1667">issue #1667</a>: Ability to ignore a folder in commit dialog
 * Fixed <a href="/issue/1674">issue #1674</a>: Option "Push all branches" leaves out tags on pushed changes
 * Fixed <a href="/issue/1663">issue #1663</a>: F5 does not refresh the revision graph
 * Fixed <a href="/issue/522">issue #522</a>: Store password for HTTP
 * Fixed <a href="/issue/1660">issue #1660</a>: Save Push Dialog "Push all branches" and "Use Thin Pack" options
 * Fixed <a href="/issue/1542">issue #1542</a>: Can send pull request email
 * Fixed <a href="/issue/1493">issue #1493</a>: Remember Push Dialog "Push to all remotes" option state
 * Enhanced Windows 7 taskbar grouping and highlighting of windows belonging to the same working tree
 * .mailmap is now used for Log-Dialog Statistics
 * Fixed <a href="/issue/1610">issue #1610</a>: Average values on statistics dialog graph
 * Fixed <a href="/issue/1708">issue #1708</a>: Merge dialog have no saving of log message
 * Fixed <a href="/issue/1716">issue #1716</a>: TortoiseGitBlame added line parameter
 * Allow to set a default value for ssh client in HKLM (Software\TortoiseGit\SSH)

== Bug Fixes ==
 * Fixed <a href="/issue/1642">issue #1642</a>: Incorrect behavior if repo is located on root of drive
 * Fixed <a href="/issue/1643">issue #1643</a>: TortoiseGitMerge window do not maximize correctly on screeen with right-side docked start bar
 * Fixed <a href="/issue/1639">issue #1639</a>: Does not work on older CPU without SSE2
 * Fixed <a href="/issue/1682">issue #1682</a>: Rebase ignores commits after an error is received
 * Fixed <a href="/issue/1429">issue #1429</a>: Blame: copy sha to clipboard copies the log message
 * Fixed <a href="/issue/1654">issue #1654</a>: Git blame commit message tooltip flashes when tooltip window is tall
 * Fixed <a href="/issue/1658">issue #1658</a>: Checkbox 'add "cherry picked from"' has wrong anchor
 * Fixed <a href="/issue/1657">issue #1657</a>: Dirty submodule commit does not open the underling submodule commit dialog
 * Fixed <a href="/issue/1552">issue #1552</a>: TGitCache prevents a git clone from performing successfully
 * Fixed <a href="/issue/1631">issue #1631</a>: Set focus to branch menu in Switch/Checkout window
 * Fixed <a href="/issue/1646">issue #1646</a>: TortoiseGit Log gives undeterministic results (log does not apply --boundary any more by default)
 * Fixed <a href="/issue/1085">issue #1085</a>: Small commit with lots of unversioned and not ignored files takes very long
 * Fixed <a href="/issue/1675">issue #1675</a>: delete/ignore does not delete folder from repo when local copy is kept
 * Fixed <a href="/issue/1679">issue #1679</a>: Reset dialog pick another revision has no effect
 * Fixed <a href="/issue/1609">issue #1609</a>: remember my remote branch choice precisely
 * Fixed <a href="/issue/1268">issue #1268</a>: Paths in Windows Explorer changed to all lower case
 * Fixed <a href="/issue/1681">issue #1681</a>: High screen DPI breaks TortoiseGitMerge's ribbon UI sprites
 * Fixed <a href="/issue/1685">issue #1685</a>: Deleting remote branch blocks UI
 * Fixed <a href="/issue/1686">issue #1686</a>: Wrong stash list after "Stash Apply" in RefLog
 * Fixed <a href="/issue/1689">issue #1689</a>: Option to make Git Commit GUI look different from TortoiseSVN equivalent
 * Fixed <a href="/issue/1693">issue #1693</a>: Cherry pick progress bar doesn't stay green
 * Fixed <a href="/issue/1276">issue #1276</a>: Cannot show diff of renamed file
 * Fixed <a href="/issue/1696">issue #1696</a>: Fetch from switch dialog does not refresh branches list
 * Fixed <a href="/issue/1701">issue #1701</a>: Changing commit order in rebase dialog doesn't auto scroll
 * Fixed <a href="/issue/1702">issue #1702</a>: Original commit message is showen even though it is changed during rebase
 * Fixed <a href="/issue/1223">issue #1223</a>: Workaround endless waiting when git.exe already exited
 * Fixed <a href="/issue/1709">issue #1709</a>: Right click -> assume unchanged is irreversible
 * Fixed <a href="/issue/1713">issue #1713</a>: Pre-populate commit message on squash merges
 * Fixed <a href="/issue/1566">issue #1566</a>: Frequent crashes when searching log

<a name="Release_1.8.1.0">Release 1.8.1.0</a>
Released: 2013-02-07

== Features ==
 * Fixed <a href="/issue/1623">issue #1623</a>: Search by email in Log window
 * Fixed <a href="/issue/1624">issue #1624</a>: Easier navigation in revision graph with mouse

== Bug Fixes ==
 * Fixed <a href="/issue/1611">issue #1611</a>: Changing Search criteria with empty search field refreshes log content
 * Fixed <a href="/issue/1620">issue #1620</a>: Apply Patch serial can't open file dialog on WinXP
 * Fixed <a href="/issue/1533">issue #1533</a>: Could not parse git.exe version number
 * Fixed <a href="/issue/1619">issue #1619</a>: TortoiseGitMerge: Ribbon UI/toolbars can be toggled
 * Fixed <a href="/issue/1627">issue #1627</a>: TortoiseGit gets the wrong revision if the branch name looks like git-describe format
 * Fixed <a href="/issue/1617">issue #1617</a>: TortoiseGitPlink does not work with interactive ssh logins
 * Fixed <a href="/issue/1384">issue #1384</a>: Commit Dialog autocompletion does not work for some pattern

<a name="Release_1.8.0.0">Release 1.8.0.0</a>
Released: 2013-01-27

== Features ==
 * Fixed <a href="/issue/1377">issue #1377</a>: Log Dialog Working dir changes should include untracked files
 * Fixed <a href="/issue/1541">issue #1541</a>: Show progress dialog when running git request-pull
 * Fixed <a href="/issue/1513">issue #1513</a>: Easier to clean TortoiseGit temporary files
 * Fixed <a href="/issue/1540">issue #1540</a>: Add jump to previous / next commit in Log Dialog
 * Fixed <a href="/issue/1529">issue #1529</a>: Can show and set tagopt
 * Fixed <a href="/issue/1554">issue #1554</a>: Unwanted text formatting in commit dialogue (styling can be disabled using advanced settings now)
 * Fixed <a href="/issue/1563">issue #1563</a>: Show repository dir on Settings Dialog title bar
 * Fixed <a href="/issue/1577">issue #1577</a>: Add a retry button on Reset failure
 * Fixed <a href="/issue/1571">issue #1571</a>: Warn author not set when editing notes
 * Fixed <a href="/issue/221">issue #221</a>: After resolving all merge conflicts, it's not obvious the project needs a commit
 * Fixed <a href="/issue/1568">issue #1568</a>: Add support for recurse-submodules on push dialog
 * Fixed <a href="/issue/1578">issue #1578</a>: Warn author not set when merging
 * Fixed <a href="/issue/1557">issue #1557</a>: Submodule Diff Dialog should launch diff with working copy if the submodule is dirty
 * Fixed <a href="/issue/1066">issue #1066</a>: Add 'Revision Graph' Feature
 * Fixed <a href="/issue/1435">issue #1435</a>: EXE Name collision with TortoiseSVN
   Renamed:
   - TortoiseIDiff.exe to TortoiseGitIDiff.exe
   - TortoiseMerge.exe to TortoiseGitMerge.exe
   - TortoiseProc.exe to TortoiseGitProc.exe
   - TortoisePlink.exe to TortoiseGitPlink.exe (TortoisePlink.exe is still shipped for compatibility reasons)
   - TortoiseUDiff.exe to TortoiseGitUDiff.exe
 * Fixed <a href="/issue/1433">issue #1433</a>: Can reset to remote tracking branch after fetch
 * Fixed <a href="/issue/763">issue #763</a>: "Clean up" should show a list of to-be-deleted files
 * Fixed <a href="/issue/1581">issue #1581</a>: Show/Hide tags in the log
 * Synced TortoiseGitMerge code with TortoiseMerge of TortoiseSVN code (trunk)
 * Log Dialog specially highlights branches that have a remote tracking branch
 * Added search mechanism to reflog dialog
 * Add No fetch, Merge and Rebase options in Submodule Update Dialog
 * Fixed <a href="/issue/1467">issue #1467</a>: put remote branches at the bottom in "branches" dropdowns (can be disabled using advanced settings)
 * Fixed <a href="/issue/1285">issue #1285</a>: Show Log Filter on Tags
 * Uncheck "Track" checkbox when creating a branch with different name to remote branch
 * Add button to push notes in Sync Dialog

== Bug Fixes ==
 * Fixed <a href="/issue/1535">issue #1535</a>: Unclear UI in prompt dialog of Stash Save
 * Fixed <a href="/issue/1538">issue #1538</a>: Deleting multiple stash list entries skips some and delete the wrong ones
 * Fixed <a href="/issue/1543">issue #1543</a>: TortoiseGit Comboboxes trim input if longer than their width
 * Fixed <a href="/issue/1544">issue #1544</a>: TortoiseGit Init warning message is not appropriate
 * Fixed <a href="/issue/1555">issue #1555</a>: Commit dialog fails to warn submodule dirty state
 * Fixed <a href="/issue/1556">issue #1556</a>: Narrower treeview when launching Settings Dialog with default page Remote
 * Fixed <a href="/issue/1383">issue #1383</a>: Pageant is not always auto-loaded if using sync remote update (some without keys)
 * Fixed <a href="/issue/1536">issue #1536</a>: Committing via log causes hidden progress dialog
 * Fixed <a href="/issue/1593">issue #1593</a>: Sync dialog In ChangeList shows wrong diff when pressing enter

<a name="Release_1.7.15.0">Release 1.7.15.0</a>
Released: 2012-11-20

== Features ==
 * Fixed <a href="/issue/1436">issue #1436</a>: Clone Dialog add --branch and --no-checkout options
 * Fixed <a href="/issue/1450">issue #1450</a>: Option to fetch when adding a remote
 * Fixed <a href="/issue/1411">issue #1411</a>: Commit dialog checks submodule dirty state
 * Fixed <a href="/issue/1485">issue #1485</a>: Implement a Goto-Line dialog in TortoiseMerge
 * Upgraded gitdll and libgit to 1.8.0
 * Fixed <a href="/issue/1461">issue #1461</a>: Add progress indicator for stash operations
 * Fixed <a href="/issue/1484">issue #1484</a>: Settings dialog default to Git page
 * Fixed <a href="/issue/1505">issue #1505</a>: Tell user that author not set in Rebase Dialog
 * Fixed <a href="/issue/1503">issue #1503</a>: Passing full file paths and revisions into diff viewer
 * Fixed <a href="/issue/1518">issue #1518</a>: Add "Export selection to" menu item in Log Dialog
 * Fixed <a href="/issue/1497">issue #1497</a>: Systemwide gitconfig may require UAC elevation when editing
 * Fixed <a href="/issue/1512">issue #1512</a>: In Rebase Dialog, ask user to retry after failed "git checkout" and "git reset"
 * Fixed <a href="/issue/1330">issue #1330</a>: Preparing for git 1.7.12 new config file location
 * Fixed <a href="/issue/1515">issue #1515</a>: Allow Changed Files Dialog to revert selected files
 * Fixed <a href="/issue/1458">issue #1458</a>: Shell context menu shows "Commit submodule" if the selected folder is submodule root
 * Fixed <a href="/issue/1211">issue #1211</a>, <a href="/issue/1472">issue #1472</a>: Commit dialog can amend message only without affecting any files
 * Fixed <a href="/issue/1200">issue #1200</a>: Ignored folders can be destroyed by "git stash -u"
 * Fixed <a href="/issue/1508">issue #1508</a>: Indicate change type in Submodule Diff Dialog
 * Fixed <a href="/issue/1527">issue #1527</a>: Rebase Dialog shows Log menu button after rebasing if not launched from log dialog
 * Fixed <a href="/issue/1252">issue #1252</a>: wrap commit logs per git conventions (set tgit.logwidthmarker using git-config)
 * Added basic support for localized spellcheckers in commit dialog (see help file, keyword "spellchecker")
 * Fixed <a href="/issue/1531">issue #1531</a>: Clean up should show progress dialog

== Bug Fixes ==
 * Fixed <a href="/issue/1427">issue #1427</a>: Combine to one commit: Commit-window doesn't show changes from oldest commit
 * Fixed <a href="/issue/1443">issue #1443</a>: git svn dcommit: unclear behavior for dirty working tree
 * Fixed <a href="/issue/1457">issue #1457</a>: Confusing amend commit behaviour
 * Fixed <a href="/issue/1466">issue #1466</a>: Checkout Dialog always track remote branches
 * Fixed <a href="/issue/1396">issue #1396</a>: Sync Dialog Pull show wrong commit range in "In commits"
 * Fixed <a href="/issue/1465">issue #1465</a>: Clean up does not work with core.quotepath=true
 * Fixed <a href="/issue/1419">issue #1419</a>: Push window doesn't load PuTTY key when pushing to multiple remotes
 * Fixed <a href="/issue/1482">issue #1482</a>: Allow to show log of other branches if current HEAD points to an orphan branch
 * Fixed <a href="/issue/1487">issue #1487</a>: Do not use libgit in TortoiseShell for displaying HEAD commit (might set %GIT_DIR% permanently)
 * Fixed <a href="/issue/1475">issue #1475</a>: Installer resets your SSH preference to ssh.exe when run silently
 * Fixed <a href="/issue/1470">issue #1470</a>: Log Dialog: Selected item not remembered after refresh if item is one of the first 100
 * Fixed <a href="/issue/1499">issue #1499</a>: Allow to show log if current HEAD and selected ref is orphan branch when show all branches checked
 * Fixed <a href="/issue/1507">issue #1507</a>: Submodule Diff Dialog should show dirty state only on working copy revision
 * Fixed <a href="/issue/1511">issue #1511</a>: Changed Files Dialog cannot show icons as folder for submodule
 * Fixed <a href="/issue/1516">issue #1516</a>: SVN clone strips trailing slashes
 * Fixed <a href="/issue/1486">issue #1486</a>: Rebase/Cherry-pick Dialog conflict list fail to refresh the status of resolved deleted file
 * Fixed <a href="/issue/1521">issue #1521</a>: Directories with period at beginning are recognized as file extensions showing *.dir
 * Fixed <a href="/issue/1519">issue #1519</a>: Quotes in notes break Merge command
 * Fixed <a href="/issue/1524">issue #1524</a>: Log dialog failed to view revision in alternative editor of the same file more than once
 * Fixed <a href="/issue/1459">issue #1459</a>: Refreshing Log Dialog may freeze up to 20 seconds
 * Fixed <a href="/issue/1523">issue #1523</a>: Bugtraq Number is checked for number only (inconsistent default value)

<a name="Release_1.7.14.0">Release 1.7.14.0</a>
Released: 2012-10-14

== Features ==
 * Fixed <a href="/issue/1424">issue #1424</a>: Better Indicate submodule diff error
 * Fixed <a href="/issue/1425">issue #1425</a>: Indicate dirty state in Submodule Diff Dialog
 * Fixed <a href="/issue/1430">issue #1430</a>: Diff: allow multiple files unified diff
 * Fixed <a href="/issue/1453">issue #1453</a>: Allow Log List to copy multiple SHA-1 hashes
 * Fixed <a href="/issue/1454">issue #1454</a>: Show git progress time spent and end time in Progress Dialog and Sync Dialog
 * Fixed <a href="/issue/1434">issue #1434</a>: Easier to copy to file text to clipboard in TortoiseGitBlame

== Bug Fixes ==
 * Fixed <a href="/issue/1426">issue #1426</a>: Pull Dialog: Unknown option 'no-ff' when using no-tags
 * Fixed <a href="/issue/1423">issue #1423</a>: Update Dialog downloads the wrong language pack
 * Fixed <a href="/issue/1439">issue #1439</a>: In Show log, long messages don't have the triple dots appended, when a Tag/Branch lable is visible
 * Fixed <a href="/issue/1444">issue #1444</a>: Cannot launch settings dialog from shell context menu when selecting a file
 * Fixed <a href="/issue/1265">issue #1265</a>: MSVC crashing unloading TortoiseGit32.dll (faulting module TortoiseGit32.dll_unloaded)
 * Fixed <a href="/issue/772">issue #772</a>: Rebase may take extremely long time to open window

<a name="Release_1.7.13.0">Release 1.7.13.0</a>
Released: 2012-09-30

== Features ==
 * Fixed <a href="/issue/1224">issue #1224</a>: Still can't delete a remote tag
 * Fixed <a href="/issue/366">issue #366</a>: Log - multiline search
 * Fixed <a href="/issue/696">issue #696</a>: Allow specifying execute bit
 * Fixed <a href="/issue/1335">issue #1335</a>: Available the branch edit description
 * Fixed <a href="/issue/494">issue #494</a>: Implement a commit button in the sync dialog
 * Fixed <a href="/issue/1333">issue #1333</a>: Added support for "update-index --skip-worktree"
 * Fixed <a href="/issue/1350">issue #1350</a>: Commit Dialog set focus to message editor after clicking amend checkbox
 * Fixed <a href="/issue/811">issue #811</a>: Log the history of a submodule not its contents
 * Fixed <a href="/issue/1344">issue #1344</a>: Log list add copy to clipboard only with commit message
 * Fixed <a href="/issue/1336">issue #1336</a>: Have visible the name of the branch in Repository Browser
 * Fixed <a href="/issue/1351">issue #1351</a>: Add Hotkey to (de)select all files and commit
 * Fixed <a href="/issue/1363">issue #1363</a>: copy email in the log window
 * Fixed <a href="/issue/1373">issue #1373</a>: The only available remote should be set on PUSH
 * Fixed <a href="/issue/1216">issue #1216</a>: Revamp update dialog
 * Fixed <a href="/issue/1381">issue #1381</a>: Sync Dialog log message add colored "success" or "git did not exit cleanly" at the end
 * Fixed <a href="/issue/1372">issue #1372</a>: Support git pull --no-tags

== Bug Fixes ==
 * Fixed <a href="/issue/1062">issue #1062</a>: Log window should refresh automatically after reset --hard
 * Fixed <a href="/issue/641">issue #641</a>: Failure trying to revert a "merge"
 * Fixed <a href="/issue/1338">issue #1338</a>: Assume Unchanged and Executable Bit checkboxes not show in TortoiseShell tab page on x86
 * Fixed <a href="/issue/1341">issue #1341</a>: Overlays missing in TotalCommander (x86) with TGitCache on x64
 * Fixed <a href="/issue/1355">issue #1355</a>: Added files are no-longer auto-checked for commit
 * Fixed <a href="/issue/1356">issue #1356</a>: File ignoring works not well with unicode filenames
 * Fixed <a href="/issue/1354">issue #1354</a>: Remote archive in push dialog not remembered, even if selected
 * Fixed <a href="/issue/1358">issue #1358</a>: Sync Dialog Pull cannot show updated ref labels in "In commits"
 * Disabled Direct2D by default for textboxes (in Commit Dialog, Patch Viewer, TortoiseGitBlame and TortoiseUDiff).
 * Fixed <a href="/issue/1362">issue #1362</a>: Merge Dialog cannot automatically select the annotated tag
 * Fixed <a href="/issue/1357">issue #1357</a>: Cannot add file in submodule
 * Fixed <a href="/issue/1343">issue #1343</a>: should not set SCI_SETFONTQUALITY to SC_EFF_QUALITY_LCD_OPTIMIZED which bypasses user preference
 * Fixed <a href="/issue/1361">issue #1361</a>: git push: Entered source ref is ambiguous
 * Fixed <a href="/issue/1076">issue #1076</a>: Error trying to delete remote branch named 1.0.0
 * Fixed <a href="/issue/1375">issue #1375</a>: Wrong default action on dialog to delete references
 * Fixed <a href="/issue/1378">issue #1378</a>: Ignoring a file into info/exclude with seperate .git directory failed
 * Fixed <a href="/issue/1371">issue #1371</a>: Log window does not always remember column order correctly
 * Fixed <a href="/issue/1380">issue #1380</a>: When chosing Pull Remote Branch using Browse Reference the remote branch name won't get updated
 * Fixed <a href="/issue/1287">issue #1287</a>: Setting Remote URL not translated to '/' when clicking Apply or OK
 * Fixed <a href="/issue/1382">issue #1382</a>: Log messages with PGP signatures showing badly
 * Fixed <a href="/issue/1394">issue #1394</a>: Submodule Diff Dialog cannot jump to a commit that not belongs to current branch
 * Fixed <a href="/issue/1387">issue #1387</a>: Submodule Diff Dialog might display wrong submodule revision
 * Fixed <a href="/issue/1390">issue #1390</a>: TortoiseMerge cannot diff files after calling Export in Log Dialog
 * Fixed <a href="/issue/1395">issue #1395</a>: Problem restoring from earlier commit for file in a subdirectory
 * Fixed <a href="/issue/1392">issue #1392</a>: Window with rebase results is non-manageable via keyboard (no focus, can't press Done)
 * Fixed <a href="/issue/1405">issue #1405</a>: Cannot show branch description in Browse Reference Dialog with separate .git directory
 * Fixed <a href="/issue/1406">issue #1406</a>: Commit dialog cannot check MERGE_HEAD with separate .git directory
 * Fixed <a href="/issue/861">issue #861</a>: TGitCache gets confused about file status (should detect touched file as unmodified)
   TGitCache only checked timestamps of files in order to get their state since 1.7.0.
   This fix makes TortoiseGit check the content of the files.
   If you want to restore the old behavior, you can disable checking the contents via
   the Settings dialog -> Advanced and set TGitCacheCheckContent to "false".
 * Fixed <a href="/issue/1300">issue #1300</a>: TortoiseProc LogDialog might crash if tortoisegit.index or tortoisegit.data is broken
 * Fixed <a href="/issue/1413">issue #1413</a>: Submodule Diff Dialog: show log button should be right-aligned
 * Fixed <a href="/issue/1418">issue #1418</a>: Submodule Diff Dialog labels disappeared

<a name="Release_1.7.12.0">Release 1.7.12.0</a>
Released: 2012-08-10

== Features ==
 * Fixed <a href="/issue/238">issue #238</a>: submodule update should be recursive
 * Fixed <a href="/issue/1234">issue #1234</a>: Abbreviate renamings in the Log dialog as in the Commit Progress dialog
 * Fixed <a href="/issue/822">issue #822</a>: allow push to all remotes defined by checking a mark
 * Fixed <a href="/issue/1192">issue #1192</a>: fetch --all via dialog
 * Allow users to store pushremote and pushbranch on push dialog for local branches
 * Fixed <a href="/issue/1205">issue #1205</a>: Add "Last modified" column to commit dialog
 * Fixed <a href="/issue/1295">issue #1295</a>: Browse Ref Dialog ref name order consider numbers in string
 * Added unicode big-endian text file support to TortoiseMerge and TortoiseGitBlame
 * Fixed <a href="/issue/277">issue #277</a>: Add the ability to run the 'assume-unchanged' command
 * Fixed <a href="/issue/626">issue #626</a>: add "show log" in Git Sync dialog
 * Fixed <a href="/issue/884">issue #884</a>: Blame dialog Commit Info doesn't allow copy-paste
 * Fixed <a href="/issue/1165">issue #1165</a>: sort UI items alphabetically

== Bug Fixes ==
 * Fixed <a href="/issue/913">issue #913</a>: Merge does not cope with ambiguous tag and branch names
 * Fixed <a href="/issue/1266">issue #1266</a>: gitdll can cause stack overflow because of endless recursion in mark_parents_uninteresting (revision.c)
   Upgraded gitdll and libgit to 1.7.11.1
 * Fixed <a href="/issue/1267">issue #1267</a>: Tortoisegit adding gitignore backslash instead of slash
 * Fixed <a href="/issue/1260">issue #1260</a>: TortoiseGit crashes if index is broken
 * Fixed <a href="/issue/1280">issue #1280</a>: TortoiseProc might crash after displaying libgit error
 * Fixed <a href="/issue/1235">issue #1235</a>: TortoiseGIT commit dialog freezes if index.lock file already exists
 * Fixed <a href="/issue/1283">issue #1283</a>: TortoiseProc might crash after LogDialog is closed
 * Fixed <a href="/issue/1282">issue #1282</a>: gitdll.dll might crash if a pack file is closed which is still in use
 * Fixed <a href="/issue/1290">issue #1290</a>: When pushing, 'remote' should default to the tracked archive, or empty
 * Optimized remembering of checked/unchecked files on the commit dialog
 * Fixed <a href="/issue/767">issue #767</a>: Default column widths in log commit file list are off
 * Fixed <a href="/issue/1294">issue #1294</a>: TortoiseGit might crash on concurrent access on CGitHeadFileList
 * Fixed <a href="/issue/1279">issue #1279</a>: Nothing seen in Repository Browser without config core.bare=false
 * Fixed <a href="/issue/1296">issue #1296</a>: Closing log dialog might cause crash
 * Fixed <a href="/issue/1255">issue #1255</a>: Adding non-versioned files from the commit dialog does not always work
 * Fixed <a href="/issue/620">issue #620</a>: Unmodified files appear in the Modified File list while committing
 * Fixed <a href="/issue/774">issue #774</a>: Revert does not work for submodules
 * Fixed <a href="/issue/1303">issue #1303</a>: Past Recent Message command in commit dialog doesn't work
 * Fixed <a href="/issue/1311">issue #1311</a>: Open With is broken
 * Fixed <a href="/issue/898">issue #898</a>: Committed everything even though I only selected one file
 * Fixed <a href="/issue/693">issue #693</a>: Ignoring *.[mime] in the commit window doesn't refresh file list
 * Fixed <a href="/issue/1318">issue #1318</a>: Higher dir combox box in format patch dialog
 * Fixed <a href="/issue/830">issue #830</a>: Renaming file with differences only in casing doesn't work
   Added basic support only: the overlay status might be wrong (file shown as clean or modified instead of added)
 * Fixed <a href="/issue/867">issue #867</a>: Add should not run a new Git process for each file

<a name="Release_1.7.11.3">Release 1.7.11.3</a>
Released: 2012-07-07

== Bug Fixes ==
 * Fixed <a href="/issue/1256">issue #1256</a>: TortoiseProc crashes on composing invalid ref error message
 * Fixed <a href="/issue/1254">issue #1254</a>: Adding file extensions to root .gitignore
 * Fixed <a href="/issue/1261">issue #1261</a>: gitdll.dll can crash if an already closed handle is closed again
 * Fixed <a href="/issue/1257">issue #1257</a>: HOME environment variable is not set up correctly on x86
 * Fixed <a href="/issue/1262">issue #1262</a>: TortoiseProc might crash if git.exe version is not parseable
 * Fixed <a href="/issue/1264">issue #1264</a>: TortoiseProc might crash if commands are executed w/o a working tree directory

<a name="Release_1.7.11.0">Release 1.7.11.0</a>
Released: 2012-07-02

== Features ==
 * Push Dialog: Allow to enter SHA-1 or more complex refs as source ref
 * Fixed <a href="/issue/746">issue #746</a>: Make tortoisegit more gerrit friendly
 * Optimized TGitCache overlay calculation (deleted but kept files are now displayed as such)
 * Fixed <a href="/issue/1067">issue #1067</a>: Add clear button into "stash list"
 * Fixed <a href="/issue/1230">issue #1230</a>: Filter/Search for Branches in Reference Browser
 * Fixed <a href="/issue/1228">issue #1228</a>: Ignore by mask work recursively
 * Fixed <a href="/issue/1086">issue #1086</a>: Partial commit resets "Added" status on files omitted from checkin
 * Fixed <a href="/issue/1250">issue #1250</a>: Sync Dialog should reload remote combo box after clicking "Manage"
 * Fixed <a href="/issue/462">issue #462</a>: Implement 'All' and 'None' opportunities in the Set Extended menu settings dialog
 * TortoiseGit 1.7.11 comes with a new crash handler (crash-server.com) and lots of crash fixes (found using crash reports in our preview releases)

== Bug Fixes ==
 * Fixed <a href="/issue/1213">issue #1213</a>: cannot diff renamed files with Revision Diff Dialog
 * Fixed <a href="/issue/1215">issue #1215</a>: Branch name in the right click menu (for commit) does not support utf-8
 * Fixed <a href="/issue/652">issue #652</a>: "Clean up" should use recycle bin
 * Fixed <a href="/issue/1166">issue #1166</a>: Unreadable error message in TortoiseMerge
 * Fixed <a href="/issue/665">issue #665</a>: Problem with temporary file creation
 * Fixed <a href="/issue/1221">issue #1221</a>: Log Dialog revert files shows wrong number of files in message
 * Fixed <a href="/issue/1207">issue #1207</a>: TortoiseGit 1.7.10 crashes directly when trying to start
 * Fixed <a href="/issue/1226">issue #1226</a>: Show log crash when path contain some unicode symbols
 * Fixed <a href="/issue/1229">issue #1229</a>: Progress Window Gains Focus on Command Completion
 * Fixed <a href="/issue/1231">issue #1231</a>: incorrect behavior of squash in rebase/cherry pick
 * Fixed <a href="/issue/1233">issue #1233</a>: Could not go to annotated tag
 * Fixed <a href="/issue/947">issue #947</a>: The "Git check for modifications" form does not remember the setting for "Show unversioned files"
 * Fixed <a href="/issue/1093">issue #1093</a>: Directory incorrectly shown as changed (overlay icon)

<a name="Release_1.7.10.0">Release 1.7.10.0</a>
Released: 2012-06-03

== Features ==
 * Updated shipped zlib library to version 1.2.7
 * Synced TortoiseIDiff with TortoiseSVN
 * Synced TortoiseUDiff with TortoiseSVN
 * Fixed <a href="/issue/1150">issue #1150</a>: Add support for ANSI Color Codes in log Outputs
 * Fixed <a href="/issue/969">issue #969</a>: Support for localization (you can download/create language packs for TortoiseGit, see <a href="https://tortoisegit.org/translate">https://tortoisegit.org/translate</a>)
 * Added Repository Browser
 * Updated shipped libgit2 to version 0.17.0
 * Fixed <a href="/issue/1169">issue #1169</a>: Show Branch for Commits
 * Fixed <a href="/issue/337">issue #337</a>: TortoiseGit Log doesn't apply --follow (added basic support for --follow)

== Bug Fixes ==
 * Fixed <a href="/issue/1162">issue #1162</a>: Sync dialog should use same font as progress dialog (use log font for both)
 * Fixed <a href="/issue/1174">issue #1174</a>: Clone Dialog incorrectly extracts name from URL that contains ".git" in the middle
 * Fixed <a href="/issue/1184">issue #1184</a>: diff of "Working dir changes" uses incorrect line endings (AutoCrLf=true)
 * Fixed <a href="/issue/1193">issue #1193</a>: Not properly handling submodule meta information (.git-file contains relative path)
 * Fixed <a href="/issue/1167">issue #1167</a>: TGitCache.exe crashes under Win7/64bit
 * Fixed <a href="/issue/1195">issue #1195</a>: Problems with tags containing non-ascii chars
 * Fixed <a href="/issue/1185">issue #1185</a>: In TortoiseGit log dialog info text, diff dialog and TortoiseGitBlame, git shows 7 chars of the hash by default (as git does)
 * Fixed <a href="/issue/1197">issue #1197</a>: Create branch: Alt+S doesn't mark "Switch to new branch"
 * Fixed <a href="/issue/1152">issue #1152</a>: Log-Dialog constantly crashes when diffing a file while loading
 * Fixed <a href="/issue/719">issue #719</a>: TortoiseGit is ignoring Global excludesfile for overlays
 * Fixed <a href="/issue/1199">issue #1199</a>: TortoiseGitBlame shows wrong revision history
 * Fixed <a href="/issue/1157">issue #1157</a>: Custom screen DPI causes wrong text size calculation

<a name="Release_1.7.9.0">Release 1.7.9.0</a>
Released: 2012-05-05

== Features ==
 * Allow to fetch from log dialog
 * Fixed <a href="/issue/1058">issue #1058</a>: Allow to create tag after commit
 * Fixed <a href="/issue/101">issue #101</a>: UTF-8 support
   This makes TortoiseGit msysgit 1.7.10 compatible. If you have non-ascii chars in filenames in your repository you should consider rewriting your history
   (see <a href="https://github.com/kblees/git/wiki#wiki-Migrating_old_Git_for_Windows_repositories" target="_blank">https://github.com/kblees/git/wiki#wiki-Migrating_old_Git_for_Windows_repositories</a>).
 * Added "prune/cleanup stale remote tracking branches" option to sync dialog

== Bug Fixes ==
 * Fixed <a href="/issue/1123">issue #1123</a>: Apply patch serial does not correctly add patches to list if >3 patches are selected
 * Fixed <a href="/issue/1061">issue #1061</a>: Enviroment variables ignored: GIT_AUTHOR_NAME and GIT_AUTHOR_EMAIL
 * Fixed <a href="/issue/1028">issue #1028</a>: overlays with separate-git-dir (e.g. .git\modules\... for submodules) not working
 * Fixed <a href="/issue/1131">issue #1131</a>: "Whole project" checkbox in commit dialog selects all files
 * Fixed <a href="/issue/1065">issue #1065</a>: Explorer crashes when rendering a file conflict icon
 * Fixed <a href="/issue/1027">issue #1027</a>: When post-receive hook contains more than 1 echo bash command, only the LAST echo is displayed in dialog
 * Fixed <a href="/issue/1136">issue #1136</a>: Proxy password with @ symbol not parsed/saved correctly
 * Fixed <a href="/issue/1135">issue #1135</a>: branch name does not get updated in show log
 * Fixed <a href="/issue/153">issue #153</a>: Empty error messagebox when starting rebase dialog
 * Fixed <a href="/issue/1140">issue #1140</a>: Settings page does not inherit msysgit system-wide config
 * Fixed <a href="/issue/1146">issue #1146</a>: Add hotkey for option "All Branches" in log dialog
 * Fixed <a href="/issue/1141">issue #1141</a>: Sync dialog local branch combo box too short
 * Fixed <a href="/issue/1130">issue #1130</a>: Sync dialog forgets the remote branch every time
 * Fixed <a href="/issue/1144">issue #1144</a>: Cancel button on commit dialog works very slow
 * Fixed <a href="/issue/1155">issue #1155</a>: TortoiseGit SVN rebase finish dialog produces very big dialog box

<a name="Release_1.7.8.0">Release 1.7.8.0</a>
Released: 2012-04-01

== Features ==
 * Updated shipped zlib library to version 1.2.6
 * Updated shipped apr library to version 1.4.6
 * Updated shipped libgit2 to version 0.16.0
 * When deleting remote branch on log dialog one can also delete branch in remote repository now.
 * Fixed <a href="/issue/1103">issue #1103</a>: cherrypick option to indicate original commit
 * Fixed <a href="/issue/1094">issue #1094</a>: Support filter command line options for log
 * Fixed <a href="/issue/435">issue #435</a> and <a href="/issue/1102">issue #1102</a>: Feature: "Save as patch" from Diff window

== Bug Fixes ==
 * Fixed <a href="/issue/1085">issue #1085</a>: Small commit with lots of unversioned and not ignored files takes very long
 * Fixed <a href="/issue/1098">issue #1098</a>: 'In ChangeList' is cleared when you click a table header
 * Fixed <a href="/issue/1073">issue #1073</a>: Correct default name in Switch/Checkout dialog
 * Fixed <a href="/issue/1110">issue #1110</a>: Tracking branches when you don't want to/incorrectly
 * Fixed <a href="/issue/1115">issue #1115</a>: Detached HEAD message has no cancel/abort option
 * Fixed <a href="/issue/1112">issue #1112</a>: Rebase corrupt commit message (ignores windows codepage and converts everything to utf-8)
 * Fixed <a href="/issue/985">issue #985</a>: Correctly store status for view patch in commit dialog
 * Fixed <a href="/issue/1116">issue #1116</a>: Make remote branch in pull dialog default to the current branch (or remote tracking branch)
 * Fixed <a href="/issue/1060">issue #1060</a>: tgit rebase process can get stuck
 * Fixed <a href="/issue/1107">issue #1107</a>: Wrong font used in MessageBoxes on Windows XP
 * Fixed <a href="/issue/1105">issue #1105</a>: Wrong status for missing files
 * Fixed some TortoiseMerge patch apply issues (includes <a href="/issue/1117">issue #1117</a>)
 * Fixed <a href="/issue/718">issue #718</a>: When pulling it should be possible to directly open the modifications window for editing conflicts
 * Fixed <a href="/issue/982">issue #982</a>: TortoiseGit messes up TortoiseSVN and TortoiseCVS menu icons on WinXP
 * Fixed <a href="/issue/1118">issue #1118</a>: Exporting from Changed Files dialog stops on checkout failure
 * Fixed <a href="/issue/1108">issue #1108</a>: Allow users to disable the "show unversioned files" messagebox in commit dialog

== Known Issue ==
 * separate-git-dir .git-directories do not work for overlays

<a name="Release_1.7.7.0">Release 1.7.7.0</a>
Released: 2012-02-09

== Features ==
 * Fixed <a href="/issue/758">issue #758</a>: Make the log of a bare repository available
 * Fixed <a href="/issue/880">issue #880</a>: Allow sync / fetch / push on bare repository
 * Fixed <a href="/issue/818">issue #818</a>: SafeCRLF = warn should be available
 * Fixed <a href="/issue/855">issue #855</a>: moved up Git page in settings dialog
 * Fixed <a href="/issue/999">issue #999</a>: Added "Diff submodule" dialog
 * Fixed <a href="/issue/1019">issue #1019</a>: better indicate errors to user on progress dialog
 * Added "Show Log before rename/copy" on log dialog file list
 * Added basic support for separate-git-dir (Overlays are not working yet)
 * Fixed <a href="/issue/1042">issue #1042</a>: Allow to delete branch after successful merge
 * Fixed <a href="/issue/1040">issue #1040</a>: Commit to new branch
 * Fixed <a href="/issue/1043">issue #1043</a>: Add 'Stash' to 'Working dir changes' context menu in log dialog
 * Added support for Windows 7 libraries
 * Fixed <a href="/issue/222">issue #222</a>: support partially committing a file

== Bug Fixes ==
 * Updated shipped PuTTY binaries to version 0.62
 * Fixed <a href="/issue/1000">issue #1000</a>: Git commit doesn't work
 * Fixed <a href="/issue/587">issue #587</a>: Log entries with tag/branch/head marker don't highlight correctly
 * Fixed <a href="/issue/1004">issue #1004</a>: git svn fetch/rebase/clone return an error for ssh authorization
 * Reenabled --topo-order for log. Users who do not want this, can disable it on settings dialog
 * Fixed <a href="/issue/1024">issue #1024</a>: commit freezes after click on ok with a lot of files
 * Fixed <a href="/issue/1026">issue #1026</a>: Cannot configure (and autoload) putty key for submodules with new mSysGit.
 * Fixed <a href="/issue/849">issue #849</a>: Searching in log breaks graph/branch-line
 * Fixed <a href="/issue/1030">issue #1030</a>: Does not close handle after export to zip
 * Fixed <a href="/issue/1025">issue #1025</a>: branch is not recognized correctly with separate-git-dir
 * Fixed <a href="/issue/1022">issue #1022</a>: Cannot add files to submodules with-separate-git-dir
 * Fixed <a href="/issue/1008">issue #1008</a>: Bug Id not displayed in Log Messages (multiline comment)
 * Fixed <a href="/issue/1020">issue #1020</a>: Incorrect Overlays on large repos (> 2 GiB), upgraded libgit2
 * Fixed <a href="/issue/607">issue #607</a> and <a href="/issue/1005">issue #1005</a>: Tortoisegit overlay icons are not correctly shown for new create local repo
 * Fixed <a href="/issue/1051">issue #1051</a>: Adding files using the add files dialog doesn't normalize line endings
 * Fixed <a href="/issue/582">issue #582</a>, <a href="/issue/956">issue #956</a>, <a href="/issue/960">issue #960</a>, <a href="/issue/973">issue #973</a>, <a href="/issue/980">issue #980</a>, <a href="/issue/959">issue #959</a> and <a href="/issue/1016">issue #1016</a>: 100% CPU, icons blinking and refreshing bugs in TGitCache
 * Fixed <a href="/issue/1044">issue #1044</a>: Add file to index with special character (ei � "dash") or accent fail without any message

== Known Issue ==
 * separate-git-dir .git-directories do not work for overlays

<a name="Release_1.7.6.0">Release 1.7.6.0</a>
Released: 2011-12-10

== Features ==
 * Fixed <a href="/issue/972">issue #972</a>: Add "fast forward only" checkbox in pull dialog
 * Fixed <a href="/issue/976">issue #976</a>: Compare Revisions should inherit path filter
 * Fixed <a href="/issue/869">issue #869</a>: TortoiseProc CloneCommand is ignoring "url" command line parameter
 * Fixed <a href="/issue/820">issue #820</a>: Missing menu item for "git svn fetch"
 * Fixed <a href="/issue/792">issue #792</a>: Fetch progress window should have a "Log" button
 * Fixed <a href="/issue/451">issue #451</a>: rebase (maybe be for any conflicts) select multiple files to mark
 * Fixed <a href="/issue/894">issue #894</a>: Make images "open with" TortoiseIDiff
 * Fixed <a href="/issue/985">issue #985</a>: Store status for view patch in commit dialog
 * Fixed <a href="/issue/977">issue #977</a>: Fetch does not have an option to fetch tags
 * Fixed <a href="/issue/801">issue #801</a>: Be able to enter a custom stash message
 * Fixed <a href="/issue/933">issue #933</a>: implement git stash --include-untracked
 * Fixed <a href="/issue/987">issue #987</a>: Add Stash save/pop to "Working Dir Changes" dialog
 * Fixed <a href="/issue/988">issue #988</a>: Stash pop/apply fail should have a button for viewing conflicts
 * Fixed <a href="/issue/676">issue #676</a>: Add remove remote tag ability
 * Fixed <a href="/issue/678">issue #678</a>: TortoiseGitBlame log incomplete (integrated --follow into TortoiseGitBlame)

== Bug Fixes ==
 * Fixed <a href="/issue/747">issue #747</a>: TortoiseProc & less process not closing
 * Fixed <a href="/issue/808">issue #808</a>: Less: file viewer stop work when use show-low
 * Fixed <a href="/issue/931">issue #931</a>, <a href="/issue/934">issue #934</a> and <a href="/issue/948">issue #948</a>: TortoiseProc crashes when repo contains huge files (on x86)
   Upgraded gitdll, libgit and tgit.exe to 1.7.7.2
 * Fixed <a href="/issue/936">issue #936</a>: Applying a stash from the list fails
 * Fixed <a href="/issue/962">issue #962</a>: git_init in gitdll.c uses USERPROFILE for creating home
 * Fixed <a href="/issue/964">issue #964</a>: Can't mark conflict as resolved
 * Fixed <a href="/issue/867">issue #867</a>: Add should not run a new Git process for each file
 * Fixed <a href="/issue/981">issue #981</a>: After Aborting a Rebase a different Commit is checked out
 * Fixed <a href="/issue/670">issue #670</a>: Cannot delete/apply individual stash entries
 * Fixed <a href="/issue/863">issue #863</a>: Commit and revert gets stuck on repositories with mixed line-endings
 * Fixed <a href="/issue/378">issue #378</a>: CRLF/LF conversion causes commit dialog to freeze
 * Fixed <a href="/issue/858">issue #858</a>: Tortoise Git Client crash when showing the log dialog
 * Fixed <a href="/issue/920">issue #920</a>: TortoiseGitBlame and UTF-16 LE
 * updated documentation
 * Fixed <a href="/issue/994">issue #994</a>: Git clone from SVN - Depth field is useless
 * Fixed <a href="/issue/996">issue #996</a>: Compare with Working Copy error on renamed files

<a name="Release_1.7.5.0">Release 1.7.5.0</a>
Released: 2011-11-09

== Features ==
 * Fixed <a href="/issue/260">issue #260</a>: Added support for bisect
 * Fixed <a href="/issue/916">issue #916</a>: Fixed wrong contextmenu icon display position with installed TSVN 1.7
 * Add new command to diff two selected files in the CGitStatusListCtrl.

== Bug Fixes ==
 * Fixed <a href="/issue/931">issue #931</a>: TGitCache: Missing NULL check
 * Fixed <a href="/issue/935">issue #935</a>: TortoiseMerge removes newlines at file end
 * Fixed <a href="/issue/931">issue #931</a>: libgit2 had problems with big pack files
 * Fixed <a href="/issue/486">issue #486</a>: Resolving conflicts can result in nothing to commit and branch merge not concluded
 * Fixed <a href="/issue/938">issue #938</a>: *Backlash* between user name and domain name is incorrectly replaced by *Slash*
 * Fixed <a href="/issue/941">issue #941</a>: Performing diff on an unchanged file behaves like a diff with an added file
 * Fixed <a href="/issue/928">issue #928</a>: Mark diff-tempfiles as read-only
 * Fixed <a href="/issue/951">issue #951</a>: Alt+S conflict for sign and set date
 * Fixed <a href="/issue/949">issue #949</a>: TortoiseMerge does not allow copying to the clipboard via Ctrl+Insert
 * Fixed <a href="/issue/950">issue #950</a>: avoid possible crash if .git/packed-refs contains annotated tags
 * Fixed <a href="/issue/915">issue #915</a>: TortoiseProc memory corruption on empty repo
 * Fixed <a href="/issue/959">issue #959</a>: Overlay icons are not updated immediately

<a name="Release_1.7.4.0">Release 1.7.4.0</a>
Released: 2011-10-10

== Features ==
 * Add Show Environment variables at setting dialog to help debug user problem.
 * Allow users to be warned if there is no Signed-Off-By line in a commit message
 * Fixed <a href="/issue/375">issue #375</a>: Implement --date/time option gui interface in the commit dialog
 * Fixed <a href="/issue/814">issue #814</a>: Remember last selected commit/line on log filtering
 * Allow to "Update submodules" after pull or hard reset
 * Fixed <a href="/issue/780">issue #780</a>: "Merge to [branch]..." should pre-select the chosen commit's branch or tag name
 * Fixed <a href="/issue/459">issue #459</a>: Implement more talkative name than just 'Revert fail'

== Bug Fixes ==
 * Fixed <a href="/issue/899">issue #899</a>: Push via Showlog  use Push with force option
 * Fixed <a href="/issue/893">issue #893</a>: "Show Unified Diff" on file entry in "show log" has changes backwards
 * Fixed <a href="/issue/881">issue #881</a>: "Create repository here" should warn if target directory is not empty
 * Fix wrong contextmenu icon display position when install TSVN 1.7 on WinXP
 * TGitCache: Fix sometime project root show as unversioned icon
 * Fixed <a href="/issue/862">issue #862</a> and <a href="/issue/870">issue #870</a>: TGitCache: Fix sometime show "+" at tracked items
 * Upgraded libgit2 to 0.15.0
 * Fixed <a href="/issue/716">issue #716</a>: Aborted clone leaves git process running
 * Fixed <a href="/issue/787">issue #787</a>: Problem of setting proxy for work using HTTPS
 * Fixed <a href="/issue/908">issue #908</a>: TortoiseGit crashes if .git/config-file is broken
 * Fixed <a href="/issue/909">issue #909</a>: Cannot amend initial commit
 * Fixed <a href="/issue/829">issue #829</a>: "Remote" combobox in "Push" window updates only after losing focus
 * Fixed <a href="/issue/906">issue #906</a>: Add ability to add files with options -force.
 * Fixed <a href="/issue/914">issue #914</a>: unifiled diff always show wrong changes (base files compare with new files)
 * Fixed <a href="/issue/673">issue #673</a>: Applying a patch does not honour CRLF in files
 * Fixed <a href="/issue/713">issue #713</a>: apply serial patch window need to improve
 * Fixed <a href="/issue/922">issue #922</a>: Settings / Git / Config / Edit global .gitconfig uses wrong path

<a name="Release_1.7.3.0">Release 1.7.3.0</a>
Released: 2011-08-24

== Features ==
 * Fixed <a href="/issue/872">issue #872</a>: 32 shell extension should be made optional on x64 installation

== Bug Fixes ==
 * Fixed <a href="/issue/833">issue #833</a>: Password with Percent (%) is not accepted
 * Fixed <a href="/issue/852">issue #852</a>: Incorrect current version string in Check For Updates dialog on x86
 * Fixed <a href="/issue/850">issue #850</a>: 32bit application can't show icon overlay at 64bit system.
 * Fixed <a href="/issue/864">issue #864</a>: Show log crashes when commit with "encoding" in comment exists
 * Fixed <a href="/issue/851">issue #851</a>: Windows Explorer Shell Crashed when empty repository
 * Fixed open handles in TGitCache
   Fixed <a href="/issue/497">issue #497</a>, <a href="/issue/623">issue #623</a> and <a href="/issue/892">issue #892</a>: TortoiseGit locks repository folders so that the user can't delete them
 * Fixed <a href="/issue/870">issue #870</a>: Wrong overlay icons
 * Fixed <a href="/issue/793">issue #793</a>: Context menu for files does not contain "Add to ignore list"
 * Fixed <a href="/issue/860">issue #860</a>: Commit file moves does not properly remove the source file
 * Fixed <a href="/issue/386">issue #386</a>: Commit dialog does not preserve selection when toggling "Whole project"

<a name="Release_1.7.2.0">Release 1.7.2.0</a>
Released: 2011-08-08

== Features ==
 * Added shortcuts to the Windows 7 taskbar jumplist
 * Allow to clone recursively
 * Fixed <a href="/issue/841">issue #841</a>: Pull dialog should have "no fast-forward" option like the merge dialog

== Bug Fixes ==
 * Fixed blame crash at XP system.
 * Upgraded TortoisePlink to TortoiseSVN rev. 21694, fixes a bug with shipped Pageant under x64
 * Updated shipped apr library to version 1.4.5
 * Updated shipped apr-util library to version 1.3.12
 * Updated shipped zlib library to version 1.2.5
 * Fixed <a href="/issue/783">issue #783</a>: Git Command Progress window should close with Escape
 * Fixed <a href="/issue/499">issue #499</a>: Git Command Progress window does not close from X-closebutton
 * Fixed <a href="/issue/844">issue #844</a>: Git Command Progress window, "Writing Objects" , progress info refresh with glitches.

<a name="Release_1.7.1.0">Release 1.7.1.0</a>
Released: 2011-07-29 (internal release)

== Features ==
 * Fixed <a href="/issue/828">issue #828</a>: disable the commit button if there's no comment entered

== Bug Fixes ==
 * Fixed <a href="/issue/796">issue #796</a>: plz add code page name "cp949"
 * Fixed <a href="/issue/795">issue #795</a>: Switch/Checkout Dialog, "Switch To Version" is confusing
 * Fixed <a href="/issue/757">issue #757</a>: TortoiseGit Blame not working from working dir with autocrlf-enabled.
 * Fixed <a href="/issue/691">issue #691</a>: 64-bit version should include the 32-bit shell extension too
 * Updated shipped PuTTY binaries to version 0.61
 * Updated shipped notepad2 binaries to version 4.2.25 and added x64 version
 * Updated shipped TortoiseOverlay to version 1.1.3.21564
 * Fix crash for init repository
 * Fixed <a href="/issue/799">issue #799</a>: Explore crash with empty directory
 * Fixed <a href="/issue/816">issue #816</a>: Output in Git Command Progress is broken

<a name="Release_1.7.0.0">Release 1.7.0.0</a>
Released: 2011-05-11

== Features ==
 * Fixed <a href="/issue/724">issue #724</a>: BrowseRefs: Made branch renaming possible.
 * Use fetch-dialog for "Fetch" in BrowseRefs
 * Allow to edit user signingkey in TortoiseGit->Settings->GitConfig
 * Allow to sign tags (requires GPG and a key without passphrase)
 * Allow to enter CC recipients with MAPI
 * Do not add Signed-off-by if already included
 * Do not add another empty line if there are already some Reviewed-by or Signed-off-by lines at the end of the commit message on signing.
 * Allow to start push dialog from log
 * Show changes to revision before the last commit on amend and perform actions relatively to this revision (old behavior is still possible)
 * Remember (de)selected files in "Commit dialog" after refreshing or ordering the list
 * Fixed <a href="/issue/745">issue #745</a>: Added checkbox to commit dialog to disable autoselection of submodules
 * Show status on taskbar button on Windows 7
 * Allow to add a "Signed-Off-By" line to patches on applying
 * Fixed <a href="/issue/781">issue #781</a>: Allow to push all branches at once
 * Fixed <a href="/issue/784">issue #784</a>: Rebase window should allow easier selection of Pick/Squash/Edit/Skip (keys: space: shifts the state, s: skip, e: edit, p: pick, q: squash)
 * Fixed <a href="/issue/785">issue #785</a>: Rebase window should list the contents of a commit just like Log window does
 * Fixed <a href="/issue/768">issue #768</a>: Display modified files in "Reset" dialog

== Bug Fixes ==
 * Fixed tab indices (activation order)
 * Fixed missing putty-key for deleting a remote branch
 * Fixed <a href="/issue/728">issue #728</a>: Shell "Diff With Previous" doesn't work when there are more than 2 revisions for that file and diff working copy to HEAD~1
 * Fixed 32/64-bit MAPI inconsistency-issues
 * Do not include patch in mail if user selected attachment
 * Fix attaching of patches to mails with some MAPI clients
 * Fixed sending patches combined in one mail w/o attachments (files were always attached)
 * Make change setting "hide TGit menu" work
 * Fixed <a href="/issue/729">issue #729</a>: SVN DCommitt incorrectly executes with --rmdir
 * Fixed <a href="/issue/732">issue #732</a>: Synch-dialog layout broken
 * Fixed <a href="/issue/734">issue #734</a>: git not found: Fixed possible problems with folders containing spaces
 * Fixed <a href="/issue/735">issue #735</a>: Log generates file stats from shown parent instead of actual parent
 * Do not show "Diff with previous" for added files
 * Fixed <a href="/issue/737">issue #737</a>: Diffing of added files does not work with Shell
 * Fixed <a href="/issue/738">issue #738</a>: Lost the commit id on file name when use a external diff tool
 * Fixed <a href="/issue/714">issue #714</a>: add "FETCH_HEAD" to reference drop down list
 * Fixed <a href="/issue/548">issue #548</a>:  tortoisegit use incorrect case sensitive comparison
 * Fixed <a href="/issue/727">issue #727</a>: /CloseOnEnd not working for commit
 * Fixed <a href="/issue/754">issue #754</a>: Allow to show log for files in "Changes files" dialog
 * Fixed <a href="/issue/749">issue #749</a>: Ask before delete files.
 * Fixed <a href="/issue/125">issue #125</a>: Export files from revision or range of revisions in "Changes files" dialog.
 * Fixed <a href="/issue/512">issue #512</a>: Git sync lose local commits (remote update, fetch and rebase)
 * Fixed <a href="/issue/766">issue #766</a>: "Switch/Checkout" dialog: "Track" should be disabled when no new branch is created
 * Fixed <a href="/issue/765">issue #765</a>: "Check for Updates" in about box doesn't work
 * Fixed <a href="/issue/731">issue #731</a>: Git Command Progress Window text box doesn't have a context menu.
 * Rebase failed at revision with empty commit message.
 * Fixed sorting of columns of file lists (e.g. commit dialog).
 * Fixed <a href="/issue/757">issue #757</a>: TortoiseGit Blame not working from working dir.
 * Fixed layout issues with Windows 7 Aero
 * TortoiseProc.exe sometimes didn't exit correctly after closing the log dialog
 * Fixed <a href="/issue/761">issue #761</a>: Settings should correctly deal with backslashes (windows path separators) in entered remote URLs
 * Patch Viewer might display no horizontal scrollbar
 * Fixed <a href="/issue/779">issue #779</a>: Show correct text in taskbar when rebasing finished.
 * Pushing required a remote-branch name
 * Fixed <a href="/issue/790">issue #790</a>: Add minimize button to progress window
 * Fixed some optical issues in Rebase-Dialog
 * Fixed <a href="/issue/791">issue #791</a>: /CloseOnEnd not working for switch
 * TortoiseGitCache
   Partly rewritten to fix various issues.
 * Fixed <a href="/issue/415">issue #415</a>: Fix the Settings/General/Context menu/Apply operation (missing LF trimming)

<a name="Release_1.6.5.0">Release 1.6.5.0</a>
Released: 2011-02-20

== Bug Fixes ==
 * Fixed <a href="/issue/715">issue #715</a>: Unable to show log when there are old version cache file
 * Fixed <a href="/issue/611">issue #611</a>: Add "copy all information" to "Changed Files" dialog
 * Fixed new file miss when combine commits at log dialog
 * Fixed <a href="/issue/720">issue #720</a>: Infinite loop at search in Show Log when there are notes

<a name="Release_1.6.4.0">Release 1.6.4.0</a>
Released: 2011-02-14

== Features ==
 * Significantly Improve Log fetch speed for big repository
   Fetch modified file list asynchronous.
   Time filter (From, to) use git built-in --max-age and --min-age.
   Text filter use git grep.
   Fixed <a href="/issue/590">issue #590</a>: wasteful use of memory with very large repository
   Fixed <a href="/issue/531">issue #531</a>: Git synchronization UI opened so slowly
   Fixed <a href="/issue/541">issue #541</a>: show log is extremely slow
   Fixed <a href="/issue/364">issue #364</a>: Log - hot key for "browse refs" dialog

 * Improve TortoisePlink 3x transfer perfomance
   Update TortoisePlink to Plink 9078

 * Implemented <a href="/issue/664">issue #664</a>: Warn when committing to detached HEAD
 * The context menu can be hidden completely for unversioned items (issue 674)
 * Only show DCommit type dialog if "svn.rmdir" is unset
 * Optionally remember DCommit type setting
 * enable git status column in TortoiseShell
 * Fixed <a href="/issue/644">issue #644</a>: Dropped "Check repository" button on check for modifications dialog
 * Allow to diff two revisions of a file by calling TortoiseProc
 * Fixed <a href="/issue/480">issue #480</a>: Implement text copying opportunities in the dialogs
 * Allow to change EOL by pressing CTRL+Return in TortoiseMerge
 * Allow to replace (previously hardcoded) Notepad2 by any other editor
 * Optionally send/mail patches via MAPI, if a default mail client is set up
 * Fixed <a href="/issue/248">issue #248</a>: Allow to reorder commits on rebase
 * Fixed <a href="/issue/702">issue #702</a>: Added request-pull functionality

 * TortoiseGitBlame
   Clicking on a line automatically selects the log entry in the loglist
   Allow to diff to previous revision of a file
   Added new context menu
   Allow to toggle author column

== Bug Fixes ==
 * Fixed <a href="/issue/669">issue #669</a>: cannot open help from clean window
 * Fixed <a href="/issue/671">issue #671</a>: Help not working when choose switch dialog and dcommit dialog
 * Fixed <a href="/issue/690">issue #690</a>: Superfluous line in displayed commit message
 * Do not allow to delete-ignore working copy root-directory
 * Starting TortoiseGitBlame might fail to start if folder contains spaces
 * Fixed window titles of log and statistics window
 * Fixed <a href="/issue/697">issue #697</a>: /CloseOnEnd was not working, fixed for fetch&pull

 * Fixed issues with the send mail dialog
   If all three attempts failed, do not show success
   If all three attempts failed, do not go on sending more patches
   Correctly show retries
   Interpret user cancel as failure

 * TortoiseGitBlame
   Fixed <a href="/issue/448">issue #448</a>: Disable personalized menu behaviour
   After blaming an older revision, TortoiseGitBlame was fixed to this.

 * Fixed <a href="/issue/704">issue #704</a>: cannot open help from diff from previous, browse refs
 * Fixed <a href="/issue/694">issue #694</a>: "Clean Up" executes on top level directory
 * Fixed <a href="/issue/680">issue #680</a>: StatGraphDlg.cpp min-avg statistics are incorrect
 * Fixed <a href="/issue/705">issue #705</a>: Fixed comparing added/deleted files on diffing whole revisions
 * Improved "Combine commits" process (prevents possible loss of data)

<a name="Release_1.6.3.0">Release 1.6.3.0</a>
Released: 2011-01-14

== Features ==
 * Improved log dialog
   Fixed <a href="/issue/662">issue #662</a>: Allow to filter for paths
   Added hash column
   Show BugID when user config bugtraq.logregex

 * Improved commit dialog
   Removed useless options from the contextmenu (e.g. file operations for directories/submodules)
   allow to ignore deleted or unversioned files

 * Improved changed files dialog
   Removed useless options from the contextmenu (e.g. file operations for directories/submodules)
   Fixed <a href="/issue/618">issue #618</a>: Added a commit button
   allow to ignore deleted or unversioned files

 * Rewrite patch import dialog
   Patch import dialog look like sync dialog.
   User can know which patch fail import easily.
   Show patch import progress.
   Add 3way and ignore space option.
   Fix many issues about patch import dialog (<a href="/issue/252">issue #252</a>, <a href="/issue/324">issue #324</a>, <a href="/issue/332">issue #332</a> and <a href="/issue/430">issue #430</a>)

 * Include version information for all executables

 * Improved TortoiseBlame
   Fixed <a href="/issue/490">issue #490</a> and <a href="/issue/436">issue #436</a>: Allow to blame older revisions in TortoiseGitBlame
   Removed useless options from the contextmenu (e.g. compare to working copy)
   Fixed <a href="/issue/658">issue #658</a>: Added author column

 * Fixed <a href="/issue/323">issue #323</a>: implement DCommit type

 * ask user if he wants to stash pop after "SVN fetch"

 * allow to override branch on switch/checkout dialog

 * allow to prune on fetch

 * allow to edit global and local .gitconfig

 * Fixed <a href="/issue/655">issue #655</a>: remember previously selected features on upgrade

== Bug Fixes ==
 * Fixed <a href="/issue/663">issue #663</a>, <a href="/issue/656">issue #656</a> and <a href="/issue/77">issue #77</a>: allow to diff added or deleted files
 * Commit and changed files dialog
   Fixed possible hangs
   double-click default to open files for unversioned files
   make double-click on newly added file for diff work
 * TortoiseShell
   Fixed an assertion (when executed on ignored files)
 * Optical and smaller optimizations/fixes (missing spaces and typos, <a href="/issue/654">issue #654</a>, <a href="/issue/595">issue #595</a> and <a href="/issue/543">issue #543</a>)
 * Fixed <a href="/issue/666">issue #666</a>: Remote names with dots (e.g., john.doe) do not show properly in fetch dialog box
 * Fixed <a href="/issue/661">issue #661</a>: TProc crash when choose file and Git Sync dialog loads

<a name="Release_1.6.2.0">Release 1.6.2.0</a>
Released: 2010-12-22

== Bug Fixes ==
 * Fixed <a href="/issue/650">issue #650</a>: Settings crashes on setting user name and email address
 * Fixed <a href="/issue/648">issue #648</a>: TortoiseProc crash when cloning from a working copy repo

<a name="Release_1.6.1.0">Release 1.6.1.0</a>
Released: 2010-12-19

== Bug Fixes ==
 * Fixed <a href="/issue/645">issue #645</a>: Context menu diff crashes/errors

<a name="Release_1.6.0.0">Release 1.6.0.0</a>
Released: 2010-12-17

== Bug Fixes ==
 * Fixed <a href="/issue/639">issue #639</a>: Tortoise Git crashes on Settings | Git | Config dialogue
 * Fixed <a href="/issue/640">issue #640</a>: Sentence is grammatically poor on Windows installer

<a name="Release_1.5.9.0">Release 1.5.9.0</a>
Released: 2010-12-08

== Features ==
 * Fixed <a href="/issue/220">issue #220</a>: Enhancement, support creating bare repositories

 * Improve log dialog
   Add "checkout to branch" menu at log dialog
   Show icon for sub menu at log dialog
   Show bisect flag at log dialog

 * Support multi-parent diff
   Enhance GNU Diff for merge commit by choose Parent.
   Add gnu diff combine option for merge commit
   Support compare 1st parent, 2nd parent at merge commit
   Show diff with multi parent at merge commit at log dialog
   Support multi parent compare with udiff and view diff
   Show merged file group at log dialog
   show combine udiff at merged files
   Add Three way diff for merge commit at log dialog

 * Compare multi parent at reflog log dialog

 * Add "Show log" in reflog context menu

 * Add --init in Submodule update from sync dialog

== Bug Fixes ==
 * Fixed <a href="/issue/270">issue #270</a>: Clone fails: bash: X.X.X.X: command not found
 * Fixed <a href="/issue/568">issue #568</a>: push using ssh private key with a password fails the first time
 * Fixed <a href="/issue/577">issue #577</a>: Commit warning "svn:externals"
 * Fixed <a href="/issue/586">issue #586</a>: Ampersand in log description underlines next character
 * Fixed <a href="/issue/323">issue #323</a>: GIT SVN dcommit --rmdir
 * Fix stash list can't show stash at reflog default
 * Fixed <a href="/issue/603">issue #603</a>: Columns not sorted in Compare revisions dialog box
 * Fixed <a href="/issue/598">issue #598</a>: Background color of log messages changed when mouse is over tag or branch indicator
 * Fixed <a href="/issue/616">issue #616</a>: Problem encoding Cyrillic characters in Author name
 * Fixed <a href="/issue/612">issue #612</a>: "Copy all information to clipboard" copies incorrect header
 * Fixed <a href="/issue/602">issue #602</a>: Rebase shows overlapping branches
 * Fixed <a href="/issue/542">issue #542</a>: Difference between line endings in compared files (external Diff Viewer)
 * Fixed <a href="/issue/468">issue #468</a>: Non-ASCII characters in user info aren't stored correctly
 * Fixed <a href="/issue/593">issue #593</a>: Fetch and push fails with TortoiseGit but succeeds with git bash

<a name="Release_1.5.8.0">Release 1.5.8.0</a>
Released: 2010-10-02

== Bug Fixes ==
 * Fixed <a href="/issue/571">issue #571</a>: missing annotated  tags display in log dialog
 * Fix don't show tag info at log dialog
 * Fixed <a href="/issue/570">issue #570</a>: Check for modifications dialog's controls not functioning correctly
 * Fixed <a href="/issue/573">issue #573</a>: Upgrading to 1.5.7.0 removes gitdll.dll
 * Fixed <a href="/issue/572">issue #572</a>: Commit dialog becomes unresponsive after hitting F5 to refresh

<a name="Release_1.5.7.0">Release 1.5.7.0</a>
Released: 2010-09-28

== Bug Fixes ==
 * Fix progressdlg show mass data when clone

<a name="Release_1.5.6.0">Release 1.5.6.0</a>
Released: 2010-09-24

== Bug Fixes ==
 * Fixed <a href="/issue/556">issue #556</a>: "bad revision" in log of "Submodule Add"
 * Fix "amend last commit" can't get last commit message.
 * Fix icons were just white when using 125% display magnification mode
 * Fix ref is not updated when refresh at log dialog
 * Fix Git property page information show wrong

<a name="Release_1.5.5.0">Release 1.5.5.0</a>
Released: 2010-09-22

== Features ==
 * Support msysgit 1.7.2.3

 * Upgrade gitdll.dll to 1.7.2.3
   Use gitdll to fetch reflog, branch, tag information.

 * Enhance Column manage for commit list.
   At log, rebase, sync dialog, reflog, blame. User can customize column position. User can show\hide column. Add commiter date, commiter name, email.

 * BrowseRefs: Put focus on a tree node after a branch is deleted.s

 * Add Note Support

 * Icon Overlay (TGitCache)
   Add RW Lock to reduce tgitcache crash.

 * SyncDlg: Log output is now in Courier font. This way characters are aligned like in the console.

== Bug Fixes ==
 * Fixed <a href="/issue/535">issue #535</a>: Wandering label when drag top of progress dialog
 * Fixed <a href="/issue/478">issue #478</a>: Fix Blame timing because it shows wrong date and 00000.... commit hash
 * Fix outlook can't get correct attachment send by TortoiseGit-1.5.3.0-32bit.msi
 * Fixed <a href="/issue/537">issue #537</a>: Glitch at "merge to the right" in graph display at log dialog
 * Fixed <a href="/issue/546">issue #546</a>: Exporting without specifying a zip filename sends all zip output to stdout and confuses the GUI
 * Fixed <a href="/issue/545">issue #545</a>: Switch/Checkout Dialog: Checkbox "Create New Branch" not responding
 * Fixed <a href="/issue/160">issue #160</a>: Browse Refs dialog: Parameter is incorrect.
 * Fix Sync dialog crash when using msysgit 1.7.2.3
 * Fixed <a href="/issue/191">issue #191</a>: No Progress Indicator making one believe a hang or some failure.

<a name="Release_1.5.3.0">Release 1.5.3.0</a>
Released: 2010-08-22

== Features ==
 * Add pre and post push hook support
 * Don't show remote branch name if remote branch is track branch at syncdlg
 * RebaseDlg: working at no branch, not upstream branch.

== Bug Fixes ==
 * Fixed <a href="/issue/475">issue #475</a>: Fix 'Save revision to' for 'Working copy'
 * Fixed <a href="/issue/486">issue #486</a>: Resolving conflicts can result in nothing to commit
 * Fixed <a href="/issue/493">issue #493</a>: Add username option at clone dialog
 * Fixed <a href="/issue/482">issue #482</a>: View message details of a tag
 * Fixed <a href="/issue/495">issue #495</a>: Push dialog does not select remote branch correctly
 * Fixed <a href="/issue/505">issue #505</a>: Delete (keep local) results in commit error
 * Fixed <a href="/issue/503">issue #503</a>: remote branch drop down doesn't allow enough characters at sync dialog
 * Fixed <a href="/issue/506">issue #506</a>: TortoiseGit-1.5.2.0-32bit.msi does not recognize msysGit-fullinstall-1.7.1-preview20100612.exe
 * Fixed <a href="/issue/492">issue #492</a>: Remember AutoLoad Putty Key status in Sync dialog
 * Fixed <a href="/issue/492">issue #492</a>: Remember AutoLoad Putty Key status in Pull dialog
 * Fixed <a href="/issue/492">issue #492</a>: Fix AutoLoad Putty Key operation in push dialog
 * Fixed <a href="/issue/513">issue #513</a>: Remember "Git Command Progress" window size
 * Fixed <a href="/issue/519">issue #519</a>: Icon overlay not working for exclude files
 * Fixed <a href="/issue/507">issue #507</a>: Help Spell error and push wrongly link to sync
 * Fixed <a href="/issue/507">issue #507</a>: context menu spell error and description error
 * Fixed <a href="/issue/517">issue #517</a>: Context menu for folder does not contain "Add to ignore list"
 * Fixed <a href="/issue/380">issue #380</a>: TortoiseMerge "Created unified diff file" doesn't work
 * Fixed <a href="/issue/528">issue #528</a>: Whole repository context menu actions not visible in Win 7 folders in libraries
 * Fixed <a href="/issue/412">issue #412</a>: Create new branch default check at switch\checkout dialog.
 * Fixed <a href="/issue/477">issue #477</a>: Commit from "GitShowLog" doesn`t show not versioned files
 * Fixed <a href="/issue/527">issue #527</a>: Fix the dialog after commit for us to be able to continue committing
 * Fixed <a href="/issue/472">issue #472</a>: TortoiseGit allows to commit without setting up a username + email
 * Fixed <a href="/issue/464">issue #464</a>: Push and pull missing from context menu
 * Fixed <a href="/issue/470">issue #470</a>: auto stash apply/pop for SVN dcommit

<a name="Release_1.5.2.0">Release 1.5.2.0</a>
Released: 2010-06-10

== Bug Fixes ==
 * Fixed <a href="/issue/454">issue #454</a>: Fix "check Now" can't work at setting dialog
 * Fixed <a href="/issue/457">issue #457</a>: Git copy versioned item(s) here adds all unversioned files
 * put pull, push and fetch to external manual.
 * Fixed <a href="/issue/460">issue #460</a>: "Show changes as unified diff" compares files in reverse order.
 * Fix history combobox show twice item

<a name="Release_1.5.1.0">Release 1.5.1.0</a>
Released: 2010-06-03 (internal release)

== Features ==
 * TortoiseMerge
   Tip show "new file" "delete file" "rename file" status at tortoisemerge

 * TortoiseGitBlame
   Add encode support for blame

 * Sync Dialog
   Fixed <a href="/issue/392">issue #392</a>: refresh branch info when press "F5"
   Improve user experience when input remote branch and url

 * Log Dialog
   <a href="/issue/371">issue #371</a>: Log offer per-file "revert ..." of working dir changes
   Add AntiAlias at show log
   Fix version tree graphic line break at Win 7
   Fixed <a href="/issue/427">issue #427</a>: Implement enter operation to open a file in the 'Show Log' window

 * Fixed <a href="/issue/355">issue #355</a>: Implement Show log like history in the Changed Files window after a git pull operation

 * ProgressDlg Post Cmd support menubutton

 * Update the translations.txt for translators

 * Fixed <a href="/issue/421">issue #421</a>: Implement ctrl+a standard 'Select all' facility

 * Change FormatPatch dialog default output directory is project root

 * Fixed <a href="/issue/431">issue #431</a>: Implement commit button in the git add dialog

== Bug Fixes ==
 * Fixed <a href="/issue/401">issue #401</a>: TGitCache.exe keeps open pack-xxx.idx on git repo
 * Fix issue review patch fail when there are new FilePatchesDlg.cpp
 * Fix all file show "+" icon after run git gc
 * Fixed <a href="/issue/449">issue #449</a>: Files not in the Commit dialog are committed if in index
 * Fixed <a href="/issue/450">issue #450</a>: Log Messages file list wrong when choose children dir firstly.
 * Fixed <a href="/issue/387">issue #387</a>: "Automatically check for newer versions every week" remains disabled
 * Show correct file when Add new file at tortoisemerge
 * Fixed <a href="/issue/381">issue #381</a>: About screen of TortoiseMerge shows invalid build information
 * Fixed <a href="/issue/382">issue #382</a>: TGitBlame encoding problem
 * Fixed <a href="/issue/400">issue #400</a>: CrLf options are missing in the help file
 * Fixed <a href="/issue/397">issue #397</a>: Settings/Set Extended menu/Help button doesn't work
 * Fixed <a href="/issue/396">issue #396</a>: Fix 'Copy paths to clipboard' option
 * Fixed <a href="/issue/398">issue #398</a>: Settings/Revision Graph/Help button doesn't work
 * Fixed <a href="/issue/392">issue #392</a>: Implement refresh button in sync dialog
 * Fixed <a href="/issue/395">issue #395</a>: [BUG] Infomation error when "Switch the comparison"
 * Fixed <a href="/issue/385">issue #385</a>: Bug In properties->Git dialog
 * Clear HOME at gitdll dll after load git config
 * Fixed <a href="/issue/403">issue #403</a>: Diff Show changes, but commit not
 * Fixed <a href="/issue/404">issue #404</a>: GetOpenFileName does not work in Cygwin
 * Fixed <a href="/issue/411">issue #411</a>: Fix the refresh button operation in 'Check for modifications' when only file time change.
 * Fixed <a href="/issue/406">issue #406</a>: Putty key can't save when clone
 * Fixed <a href="/issue/419">issue #419</a>: wrong error message for empty commit
 * Fixed <a href="/issue/418">issue #418</a>: Misleading button title in Sync dialog leads to loss of uncommitted changes
 * Fixed <a href="/issue/402">issue #402</a>: Revert Renamed File Fail
 * Fixed <a href="/issue/429">issue #429</a>: When applying patches, tortoise doesn't remember last file location
 * Fixed <a href="/issue/428">issue #428</a>: Blame of an old version
 * Fixed <a href="/issue/410">issue #410</a>: Change some menu item name to make it clear
 * Fixed <a href="/issue/440">issue #440</a>: Don't enable 'Apply' button until the data are ok in Settings/Git/Remote
 * Fixed <a href="/issue/439">issue #439</a>: Fix the Help action in the TortoiseGitBlame window
 * Fixed <a href="/issue/438">issue #438</a>: Slow load Switch/Checkout dialog
 * Fixed <a href="/issue/221">issue #221</a>: After resolving all merge conflicts, it's not obvious the project needs a commit
 * Fixed <a href="/issue/437">issue #437</a>: Blame should be available when there are local changes
 * Fixed <a href="/issue/444">issue #444</a>: Crash rebase  dialog when press ESC and move split bar
 * Fixed <a href="/issue/445">issue #445</a>: Resolve conflict does not delete temporary files
 * Fixed <a href="/issue/446">issue #446</a>: TortoiseMerge crash when "Edit Conflicts"
 * Fixed <a href="/issue/405">issue #405</a>: Merge commit message when there is a conflict

<a name="Release_1.4.4.0">Release 1.4.4.0</a>
Released: 2010-04-13

== Features ==
 * #379:  Create Branch for remote branch do not set branch name

== Bug Fixes ==
 * Fix log show mass when encode is cp1251
 * Fixed <a href="/issue/357">issue #357</a>: Fix line endings merging issue

<a name="Release_1.4.3.0">Release 1.4.3.0</a>
Released: 2010-04-10

== Bug Fixes ==
 * Fix explore crash when there are ignore patten at .git/info/exclude
 * Fixed <a href="/issue/367">issue #367</a>: Last line of .gitignore ignored
 * Fixed <a href="/issue/368">issue #368</a>: TortoiseGitBlame should not spell check by default
 * Fixed <a href="/issue/369">issue #369</a>: TortoiseGitBlame should expand its menu items by default
 * Fixed issue: Compare submodule dialog show wrong subject at log dialog
 * Fixed <a href="/issue/365">issue #365</a>: Log - enter closes dialog
 * Fixed <a href="/issue/358">issue #358</a>: Renamed files are not properly committed, after refreshing the commit dialog
 * Fixed issue .git\* locked when remove git repository

<a name="Release_1.4.2.0">Release 1.4.2.0</a>
Released: 2010-04-06

== Features ==
 * Log dialog find support search tag and branch
 * Fixed <a href="/issue/354">issue #354</a>: impliment revert of this commit at log dialog
 * Add Merge command at log context menu
 * Fixed <a href="/issue/350">issue #350</a>: Implement "Copy and rename" from context menu

== Bug Fixes ==
 * Fixed <a href="/issue/346">issue #346</a>: can't remove remote repos
 * Fixed <a href="/issue/280">issue #280</a>: "Use recycle bin when reverting" does not work
 * Fix ignore over lay show wrong when second level directory exist .gitignore file
 * Fixed <a href="/issue/240">issue #240</a>: Setting "Do not show the context menu for following paths:" not working properly

<a name="Release_1.4.1.0">Release 1.4.1.0</a>
Released: 2010-04-04

== Features ==
 * Fixed <a href="/issue/349">issue #349</a>: Offer "DCommit" instead of "Push" when working as SVN client

== Bug Fixes ==
 * Fixed clone fail if msysgit version below 1.7.0.2
 * Fixed Folder keep "X" delete icon after commit
 * Fixed some small repository can't reflect "add"
 * Fixed show "?" at second level directory when icon overlay using "shell"
 * Fixed <a href="/issue/351">issue #351</a>: "Search log messages..." in Log context menu does nothing
 * Fixed <a href="/issue/226">issue #226</a>: tortoisegit is searching for .git share on network drives
 * Fixed number of files selected is wrong at commit dialog
 * Fixed <a href="/issue/353">issue #353</a>: Fix Help button in the git sync dialog

<a name="Release_1.4.0.0">Release 1.4.0.0</a>
Released: 2010-03-31

== Features ==
 * Improve Icon Overlay
   Rewrite icon overlay implement. TGitCache will call gitdll.dll to get HEAD tree. And direct read index files. Read .gitignore file and call gitdll.dll to judge ignore files.
   Can't watch untracked directroy to reduce TGitCache loading.

 * Git Clone
   Add --progress at clone dialog

 * Log Dialog
   Add antiAlias when draw cycle at log dialog

 * Add minimize and maximize button at rebase and sync dialog

== Bug Fixes ==
 * Fixed <a href="/issue/344">issue #344</a>: Force is the default in the sync dialog
 * Fixed <a href="/issue/343">issue #343</a>: Wrong behaviour of Show Unversioned Files checkbox at Commit dialog
 * Fixed <a href="/issue/281">issue #281</a>: show wrong character after finish commit
 * Fix commitdlg can close after commit and progress scroll too much
 * Fixed <a href="/issue/299">issue #299</a>: Do nothing "Check For Updates..."  at about dialog
 * Fixed <a href="/issue/333">issue #333</a>: Can't abort CherryPick
 * Fixed <a href="/issue/336">issue #336</a>: Text of context menu slightly wrong; replace svn with git
 * Fixed <a href="/issue/340">issue #340</a>: OpenSSH password dialog focus
 * Fixed <a href="/issue/312">issue #312</a>: The number of changed files in 'Show log' window
 * Fixed <a href="/issue/325">issue #325</a>: Would be nice to see current directory in the commit dialog
 * Fixed <a href="/issue/329">issue #329</a>: path wrong at "Save revision to...
 * Fixed <a href="/issue/327">issue #327</a>: Crash in the commit dialog after you changed a file for Linux formatted
 * Fixed <a href="/issue/321">issue #321</a>: Wrong Company/Product name in Metadata
 * Fixed <a href="/issue/309">issue #309</a>: Mispelled words in the Git Synchronization dialog
 * Fixed <a href="/issue/304">issue #304</a>: Adding a file in the commit dialog resets selection
 * Fixed <a href="/issue/305">issue #305</a>: Filtering showlog make crash

<a name="Release_1.3.6.0">Release 1.3.6.0</a>
Released: 2010-02-05

== Bug Fixes ==
 * Fixed log crash when no body message at commit
 * Fixed <a href="/issue/298">issue #298</a>: State of "View Patch/Hide Patch" link (commit window) is wrong in some ways
 * Fixed <a href="/issue/301">issue #301</a>: Show Log crashes with empty repo

<a name="Release_1.3.5.0">Release 1.3.5.0</a>
Released: 2010-02-03

== Features ==
 * Support Annotated tags
   Implemented <a href="/issue/274">issue #274</a>: Enhancement: Annotated tags

 * shallow clones support --depth at clone dialog
   Fixed <a href="/issue/290">issue #290</a>: Shallow clones support --depth at clone dialog

 * Improve Diff Dialog
   Change commit at diff dialog
   Diff commit context menu show in git repository

 * Log Dialog
   Direct Launch external diff when open dialog at file
   Log can refresh when Click Rev button.

 * Context menu
   Use setting dialog to control which menuitem is external menu.

 * Sync Dialog
   Add remote update at sync dialog

== Bug Fixes ==
 * Fixed <a href="/issue/294">issue #294</a>: commit template not supported and support msysgit unix path
 * Fixed <a href="/issue/282">issue #282</a>: Fom/To/Messages/Authors/Paths filters are eventually disabled
 * Fixed <a href="/issue/292">issue #292</a>: Very large dialog when merging
 * Fixed <a href="/issue/291">issue #291</a>: Blame makes empty "UserImages.bmp" file
 * Fix crash when copy several log message to clipboards
 * Fixed <a href="/issue/284">issue #284</a>: Show Log crashes when switching branches wait for log thread exit
 * Fixed <a href="/issue/285">issue #285</a>: Cherry picking no longer works
 * Fix fetch command can't sync remote branch at sync dialog

<a name="Release_1.3.2.0">Release 1.3.2.0</a>
Released: 2010-01-21

== Bug Fixes ==
 * Fixed <a href="/issue/276">issue #276</a>: Crash on seeing diff with previous version
 * Fixed <a href="/issue/275">issue #275</a>: Load gitweb for 'Browse' button
 * Fixed <a href="/issue/265">issue #265</a>: Log dialog: Date picker throws multiple error messages when date is before 1.1.1970
 * Fix RefLogDlg crash

<a name="Release_1.3.1.0">Release 1.3.1.0</a>
Released: 2010-01-18

== Features ==
 * Improve Log Dialog. Speed up log fetch speed.
   Build Git source as a DLL. LogDialog will call gitdll to fetch log instead of capture git.exe output.
   Improve refresh and all branch user experience. refresh can abort runing fetch log commit.

 * Improve icon-overlay
   Give up igit.exe and use tgit which build from git source by VS.
   use tgit.exe statusex to get file status.
   Don't list all untracked files.

 * Improve commit and checkout modify dialog
   Don't show file that is only time stamp change and no context change.
   Run git-update-index first when open commit and checkout out modify dialog.

== Bug Fixes ==
 * Fixed <a href="/issue/234">issue #234</a>: First log(first commit in history) was missing...
 * Fixed <a href="/issue/232">issue #232</a>: "No Commit" Option always acitve
 * Fixed issue 236: CGit::GetRemoteList uses bad regular expression
 * Fix blame show wrong when first char is '^'
 * Workaround show "fail" even git run success at sometime by remove "fail" message.
 * Fixed <a href="/issue/265">issue #265</a>: Log dialog: Date picker throws multiple error messages when date is before 1.1.1970

<a name="Release_1.2.1.0">Release 1.2.1.0</a>
Released: 2009-11-12

== Features ==
 * Add color success and fail at ProgressDlg

 * Log Dialog
   Show work copy to log dialog. User can commit change at log dialog.
   Easy to compare with working copy difference

 * Allow Alt+O in commit dialog for OK

 * Sync Dialog remote URL support save history

 * Add remote branch and current branch at proptery page

 * Add no-commit option at merge dialog

 * Enable IBugTraqProvide CheckCommit and OnCommitFinished

== Bug Fixes ==
 * Fixed <a href="/issue/219">issue #219</a>: Blame Error when git repository is at root directory and path use "/"
 * Fixed <a href="/issue/214">issue #214</a>: installer inserts unused or faulty registry key
 * Fixed <a href="/issue/179">issue #179</a>: Log dialog lacks information about changed files
 * Fixed <a href="/issue/209">issue #209</a>: High CPU usage in tortoiseproc.exe & limit line number
 * Fixed <a href="/issue/212">issue #212</a>: Disable the "Select items automaticlly" option has no effect to commit files dialog
 * Fixed <a href="/issue/209">issue #209</a>: High CPU usage in tortoiseproc.exe & Append text to edit ctrl
 * Fixed <a href="/issue/203">issue #203</a>: Remote URL select box in sync dialog is not populated with remotes.
 * Fixed <a href="/issue/208">issue #208</a>: During push (from context menu), branches missing from drop down list.
 * Fixed <a href="/issue/86">issue #86</a>: Globally sets HOME affecting third-party applications (GNU Emacs)
 * Fixed <a href="/issue/188">issue #188</a>: Add Git Properties tab into Windows File Properties

<a name="Release_1.1.1.0">Release 1.1.1.0</a>
Released: 2009-10-13

== Features ==
 * Improve Rebase Dialog
   Allow lanuch new rebase dialog again after finish rebase dialog
   Disable "force rebase" checkbox during rebase.

 * Git SVN
   Append svn:ignore settings to the default git exclude file Add shell extension command to import svn ignore settings.
   Need press "Shift" key to show "import svn ignore" command.

 * Drag-drop copy\move support
   File only

 * Add paste command at shell extension
   Copy and paste file is okay. But there are problems when including directory.
   Cut and paste working

 * Update notepad2 to 4.022

 * Sync Dialog
   Ability to sync submodules in TGit sync dialog

 * Statics
   Sort commits by dates before processed by StatGraphDlg

 * Log Dialog
   Show <No branch> replace ref error message at log dialog

 * Add Check software updater support.

== Bug Fixes ==
 * Fixed <a href="/issue/185">issue #185</a>. "Can't find Super-project" when pathname include space.
 * Fixed <a href="/issue/190">issue #190</a>: Access violation in Blame and wrong path name when root dir is git repository
 * Fixed <a href="/issue/180">issue #180</a>: Create patch serial doesn't work when there is "\" at end of path
 * Fixed <a href="/issue/173">issue #173</a>: SVN Rebase does not work The correct handle below case git config svn-remote.svn.fetch myproject/trunk:refs/remotes/trunk
 * Fixed <a href="/issue/169">issue #169</a>: Force rebase checkbox is fixed
 * Fixed <a href="/issue/163">issue #163</a>: Conflict "theirs" and "mine" are reversed during a rebase
 * Fixed <a href="/issue/165">issue #165</a>: Incorect path to Notepad2
 * Fixed <a href="/issue/158">issue #158</a>: Rebase can act on the wrong branch

<a name="Release_1.0.2.0">Release 1.0.2.0</a>
Released: 2009-09-05

== Bug Fixes ==
 * Fixed <a href="/issue/155">issue #155</a>: Fix SVN Rebase sets upstream as remotes/trunk
 * Fixed <a href="/issue/157">issue #157</a>: Move progress dlg before rebase dialog SVN Rebase doesn't fast-forward

<a name="Release_1.0.1.0">Release 1.0.1.0</a>
Released: 2009-08-29

== Features ==
 * Improve Commit Dialog
   Show line and column number
   Add view/hide patch windows

 * Improve Log Dialog
   Bolad subject at log dialog

 * Setting Config Dialog
   Add core.autocrlf and core.safecrlf

 * Add more option to resolve conflict
   Add Resolve "Their" and Resolve "Mine" at conflict item.

 * Improve Merge dialog
   Add message box to allow input message when merge

 * Improve Stash
   Add Stash pop.
   Add delete stash at logview.

== Bug Fixes ==
 * Fix don't show "push" after commit
 * Fixed <a href="/issue/133">issue #133</a>: Command fails on folder with leading dash And -- to separate file and git options
 * Fixed <a href="/issue/133">issue #133</a>: (mv\rename  problem) Command fails on folder with leading dash And -- to separate file and git options
 * Fixed <a href="/issue/140">issue #140</a>: Incorrect treatment of "Cancel" action on "Apply patch serial" command
 * Fixed <a href="/issue/135">issue #135</a>: Taskbar text says "TortoiseSVN"
 * Fixed <a href="/issue/142">issue #142</a>: TortoiseGit Clone of SVN repo does not use PuTTY session for non-standard SSH port
 * Fixed <a href="/issue/138">issue #138</a>: "Format patch" in "Show log" dialog doesn't work
 * Fixed <a href="/issue/141">issue #141</a>: Bizarre ordering in commit dialog
 * Fixed <a href="/issue/137">issue #137</a>: Proxy Authentification fails
 * Fixed <a href="/issue/131">issue #131</a>: Missing SVN DCommit Command
 * Fixed <a href="/issue/139">issue #139</a>: "Format patch" with a range of revisions doesn't export the first revision in the range
 * Fix Pathwatcher thread can't stop when commitdlg exit.
 * Fixed <a href="/issue/150">issue #150</a>: When pushing, 'remote' should default to the tracked branch, or empty

<a name="Release_0.9.1.0">Release 0.9.1.0</a>
Released: 2009-07-31

== Features ==
 * Add Sync Dialog like TortoiseHg.
   Put pull, fetch, push, apply patch and email patch together. You can Know what change pull and push. Show changed file list.

 * Enhance Rebase dialog. Add force rebase checkbox. Disable start button when no item rebase. Don't show merge commit

 * Add post action button for rebase dialog. Such as send patch by email
   After rebase, you can click button to send patch email directly.

 * Add  launch rebase option at fetch dialog.

 * Improve push dialog.
   Default settings from local repositories pushing to remote repository Choose track remote and remote branch! Add short-cut at push dialog
   Add "F5" refresh remote and branch info at push dialog

 * Add Clean Untracked version type
   User can choose clean untracked file, clean ignore file, clean all.

 * Enhancement: "Git Clone" from SVN repository with additional start revision number option (-r xxx:HEAD)

 * Improve Commit dialog
   Add recent message back to context menu.
   The "Message" field of the Commit dialog should have a shortcut key (Alt-M is a good choice)
   Make "Whole project" directory checked by default when the user commits in the root of the project.
   When using "Commit" to also add files, if you forget to check the new files and press "Ok", you get the dialog "Nothing Commit" and then the whole Commit dialog closes. Keep the dialog open after this message.

 * Improve setting dialog
   Settings: Git -> Remote: When creating the first remote, the "Remote" should have "origin" as default.
   Fix setting dialog remote page tab order
   Push: When you press "Manage" under Destinations, the Settings dialog opening should have "Git -> Remote" selected by default.

== Bug Fixes ==
 * Fixed <a href="/issue/124">issue #124</a>: Incorrect Date header in patch e-mail
 * Fixed <a href="/issue/122">issue #122</a>: Garbage text in "Git Command Progress" / suspected buffer overflow Improve commit speed when many added files.
 * Fixed <a href="/issue/121">issue #121</a>: Refresh in "Check for modifications" dialog duplicates added state entries when new repository
 * Fixed <a href="/issue/71">issue #71</a>: (TortoiseProc problem)Icon Overlays don't work in root of drive When m_CurrentDir =C:\, not C:,  pathlist calulate wrong.
 * Fixed <a href="/issue/116">issue #116</a>: SVN Rebase doesn't work
 * Fixed <a href="/issue/115">issue #115</a>: Windows XP: Initial "Git Clone..." from SVN Repository doesn't work
 * Fix "ESC" = "push" when after commit and Add Alt-P  for Push
 * Fixed <a href="/issue/111">issue #111</a>: Undo Add does not work (keep added file) and enable "F5" at revert dialog
 * Fixed <a href="/issue/109">issue #109</a>: clone on bare local repository fails Clear trail slash \ or/
 * Fixed <a href="/issue/104">issue #104</a>: Doubleclicking changed submodule dir in Check For Modifications dlg, crashes TGit Fix log dialog double click submodule problem

<a name="Release_0.8.1.0">Release 0.8.1.0</a>
Released: 2009-07-03

== Features ==
 * Improve work flow and reduce click
   Commit: Made user able to push after successful commit
   Show Push Dialog after close commit dialog
   Add messagebox to ask if stash change when rebase at dirty work space
   Show GUI friendly diffstat after pull

 * Rebase: Select default upstream based on current tracked remote

 * Improve BrowseRef Dialog
   Add Option to delete multiple refs
   Added 'Switch to this Ref' option
   Fetch context menu item added.
   Add "BrowseRef" to shell extension command.
   Add ability to diff two commits
   Added option to delete remote branch
   Always set initial ref
   Show context menu icons
   Save / Restore window size

 * Add Basic Git-SVN Operation
   Add SVN DCommit Command
   Add "SVN Rebase" and "SVN DCommit" command at shell contextmenu
   Support Git svn-clone at clone dialog.

 * Help Document Updated
   Add git basic book.

 * Add Help button at many dialog

 * Improve Format Patch
   May also select 'format patch...' if selection is continuous.
   Implemented browse buttons and added browsebutton for 'since'.

 * Add TortoiseIDiff to compare picture

 * Enable Bugtraq setting dialog

 * Improve Log dialog
   Ajust label colors when selected. (Not on Vista with themes enabled)
   Add compare with working copy at log dialog
   Ref label borders

 * FileDiffDlg: Make shift right-left button work

 * Branch/Tag dlg: Update 'track' option after browse-refs

 * StatusListCtrl: Implemented delete unversioned file.

 * RebaseDlg: Update rebase lines after browserefs

== Bug Fixes ==
 * Fixed <a href="/issue/64">issue #64</a>: When the graph column is small, the graph sometimes appears through the text of the next column on Vista
 * Fix log graph tree mass when dragon scroll bar from left to right
 * Fixed <a href="/issue/104">issue #104</a>: Doubleclicking changed submodule dir in Check For Modifications dlg, crashes TGit
 * Pull: Fixed bug that when pulling from the configured remote branch, git did not update the remote tracking branches.
 * Fix stash problem when svn dcommit at dirty working space
 * Modify Create repository error message
 * Fixed <a href="/issue/102">issue #102</a>: Invalid patch e-mails generated
 * Fix submodule update Crash when repository have not submodule
 * Fixed <a href="/issue/88">issue #88</a>: Ambiguous dialog message
 * Correct format patch command -num argument
 * PushDlg: Fix: Wrong remote selected after selection via ref-browser.
 * Rebase: Skip in context-menu appeared twice. First one should be pick.
 * Fixed <a href="/issue/89">issue #89</a>: Can't locate msysgit on x64
 * Fixed <a href="/issue/95">issue #95</a>: TortoiseBlame Icon disappears when selected in the settings treeview
 * Fixed <a href="/issue/94">issue #94</a>: Commit log showing incorrect timestamps
 * Fix pull don't launch putty key file

<a name="Release_0.7.2.0">Release 0.7.2.0</a>
Released: 2009-06-05

== Features ==
 * Add bug track plug-in support
   Compatible with TortoiseSVN. The typical plug in is gurtl(google code issue plug-in).
   <a href="https://code.google.com/p/gurtle/" rel="nofollow" target="_blank">https://code.google.com/p/gurtle/</a>.

 * Support browse reference.
   Show all branch, tags information. Support add remote, del branch\tag.

 * Show merged file at log dialog.

 * Update version graphic tree.
   Update graphic tree to qgit2.3

 * Add option -p for TortoisePlink.
   -p is the same as -P. So it is compatible with OpenSSH port option

== Bug Fixes ==
 * Fixed <a href="/issue/91">issue #91</a>: clone dialog generates bad directory name based on URL, ignores overrid
 * Fixed <a href="/issue/85">issue #85</a>: Installer: warns of downgrade when running 0.6.2.0 on top of 0.6.1.0
 * Fix i18n.logOutputEncoding doesn't work at log\commit dialog.

<a name="Release_0.6.2.0">Release 0.6.2.0</a>
Released: 2009-05-06

== Feature ==
 * Improve fetch overlay speed
   rebase igit.exe to msysgit 1.6.2.2, which is major application to get git status.

 * TortoiseMerge Support git format patch file.
   TortoiseMerge can open git patch file and review diff and merge change.

 * SendMail support
   Right click .patch or .diff file, click "send mail..." to send patch out.

 * improve clone dialog
   Add auto fill module name at directory when change URL. Fix tab order.

== Bug Fixes ==
 * Fixed <a href="/issue/73">issue #73</a>: Error while reading/writing the registry key MSysGit Access is denied
 * Fixed <a href="/issue/11">issue #11</a>: Show differences as unified diff does not use selected item
 * Fixed <a href="/issue/77">issue #77</a>: "Show differencess" against a deleted file will cause TortoiseMerge to error
 * Fixed <a href="/issue/74">issue #74</a>: Add multiple files from commit dialog only adds first two in multi-selection
 * Fixed <a href="/issue/75">issue #75</a>: About box does not show version information
 * Wrong error message when fail launch pageant
 * Setup will launch old version msi to remove old version. Directly update to new version without remove old version
 * Double click conflict item to launch diffview, not conflict edit.
 * Fixed <a href="/issue/66">issue #66</a>: Some log lines lose color or disappear after you click on them
 * Fixed <a href="/issue/81">issue #81</a>: Cannot have x86 and x64 versions installed at the same time.

<a name="Release_0.5.1.0">Release 0.5.1.0</a>
Released: 2009-04-03

== Features ==
 * Submodule Support.
   Support submodule add, sync and update.
   "Submodule Sync" is located in explore context external menu, which need press "shift" key when right click.

 * Improve show log speed at big repository, such as git.git

 * OpenSSH can prompt password dialog

 * Clone, pull push support both OpenSSH and Plink.
   Support both key and password mode.
   Show progress when clone at git and SSH protocol.

 * Stash Save\Apply support

 * Reflog support. Need press "shift" to show reflog menu item at context menu.

 * Add save special version to another file at file list, such as log dailog.

 * Add external diff merge and undiff setting at settings page

 * Add Diff with workcopy at file list

 * Add MessageBox Tell user Revert Finished

 * Add Notepad2 to setup script to view text file

 * Add view in notepad2 at file list

 * Add Copy File list to clipboard

 * Choose Default SSH client when install TortoiseGit

 * Add user config and remote manage at setting dialog

 * Pull and push can autoload putty private key.

== Bug Fixes ==
 * Fixed <a href="/issue/52">issue #52</a>: TortoiseMerge crashes on x64
 * Fixed <a href="/issue/55">issue #55</a>: "resolved" function doesn't delete temporary files.
 * Fixed <a href="/issue/57">issue #57</a>: Data duplication when Clicking Check repository in Check for modification dialog
 * Fix GetString error when edit at HistoryCombo
 * win2k context menu fix (had an issue when shift key was pressed)
 * Fix crash in logviewer on invalid time strings
 * Fixed <a href="/issue/61">issue #61</a>: Add/Commit of files with umlauts in filename not working

<a name="Release_0.4.2.0">Release 0.4.2.0</a>
Released: 2009-03-07

== Features ==
 * Full Overlay Icon Support.
   Show "Conflict, ignore file, untracked file, modified, Add, staged" icon according to file status.

 * Rebase Support.
   Support "Pick" "Sqaush" "Edit" and "Skip" commit when rebase branch.
   Support abort.

 * Combine Multi-commits to one commit.
   Combine continous commits to one commit. The limition is the only single line(no merge point) above combined commit.

 * Cherry Pick multi commits.
   User can use multi commits at log dialog and then choose cherry pick these. Cherry Pick dialog guide you finish whole work.
   Support "Pick" "Squash" "Edit" and "Skip" commits.

 * First x64 version.

 * Support version "browse" at switch, export, new branch/tag and merge dialogs.

 * Add context menu item "Revert" at Commit dialog File List.

 * Show bold font for HEAD at log dialog.

 * Add "Whole Project" checkbox at commit dialog

 * First Version Help Document.

== Bug Fixes ==
 * Fix Shell menu disappear because ATL library have not installed.
 * Fix Commit Dialog and Log Dialog default column is wrong
 * Fix some dialog can't show after resize and close and open again
 * Fix ProgressDlg Sometime thread is dead blocked.
 * Fixed x64 build of TortoiseProc crashed due to received unexpected messages
 * Fix tag to head when *force* check box checked
 * Add Git document to help
 * Fixed <a href="/issue/36">issue #36</a>: Push not working if no remote branch is specified
 * Default UnCheck untrack file at commit dialog
 * Fixed <a href="/issue/40">issue #40</a>: Commit from subfolder shows unversioned files in parent
 * Fix diff problem when filenames have embedded spaces
 * Fixed <a href="/issue/24">issue #24</a> and <a href="/issue/45">issue #45</a>: Commit results not in window with scroll bars
 * Fix for win2k context menu icons
 * Fixed <a href="/issue/46">issue #46</a>: The about window title still displays TortoiseSVN
 * Fixed <a href="/issue/37">issue #37</a>: When the file name contains Chinese char, Diff doesn't work.
 * Fixed <a href="/issue/28">issue #28</a>: "Add" status icon overlay is not correct.

<a name="Release_0.3.3.0">Release 0.3.3.0</a>
Released: 2009-02-08

== Features ==
 * Icon Overlay
   Show different icon overlay at git repository. Support File and directory icon overlay.

 * Show version tree in log dialog.
   Show version graphic tree at log list dialog. Use QGit style.

 * Enable log cache to improve log show speed.

 * VS2008 Style Blame application.
   Show different back color according to line age. Show blame file log list to know which line is newer.

 * Enable conflict handle
   Show conflict status at any file list, such as commit dialog. User just need right click and choose resolve conflict, tortoisemerge will be launch.

 * Related time show support.

 * Setting dialog support.

 * Enable TortoisePlink.
   Passwork dialog can prompt when use TortoisePlink as ssh client.

 * Git Reset support.
   User can right click log list at log dialog. Choose reset to reset current branch to chosen commit.

 * Current handle renamed file at file list.

== Bug Fixes ==
 * Disable file overlay icon at vista system to avoid explore crash
 * File overlay default is enable at XP system. User can disable it by setting dialog.
 * Fixed <a href="/issue/20">issue #20</a>: Add To Ignore from Commit dialog not working
 * Fixed <a href="/issue/31">issue #31</a>: Init Repository, Commit dialog can not show added file
 * Fixed <a href="/issue/30">issue #30</a>: Clone does not support UNC path to repository
 * Fix when setting ssh client is null. GIT_SSH environment variable is not clear
 * Fixed <a href="/issue/29">issue #29</a>: F5 should refresh TGit log
 * Fix log filter don't filter commit hash
 * Fixed <a href="/issue/25">issue #25</a>: Log refresh does not pick up new tags on top line, or move 'master' up
 * Fixed <a href="/issue/27">issue #27</a>: Deleted files not committed
 * Fixed <a href="/issue/22">issue #22</a>: Error deleting file from context menu if filename contains spaces
 * Fixed <a href="/issue/6">issue #6</a>: Add does not work.
 * Fixed <a href="/issue/8">issue #8</a>: Clone of git via HTTP Creates repo in wrong location
 * Fixed <a href="/issue/9">issue #9</a>: Error commit file with chinese filename.
 * Fixed <a href="/issue/10">issue #10</a>: Switch and Create Branch drop-downs only display 25 items
 * Fixed <a href="/issue/13">issue #13</a>: Create branch fail if branch name is invalidate
 * Fixed <a href="/issue/14">issue #14</a>: Commit dialog don't report error when no message input
 * Fixed <a href="/issue/16">issue #16</a>: Commit dialog, F5 don't work.
 * Fix "explore to" in context menu in commit dialog.
 * Fix redraw all when loading thread finish load log.

<a name="Release_0.2.0.0">Release 0.2.0.0</a>
Released: 2009-01-04

== Features ==
 * Add TortoiseMerge as default compare tools
 * Pull, Fetch, Push
 * Create Branch\Tag
 * Switch branch\Chechout
 * Compare with previous version
 * Clone(only support local repository, see known issue for detail)
 * Log Dialog support filter
 * Check for modifications
 * Revert local change
 * Create Patch Serial
 * Apply Patch Serial
 * Add file to repository(see know issue)
 * Export to zip file

== Bug Fixes ==
 * A2W cause stack overwrite bug when git output is long.

== Known Issues ==
 * ProgressDlg will wait for ever when clone remote repository(ssh, http,git).
 * push fetch and pull don't support password mode. Just support public key problem.
 * Just fetch first 100 log item.
 * If install TortoiseGit before MsysGit, you need modify register
	    HKEY_LOCAL_MACHINE\Software\TortoiseGit\\MsysGit\
	   Let it point to correct msysgit install path.
 * Add File, please commit and show unversion file, the choose add file, then right clict, Choose Add file
 * To new initial repository, You will not see add file again in commit dialog box if give up commit when choose add


<a name="Release_0.1.0.0">Release 0.1.0.0</a>
Released: 2008-12-12

== Features ==
 * Context menu(subset of TortoiseSVN)
 * Icon Overlay(version controled\unversion controled at directory)
 * Unified DIFF
 * Use third part diff tools (such kdiff3)
 * Commit change
 * Show Log
 * Create Repository
</pre>
			</div>
		</div>
		<div id="space"></div>
		<div id="container_footer">
			<div class="wrap_content">
<?php printFooter(); ?>
			</div>
		</div>
	</div>
	</body>
</html>