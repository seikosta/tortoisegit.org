<?php
require(__DIR__.'/../inc/head.php');
printHead('Download');
?>
	<body>
	<div id=wrapper>
		<div id="container_headline">
			<?php printHeadLine(2); ?>
		</div>

		<div class="container_seperator">
			<div class="wrap_content breadcrumbs">
				<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">TortoiseGit.org</span></a></span> &raquo; Download
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h1>Download</h1>
<p><strong>The current stable version is: 1.8.15.0</strong></p>
<p>For detailed info on what's new, read the <a href="/docs/releasenotes/#Release_1.8.15.0">release notes</a>.</p>
<p><a href="/support/faq/#prerequisites">FAQ: System prerequisites and installation</a></p>

<p><strong><a href="/donate/" rel="nofollow">Donate</a></strong></p>
<p>Please make sure that you choose the right installer for your PC, otherwise the setup will fail.</p>
<table class="downloadtable">
<tbody>
<tr>
<td><strong>for 32-bit OS</strong></td>
<td><strong>for 64-bit OS</strong></td>
</tr>
<tr>
<td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-1.8.15.0-32bit.msi" rel="nofollow" class="dl">Download TortoiseGit 1.8.15.0 - 32-bit</a> (~16 MB)</td>
<td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-1.8.15.0-64bit.msi" rel="nofollow" class="dl">Download TortoiseGit 1.8.15.0 - 64-bit</a> (~19 MB)</td>
</tr>
</tbody>
</table>
<p></p>
<p>Before reporting an issue, please check that your problem isn't fixed in our latest <a href="//download.tortoisegit.org/tgit/previews/">preview release</a>. Also see <a href="/support/">What to do if a crash happened?</a></p>
			</div>
		</div>

		<div class="container_white">
			<div class="wrap_content contentpage">
<h2 id="Language_Packs"><svg width=".9em" height=".9em" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path fill="currentColor" d="M320 256q0 72-64 110v1266q0 13-9.5 22.5t-22.5 9.5h-64q-13 0-22.5-9.5t-9.5-22.5v-1266q-64-38-64-110 0-53 37.5-90.5t90.5-37.5 90.5 37.5 37.5 90.5zm1472 64v763q0 25-12.5 38.5t-39.5 27.5q-215 116-369 116-61 0-123.5-22t-108.5-48-115.5-48-142.5-22q-192 0-464 146-17 9-33 9-26 0-45-19t-19-45v-742q0-32 31-55 21-14 79-43 236-120 421-120 107 0 200 29t219 88q38 19 88 19 54 0 117.5-21t110-47 88-47 54.5-21q26 0 45 19t19 45z"/></svg> Language Packs</h2>
<p>The language packs contain no standalone localized version of TortoiseGit, you need TortoiseGit from above. Each language pack has a download size of 2-3.5 MB. </p>
<p></p>
<table class="downloadtable"><tbody><tr><td><strong>Language</strong></td><td><strong>Code</strong></td><td><strong>Completeness</strong></td><td><strong>32 Bit</strong></td><td><strong>64 Bit</strong></td></tr>
<tr><td>Bulgarian, Bulgaria    </td><td>bg_BG   </td><td class="right"> 58%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-bg_BG.msi"    rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-bg_BG.msi"    rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Catalan                </td><td>ca      </td><td class="right"> 89%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-ca.msi"       rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-ca.msi"       rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Chinese, simplified    </td><td>zh_CN   </td><td class="right">100%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-zh_CN.msi"    rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-zh_CN.msi"    rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Chinese, traditional   </td><td>zh_TW   </td><td class="right">100%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-zh_TW.msi"    rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-zh_TW.msi"    rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Czech                  </td><td>cs      </td><td class="right">100%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-cs.msi"       rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-cs.msi"       rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Dutch, Netherlands     </td><td>nl_NL   </td><td class="right"> 86%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-nl_NL.msi"    rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-nl_NL.msi"    rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>French                 </td><td>fr      </td><td class="right"> 99%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-fr.msi"       rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-fr.msi"       rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>German                 </td><td>de      </td><td class="right">100%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-de.msi"       rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-de.msi"       rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Hungarian              </td><td>hu      </td><td class="right"> 58%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-hu.msi"       rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-hu.msi"       rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Indonesian             </td><td>id      </td><td class="right"> 53%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-id.msi"       rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-id.msi"       rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Italian, Italy         </td><td>it_IT   </td><td class="right"> 66%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-it_IT.msi"    rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-it_IT.msi"    rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Japanese               </td><td>ja      </td><td class="right">100%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-ja.msi"       rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-ja.msi"       rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Korean, Korea          </td><td>ko_KR   </td><td class="right"> 94%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-ko_KR.msi"    rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-ko_KR.msi"    rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Polish, Poland         </td><td>pl_PL   </td><td class="right"> 81%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-pl_PL.msi"    rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-pl_PL.msi"    rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Portuguese, Brazil     </td><td>pt_BR   </td><td class="right"> 99%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-pt_BR.msi"    rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-pt_BR.msi"    rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Portuguese, Portugal   </td><td>pt_PT   </td><td class="right"> 58%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-pt_PT.msi"    rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-pt_PT.msi"    rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Romanian               </td><td>ro      </td><td class="right"> 93%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-ro.msi"       rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-ro.msi"       rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Russian                </td><td>ru      </td><td class="right">100%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-ru.msi"       rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-ru.msi"       rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Serbian latin          </td><td>sr@latin</td><td class="right"> 58%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-sr-latin.msi" rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-sr-latin.msi" rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Slovak                 </td><td>sk      </td><td class="right"> 62%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-sk.msi"       rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-sk.msi"       rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Spanish, Spain         </td><td>es_ES   </td><td class="right">100%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-es_ES.msi"    rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-es_ES.msi"    rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Swedish                </td><td>sv      </td><td class="right"> 95%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-sv.msi"       rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-sv.msi"       rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Turkish                </td><td>tr      </td><td class="right"> 68%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-tr.msi"       rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-tr.msi"       rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Ukrainian              </td><td>uk      </td><td class="right"> 53%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-uk.msi"       rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-uk.msi"       rel="nofollow" class="dl">Setup</a></td></tr>
<tr><td>Vietnamese             </td><td>vi      </td><td class="right"> 60%</td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-32bit-vi.msi"       rel="nofollow" class="dl">Setup</a></td><td><a href="//download.tortoisegit.org/tgit/1.8.15.0/TortoiseGit-LanguagePack-1.8.15.0-64bit-vi.msi"       rel="nofollow" class="dl">Setup</a></td></tr>
</tbody></table>
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
	<h2 id="Other_resources">Other resources</h2>
	<p>Debug symbols, language dlls and other resources can be found on our dedicated download server: <a href="//download.tortoisegit.org/tgit/1.8.15.0/">download.tortoisegit.org/tgit/1.8.15.0/</a>.</p>

	<h3 id="Preview_releases">Preview releases</h3>
	<p>The <a href="//download.tortoisegit.org/tgit/previews/">preview releases</a> are built from the latest code that represents the cutting edge of the TortoiseGit development. This version is used by the TortoiseGit developers for their daily work (in the past these also were of good quality). Preview releases are provided on an irregular basis, however, w/o translations.</p>

	<h3 id="Older_releases">Older releases</h3>
	<p>Older releases are available <a href="//download.tortoisegit.org/tgit/">here</a>.</p>

			</div>
		</div>
		<div id="space"></div>
		<div id="container_footer">
			<div class="wrap_content">
<?php printFooter(); ?>
			</div>
		</div>
	</div>
	</body>
</html>