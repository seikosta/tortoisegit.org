<?php
header("HTTP/1.0 403 Forbidden");
require(__DIR__.'/inc/head.php');
printHead('', true);
?>
	<body>
	<div id=wrapper>
		<div id="container_headline">
			<?php printHeadLine(); ?>
		</div>

		<div class="container_seperator">
			<div class="wrap_content breadcrumbs">
				&nbsp;
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h1>Error 403: Forbidden</h1>
				<p>You don't have the permission to access the URL <?php echo(htmlspecialchars($_SERVER['REQUEST_URI'])); ?>.</p>
			</div>
		</div>
		<div id="space"></div>
		<div id="container_footer">
			<div class="wrap_content">
<?php printFooter(); ?>
			</div>
		</div>
	</div>
	</body>
</html>