<?php
require(__DIR__.'/../inc/head.php');
printHead('Donate');
?>
	<body>
	<div id=wrapper>
		<div id="container_headline">
			<?php printHeadLine(); ?>
		</div>

		<div class="container_seperator">
			<div class="wrap_content breadcrumbs">
				<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/" itemprop="url"><span itemprop="title">TortoiseGit.org</span></a></span> &raquo; Donate
			</div>
		</div>

		<div class="container_grey">
			<div class="wrap_content contentpage">
				<h1>Donate to TortoiseGit</h1>
				<p>Donations will be used to pay hosting and other costs. If anything is left over at the end of the year the amount is shared between the developers.</p>
				<p>We accept donations mediated by the following services:</p>
				<table>
					<tr>
						<td width="220" style="font-size: 150%;">PayPal</td>
						<td align="center"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=GJGTG75GV5PL6&lc=C2&item_name=TortoiseGit&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted" target="_blank"><img src="paypal_btn_donateCC_LG.png" border="0" width="147" height="47"></a>
						<br><span style="font-size:60%;">PayPal doesn't allow persons to receive donations,<br>please click on "Pay" instead.</span></td>
					</tr>
					<tr style="background-color:#EEEEEE;">
						<td style="font-size: 150%;">Flattr</td>
						<td align="center"><a href="https://flattr.com/thing/856329/TortoiseGit" target="_blank"><img src="flattrbutton.png" border="0" width="50" height="60"></a></td>
					</tr>
					<tr>
						<td style="font-size: 150%;">Alipay.com (支付宝)</td>
						<td align="center"><a href="http://me.alipay.com/tortoisegit" target="_blank"><img src="atlpay-btn-index.png" border="0" width="159" height="37"></a></td>
					</tr>
				</table>
				<p>We thank all donators!</p>

				<h2>It's not all about money</h2>
				<p>We really appreciate if you donate money, however, <b>manpower</b> is also needed!</p>
				<p>You&#x27;re welcome to <b><a href="/contribute/">contribute</a></b> to this project!</p>
			</div>
		</div>
		<div id="space"></div>
		<div id="container_footer">
			<div class="wrap_content">
<?php printFooter(); ?>
			</div>
		</div>
	</div>
	</body>
</html>